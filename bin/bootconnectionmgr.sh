#!/bin/bash 

# Execute network actions based WiFi connection state
#   "waitingforconnection" as set by the boot script
#   "connected" as set by MethodCall
#   "disconnected" as set by SaveConfig
#   "connectinghomeoffice" as set by this script
#   "connecteduploading" as set by this script
#   "connectedhomeoffice" as set by this script
# 

# if no devicestatus.json file exists, just exit since everything keys off of that file
if [ ! -e /home/kam/data/devicestatus.json ]; then 
    exit 0
fi

# use the wifiTimeStamp to determine elapsed time.  
#  Only potential complication will be if the device time gets updated to the iPad time.
    wifiMode=$(php /home/kam/kapturrkam/core/Common/Tools/GetDeviceStatus.php wifimode)
    wifiState=$(echo $wifiMode | cut -f1 -d:)
    wifiTimeStamp=$(echo $wifiMode | cut -f2 -d:)
    now=$(date +%s)
    elapsed=$(($now - $wifiTimeStamp))
    datetime=$(TZ=UTC+7 date '+%Y-%m-%d %H:%M:%S')
    php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "WiFi up with state: ${wifiState}, for ${elapsed} seconds"

# if WiFi is in AP mode awaiting a connection
if [ "$wifiState" == "waitingforconnection" ]; then

    # if we've been waiting for 10 minutes or more
    if (( $elapsed > 600 )); then

        # set status to indicate connectinghomeoffice incase it fails and we need to retry next time this script runs
        php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s wifimode:connectinghomeoffice:$(date +%s)
        php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "Setting WiFi state to connectinghomeoffice"

        # no connection exists, switch WiFi module to station mode
        # it will then automatically connect to predefined wifi networks in /var/lib/connman/..., if available
        php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "Switching WiFi to STA mode, connecting to favorite network if available"
        connmanctl tether wifi off 2> /dev/null
        sleep 2s
        php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "Enable network time sync, ntp = true"
        sudo timedatectl set-ntp true 2> /dev/null

        # get json ping response and strip away to reveal the response: value
        php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "Pinging IDAX server.."
        jsonresponse=$(curl -s https://collect.idaxdata.com/MethodCall.php/IdaxManager::ping | python -m json.tool | grep response) 2> /dev/null
        response=${jsonresponse#*:}
        value=${response%,*}

        # check connectivity to idax server, if connection, start upload
        if [ $value == '"success"' ]; then
                
            # if there are mp4s to upload, start upload now
            mp4s=(/home/kam/data/*.mp4)
            if [ -e "${mp4s[0]}" ]; then
                php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s wifimode:connecteduploading:$(date +%s)
                php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "Setting WiFi state to connecteduploading"

                php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "IDAX server found, starting upload script."
                php /home/kam/kapturrkam/core/Common/Tools/KamVideoTool.php -uv 2> /dev/null
            fi
            
            # set connection state to connected in case we need to retry the upload
            php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s wifimode:connectedhomeoffice:$(date +%s)
            php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "Setting WiFi state to connectedhomeoffice"
        else
            php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "No response from IDAX server, will retry again in 10 minutes."                
        fi

    fi
    
    exit 0
fi

# if WiFi is trying to connect to the home office, continue trying until successful
if [ "$wifiState" == "connectinghomeoffice" ]; then

    # get json ping response and strip away to reveal the response: value
    php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "Pinging IDAX server.."
    jsonresponse=$(curl -s https://collect.idaxdata.com/MethodCall.php/IdaxManager::ping | python -m json.tool | grep response) 2> /dev/null
    response=${jsonresponse#*:}
    value=${response%,*}

    # check for connectivity to the idax server
    if [ $value == '"success"' ]; then
    
        # if there are mp4s to upload, start upload now
        mp4s=(/home/kam/data/*.mp4)
        if [ -e "${mp4s[0]}" ]; then
            php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s wifimode:connecteduploading:$(date +%s)
            php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "Setting WiFi state to connecteduploading"

            php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "IDAX server found, starting upload script."
            php /home/kam/kapturrkam/core/Common/Tools/KamVideoTool.php -uv 2> /dev/null
        fi
        
        # set connection state to connected in case we need to retry the upload
        php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s wifimode:connectedhomeoffice:$(date +%s)
        php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "Setting WiFi state to connectedhomeoffice"

    else
        php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "No response from IDAX server, will retry again in 10 minutes."                

    fi
    
    exit 0
fi

# if WiFi is connected to home office, but we're still running, so lets retry upload
if [ "$wifiState" == "connectedhomeoffice" ]; then

    # if there are mp4s to upload, start upload now
    mp4s=(/home/kam/data/*.mp4)
    if [ -e "${mp4s[0]}" ]; then
        php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s wifimode:connecteduploading:$(date +%s)
        php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "Setting WiFi state to connecteduploading"

        php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "IDAX server found, starting upload script."
        php /home/kam/kapturrkam/core/Common/Tools/KamVideoTool.php -uv 2> /dev/null
    fi
    
    # set connection state to connected in case we need to retry the upload
    php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s wifimode:connectedhomeoffice:$(date +%s)
    php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "Setting WiFi state to connectedhomeoffice"

    exit 0
fi

# find out is wifi is "connected" by a client to the device in AP mode
if [ "$wifiState" == "connected" ]; then
    
    php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "WiFi in AP connected state, will check again in 10 minutes"    

    exit 0
fi

# disconnected occurrs after a user has completed their connection to the device in AP mode
if [ "$wifiState" == "disconnected" ]; then

    php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php bootconnectionmgr.sh "WiFi state disconnected, powering down wifi and removing this cron job"

    # power down wifi
    connmanctl disable wifi 2> /dev/null
    
    # remove this cronjob script (it will automatically be readded at next boot)
    sudo crontab -l 2> /dev/null | grep -v "/home/kam/kapturrkam/bin/bootconnectionmgr.sh" | sudo crontab -

    exit 0
fi
