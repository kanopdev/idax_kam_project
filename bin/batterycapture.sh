#!/bin/bash 

# Set bin folder path
bin="/home/kam/kapturrkam/bin"

# Set initial capture timestamp
time=$(TZ=UTC+7 date '+%Y-%m-%d %H:%M:%S')
echo $time >> /home/kam/data/battery.log

# echo current CPU frequency
cpufreq-info -e | awk '/current CPU/ { match($0, /current CPU/); print substr($0, RSTART+8, 30); }' >> /home/kam/data/battery.log

# echo available storage
df -h /dev/mmcblk0p1 | awk '/\/dev\/mmcblk0p1/ { match($0, /\/dev\/mmcblk0p1/); print "Storage Total/Used/Avail"substr($0, RSTART+15, 18); }' >> /home/kam/data/battery.log

# echo available memory
free -h | awk '/cache:/ { match($0, /cache:/); print "RAM Used/Avail"substr($0, REND + 20); }' >> /home/kam/data/battery.log

# echo battery remaining statistics
$bin/battremaining >> /home/kam/data/battery.log

# only start capture if current battery voltage >= 11v
batterystr=$($bin/batterystats | grep "CH1" | sed 's/CH1 (Battery): //g' | sed 's/mV//g' | sed 's/mA//g')
set -- $batterystr
voltage=$1
if [ $voltage -ge 11000 ]; then

    # echo a comment that starting capture
    echo "capture -F -l -x VBR -b 64 -t 3540 -w /home/kam/data/capturetest" >> /home/kam/data/battery.log
    $bin/capture -F -l -x VBR -b 64 -t 3540 -w /home/kam/data/capturetest

    echo "Converting /home/kam/data_0001.raw to /home/kam/data/capturetest_${time}.mp4" >> /home/kam/data/battery.log
    $bin/ffmpeg -hide_banner -v 0 -f h264 -y -i /home/kam/data/capturetest_0001.raw -vcodec copy /home/kam/data/capturetest_"$time".mp4
    unlink /home/kam/data/capturetest_0001.raw

else

    # voltage below minimum capture threshold
    echo "WARNING: Voltage ${voltage}mV below minimum threshold (11V), skipping video capture"

fi


# echo current CPU frequency again, at completion of recording
cpufreq-info -e | awk '/current CPU/ { match($0, /current CPU/); print substr($0, RSTART+8, 30); }' >> /home/kam/data/battery.log

# echo battery remaining statistics after capture
$bin/battremaining >> /home/kam/data/battery.log

# set final capture timestamp
time=$(TZ=UTC+7 date '+%Y-%m-%d %H:%M:%S')
echo $time >> /home/kam/data/battery.log

echo >> /home/kam/data/battery.log

