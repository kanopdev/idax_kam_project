#!/bin/bash 
 
filepath=$1

# Set bin folder path
bin="/home/kam/kapturrkam/bin"

echo 

if [[ -n "$filepath" ]]; then

    # capture 10 frames
    echo "Capturing 10 frames from camera to $filepath_0001.raw"
    $bin/capture -F -l -x CBR -b 48 -c 10 -w "$filepath"

    echo "Creating $filepath.jpg image from first frame of ${filepath}_0001.raw"
    $bin/ffmpeg -hide_banner -v 0 -y -i "$filepath"_0001.raw -vframes 1 -s 640x480 -f image2 "$filepath".jpg

    unlink "$filepath"_0001.raw
    
else
    
    echo "Usage: captureimage.sh [PATH/FILENAME] (do not supply file extension)"

fi

echo