#!/bin/bash

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
  cleanup

  # Update the live stream state
  php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s livestream:off -e "Live stream aborted" -l /home/kam/data/devicelog.json
}

function cleanup() {
  # Delete live stream media files
  rm -rf "$base"/live/*.*

  # Delete the FIFO.
  rm -rf $FIFO
}

php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -e "Live stream starting" -l /home/kam/data/devicelog.json -s livestream:starting:live/stream.m3u8

# location of live streaming files
base="/var/www/html"

# Cleanup from any previous run
cleanup

# initialize FIFO pipe
FIFO="/home/kam/data/livestream"
mkfifo -m 0777 $FIFO

# set bin path
bin="/home/kam/kapturrkam/bin"

echo
echo "Starting camera capture..."

## make sure the log file is writable
sudo chmod 0777 /home/kam/data/capture.log

## start capturing live to a file
$bin/capture -F -l -x VBR -b 48 -c 0 -o > $FIFO &
CAPTUREPID=$!

## sleep a few seconds to give the FIFO enough frames for ffmpeg to recognize it
sleep 1

echo "Starting ffmpeg stream..."

# raw h264 camera stream from FIFO to ffmpeg, convert to HLS on the fly and put in live stream folder
#   the live stream will continue indefinitely until cancelled
#   ffmpeg is set to wrap segments to ensure this process does not continue to consume disk space
$bin/ffmpeg -y \
  -v 0 \
  -hide_banner \
  -i $FIFO \
  -vcodec copy \
  -map 0 \
  -f segment \
  -segment_time 10 \
  -segment_wrap 10 \
  -segment_list "$base/live/stream.m3u8" \
  -segment_list_size 10 \
  -segment_list_flags +live \
  -segment_list_type m3u8 \
  "$base/live/%08d.ts" &
FFMPEGPID=$!

echo "Streaming to /var/www/html/livestream.html, PID=$$, CAPTUREPID=${CAPTUREPID}, FFMPEGPID=${FFMPEGPID}. CTRL-C to cancel."

php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s livestream:on:live/stream.m3u8:$$:$FFMPEGPID:$CAPTUREPID

wait $FFMPEGPID

cleanup

# Update the live stream state
php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s livestream:off -e "Live stream closed" -l /home/kam/data/devicelog.json
