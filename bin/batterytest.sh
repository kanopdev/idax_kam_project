#!/bin/bash

echo
start=$(date +%s)
echo " --- Battery test commencing ---"
echo

## loop indefinitely
while [  1 ]; do
    now=$(date +%s)
    elapsed=$(($now - $start))
    datetime=$(TZ=UTC+7 date '+%Y-%m-%d %H:%M:%S')
    echo "${datetime} - elapsed: ${elapsed}s"
    cpufreq-info -e | awk '/current CPU/ { match($0, /current CPU/); print substr($0, RSTART+8, 30); }'
#    /home/kam/kapturrkam/bin/batterystats -r
    df -h /dev/mmcblk0p1 | awk '/\/dev\/mmcblk0p1/ { match($0, /\/dev\/mmcblk0p1/); print "Storage Total/Used/Avail"substr($0, RSTART+15, 18); }'
    free -h | awk '/cache:/ { match($0, /cache:/); print "RAM Used/Avail"substr($0, REND + 20); }'
    echo
    sleep 60
done
