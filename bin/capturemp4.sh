#!/bin/bash 

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
  php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s capture:off -e "Capture aborted" -l /home/kam/data/devicelog.json
}

filepath=$1
duration=$2
seg=$3
lastjob=$4

# if a segment length supplied, use it, else default to 1 hr
if [[ -n "$seg" ]] && [[ "$seg" -gt 0 ]]; then
    segment=$seg
else 
    segment=3600
fi

# Set bin folder path
bin="/home/kam/kapturrkam/bin"

# Set initial capture timestamp
starttime=$(TZ=UTC+7 date '+%Y-%m-%d %H:%M:%S')

echo
if [[ -n "$filepath" ]] && [[ -n "$duration" ]]; then

    ## Set CPU to performance mode
    sudo /usr/bin/cpufreq-set -d 900MHz -u 1000MHz -g performance

    echo "Capturing ${duration}s h264 640x480 video into ${segment}s segments: ${filepath}.xxx.raw"

    # Create a status report that we're starting a new capture job
    php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s capture:on -e "Capture started" -l /home/kam/data/devicelog.json

    # Keep track of the video in a local file.
    php /home/kam/kapturrkam/core/Common/Tools/KamVideoTool.php -sc ${filepath} $duration

    # Set the camera parameters to configured values
    php /home/kam/kapturrkam/core/Common/Tools/SetKamParams.php

    # Capture h264 videos in 1 hour segments
    $bin/capture -F -l -x CBR -b 48 -r 15 -t "$duration" -s "$segment" -w "$filepath"

    # Update the video in the local file.
    php /home/kam/kapturrkam/core/Common/Tools/KamVideoTool.php -ec ${filepath} success

    # determine the number of video segments captured
    numvideos=$((duration / segment))
    remainder=$((duration % segment))
    if [[ $remainder -gt 0 ]]; then
        let numvideos=numvideos+1
    fi

    # Videos are captured in h264 format broken into 1 hour segments for the duration
    #  convert each video to MP4 and name accordingly
    counter=1
    while [  $numvideos -gt 0 ]; do
        printf -v srcfilecnt "%04d" $counter
        printf -v dstfilecnt "%03d" $counter
        let counter=counter+1 
        let numvideos=numvideos-1;

        echo "Converting ${filepath}_${srcfilecnt}.raw to ${filepath}.${dstfilecnt}.mp4" 
        $bin/ffmpeg -hide_banner -v 0 -f h264 -y -i "$filepath"_"$srcfilecnt".raw -vcodec copy "$filepath"."$dstfilecnt".mp4
        #unlink "$filepath"_"$srcfilecnt".raw

        # Update the video in the local file.
        php /home/kam/kapturrkam/core/Common/Tools/KamVideoTool.php -as ${filepath} "$filepath"."$dstfilecnt".mp4 $segment
    done

    # Create a status report that the capture is complete
    php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s capture:off -e "Capture complete" -l /home/kam/data/devicelog.json

else

    echo "Usage: capturemp4.sh [PATH/FILENAME (sans extension)] [DURATION (seconds)] [Optional:[SEGMENT LENGTH (seconds)]]"

fi

## Set CPU to performance mode
sudo /usr/bin/cpufreq-set -d 300MHz -u 400MHz -g powersave

## If this is the last job to execute, then indicate that in the last known good state and then shut the system down.
if [[ $lastjob = "yes" ]]; then
    php /home/kam/kapturrkam/core/Common/Tools/ReportStatus.php -s lastknownstate:videocapturescomplete:$(date +%s) -e "Last video capture completed" -l /home/kam/data/devicelog.json
    sudo shutdown
else
    # Remove the cron job that ran this instance of the script.  We can get that result by creating future video capture jobs
    php /home/kam/kapturrkam/core/Common/Tools/CreateCRONJobs.php
fi

echo
