#!/bin/bash 

# Set bin folder path
bin="/home/kam/kapturrkam/bin"

# Set initial capture timestamp
time=$(TZ=UTC+7 date '+%Y-%m-%d %H:%M:%S')
echo "PPP connect: $time" >> /home/kam/data/ppp.log

# remove ethernet and USB connections, if any
close_ethernet() {
    sudo ifconfig eth0 down
#    sudo ifconfig usb0 down
}

# test PPP connection
test_connection() {
    received=$(sudo ping -c 2 www.google.com | grep "2 received")
    if [ -z "$received" ]; then
        echo "PPP connection test completed successfully." >> /home/kam/data/ppp.log
    else
        echo "PPP connection test failed." >> /home/kam/data/ppp.log
    fi
}

# bring up PPP connection for appropriate carrier network
carrier=$($bin/wwanfuncs -s | grep "Carrier" | sed 's/Carrier: //g' | sed 's/ Wireless//g')

case $carrier in
	T-Mobile)
        echo "Carrier found, calling pon t-mobile" >> /home/kam/data/ppp.log
        close_ethernet
        pon t-mobile
        test_connection
	    ;;
	Verizon)
        echo "Carrier found, calling pon verizon" >> /home/kam/data/ppp.log
        close_ethernet
        pon verizon
        test_connection
	    ;;
	*)
        echo "No carrier found, exiting" >> /home/kam/data/ppp.log
	    ;;
	esac

echo >> /home/kam/data/ppp.log

