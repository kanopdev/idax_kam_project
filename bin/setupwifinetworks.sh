#!/bin/bash 

    # log this activity
    php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php setupwifinetworks.sh "Scanning available WiFi networks.."

    ### Get SSID, HEX SSID & Password from wifinetworks file included with build
    file=/home/kam/kapturrkam/bin/wifinetworks
    while IFS='' read -r line || [[ -n "$line" ]]; do
    
        ### For each line, try to find an equivalent network service by SSID matching
        ### extract SSID, HEX SSID & Password from the line
        SSID=$(echo $line| cut -d':' -f 1)
        HEXSSID=$(echo $line| cut -d':' -f 2)
        PASS=$(echo $line| cut -d':' -f 3)

        ### Try and locate a device specific service name from connmanctl using provided SSID name
        string=$(connmanctl services | grep -w "${SSID}")
        words=($string)    
        
        ## see if any matching networks
        count=${#words[@]}
        if (( $count > 0 )); then
            php /home/kam/kapturrkam/core/Common/Tools/dbgmsg.php setupwifinetworks.sh "Setting up autoconnect for WiFi network: ${words[$count-1]}"

            ### We have a matching network service, create the connman folder and settings entry for it
            # remove any previous folder if it existed before
            sudo rm -rf /var/lib/connman/${words[$count-1]} 2> /dev/null
            sudo mkdir /var/lib/connman/${words[$count-1]}
            
            # create settings file and set temp permissions to edit
            sudo touch /var/lib/connman/${words[$count-1]}/settings
            sudo chmod 777 /var/lib/connman/${words[$count-1]}/settings

            # write the connection parameters to the service file
            echo "[${words[$count-1]}]" >> /var/lib/connman/${words[$count-1]}/settings
            echo "Name=${SSID}" >> /var/lib/connman/${words[$count-1]}/settings
            echo "SSID=${HEXSSID}" >> /var/lib/connman/${words[$count-1]}/settings
            echo "Passphrase=${PASS}" >> /var/lib/connman/${words[$count-1]}/settings
            echo "IPv4=dhcp" >> /var/lib/connman/${words[$count-1]}/settings
            echo "AutoConnect=true" >> /var/lib/connman/${words[$count-1]}/settings
            echo "Favorite=true" >> /var/lib/connman/${words[$count-1]}/settings        
            
            # restore to system level permissions
            sudo chmod 600 /var/lib/connman/${words[$count-1]}/settings
        fi
        
    done < "$file"

