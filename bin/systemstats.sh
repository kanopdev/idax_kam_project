#!/bin/bash

echo

## get current CPU frequency setting (we only use performance and powersave settings for Kam)
echo " --- CPU Settings ---"
cpufreq-info -e | grep "available frequency steps*"
cpufreq-info -e | grep "current policy*"
cpufreq-info -e | grep "current CPU frequency*"

echo

## get battery level
echo " --- Battery State ---"
/home/kam/kapturrkam/bin/batterystats -r

echo

## get storage stats
echo " --- Storage Stats ---"
df -h /dev/mmcblk0p1

echo 

## get system board/memory stats
echo " --- System/Memory info ---"
sudo lshw -class system | grep "product:*" 
grep "model name" /proc/cpuinfo
grep MemTotal /proc/meminfo
free -h | awk '/cache:/ { match($0, /cache:/); print "used/free"substr($0, REND + 20); }'

echo 

## get network stats
echo " --- Network stats ---"
sudo ip a | grep "eth0"
sudo ip a | grep "wlan0"
sudo ip a | grep "wwan0"

echo