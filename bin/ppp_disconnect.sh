#!/bin/bash 

# Set bin folder path
bin="/home/kam/kapturrkam/bin"

# Set initial capture timestamp
time=$(TZ=UTC+7 date '+%Y-%m-%d %H:%M:%S')
echo "PPP disconnect: $time" >> /home/kam/data/ppp.log

# remove ethernet and USB connections, if any
open_ethernet() {
    sudo ifconfig eth0 up
}

# test ethernet connection
test_connection() {
    received=$(sudo ping -c 2 www.google.com | grep "2 received")
    if [ -z "$received" ]; then
        echo "Ethernet connection test completed successfully." >> /home/kam/data/ppp.log
    else
        echo "Ethernet connection test failed." >> /home/kam/data/ppp.log
    fi
}

# bring down PPP connection for appropriate carrier network
carrier=$($bin/wwanfuncs -s | grep "Carrier" | sed 's/Carrier: //g' | sed 's/ Wireless//g')

case $carrier in
	T-Mobile)
        echo "Carrier found, calling poff t-mobile" >> /home/kam/data/ppp.log
        poff t-mobile
        open_ethernet
        test_connection
	    ;;
	Verizon)
        echo "Carrier found, calling poff verizon" >> /home/kam/data/ppp.log
        poff verizon
        open_ethernet
        test_connection
	    ;;
	*)
        echo "No carrier found, ignoring command and exiting" >> /home/kam/data/ppp.log
	    ;;
	esac

echo >> /home/kam/data/ppp.log

