<?php
	define ("CORE_ROOT", __DIR__);

	require_once CORE_ROOT."/dbglog.php";

	// Debug zones for business logic / processes.
	define ("DBGZ_CORE",               intval(0x10000000));
	define ("DBGZ_AUTOLOAD",           intval(0x20000000));
	define ("DBGZ_METHODCALL",         intval(0x40000000));
	define ("DBGZ_APP",                intval(0x80000000));
	define ("DBGZ_TOKENMGR",           intval(0x01000000));
	define ("DBGZ_AWSFILEMGR",         intval(0x02000000));
	define ("DBGZ_ASYNCCALL",          intval(0x04000000));
	define ("DBGZ_KAM",                intval(0x08000000));
	define ("DBGZ_KAMKONFIG",          intval(0x00100000));
	define ("DBGZ_KAMVIDEOS",          intval(0x00200000));

	define ("dbg_file", "/home/kam/data/kam.log");
?>
