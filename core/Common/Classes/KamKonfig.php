<?php

	namespace Core\Common\Classes;

	use \Core\Common\Classes\TokenManager;
	use \Core\Common\Classes\AWSFileManager;
	use \Core\Common\Classes\Kam;
	use \Core\Common\Classes\KamVideos;

	require_once '/home/kam/kapturrkam/core/core.php';

	class KamKonfig
	{
		private $context = NULL;

		public static function MethodCallDispatcher(
			$context,
			$methodName,
			$parameters
			)
		{
 			DBG_ENTER(DBGZ_KAMKONFIG, __METHOD__, "methodName=$methodName");

			$kamKonfig = new KamKonfig($context);

			$response = "failed";
			$responder = "KamKonfig::$methodName";
			$returnval = "failed";

			switch ($methodName)
			{
				case 'GetConfig':
					$result = $kamKonfig->GetConfig(
							$serverTimeStamp,
							$secret,
							$job,
							$jobSite,
							$device,
							$deployedDevice,
							$resultString
							);

					if ($result)
					{
						$response = "success";

						// Don't return the secret over web interface.
						$returnval = array(
								"servertimestamp" => $serverTimeStamp,
								"job" => $job,
								"jobsite" => $jobSite,
								"device" => $device,
								"deployeddevice" => $deployedDevice,
								"resultstring" => $resultString
								);
					}
					else
					{
						$response = "failed";
						$returnval = array("resultstring" => $resultString);
					}

					break;

				case 'SaveConfig':
					$result = $kamKonfig->SaveConfig(
							isset($parameters['servertimestamp']) ? $parameters['servertimestamp'] : NULL,
							isset($parameters['secret']) ? strval($parameters['secret']) : NULL,
							isset($parameters['job']) ? json_decode($parameters['job'], true) : NULL,
							isset($parameters['jobsite']) ? json_decode($parameters['jobsite'], true) : NULL,
							isset($parameters['device']) ? json_decode($parameters['device'], true) : NULL,
							isset($parameters['deployeddevice']) ? json_decode($parameters['deployeddevice'], true) : NULL,
							isset($parameters['resetdevice']) ? boolval($parameters['resetdevice']) : NULL,
							$resultString
							);

					if ($result)
					{
						$response = "success";
					}
					else
					{
						$response = "failed";
					}

					$returnval = array("resultstring" => $resultString);

					break;

				case 'EraseData':
					$result = $kamKonfig->EraseData(
							0,     // kamLogAction (0=none)
							FALSE, //deleteDeviceStatus
							$resultString
							);

					if ($result)
					{
						$response = "success";
					}
					else
					{
						$response = "failed";
					}

					$returnval = array("resultstring" => $resultString);

					break;

				case 'ApplyKamParams':
					$result = $kamKonfig->ApplyKamParams(
							isset($parameters['name']) ? $parameters['name'] : NULL,
							$resultString
							);

					if ($result)
					{
						$response = "success";
					}
					else
					{
						$response = "failed";
					}

					$returnval = array("resultstring" => $resultString);

					break;

				default:
					$response = "failed";
					$responder = "KamKonfig";
					$returnval = "method not found";
					break;
			}

			DBG_INFO(DBGZ_KAMKONFIG, __METHOD__, "responder=$responder, response=$response");

			$response_str = array(
					"results" => array(
							'response' => $response,
			  				'responder' => $responder,
							'returnval' => $returnval
							)
					);

			DBG_RETURN(DBGZ_KAMKONFIG, __METHOD__);
			return $response_str;
		}

		public function __construct(
			$context
			)
		{
			$this->context = $context;
		}

		private function ScheduleCaptureJobs(
			$timeZone,
			$dateRanges,
			$timeRanges
			)
		{
			DBG_ENTER(DBGZ_KAMKONFIG, __METHOD__, "timezone=$timeZone");

			$command = "/home/kam/kapturrkam/bin/capturemp4.sh";
			$segmentDuration = 3600;

			$systemTime = time();

			//
			// Determine number of capture jobs so we can detect when we're adding the last one.  The last capture job will also shut the system down
			// after the capture is complete.
			//
			$numCaptureJobs = count($dateRanges) * count($timeRanges);
			$numCaptureJobsAdded = 0;

			// We need to add the jobs chronologically so we know which one is the last one.  So we need to first sort the date ranges and time ranges in ascending order.
			uasort($dateRanges, function($a, $b) { return strtotime($a[0]) - strtotime($b[0]); });
			uasort($timeRanges, function($a, $b) { return strtotime($a[0], 0) - strtotime($b[0], 0); });

			foreach ($dateRanges as &$dateRange)
			{
				$unixDate = strtotime($dateRange[0], 0);
				$unixDateRangeEnd = strtotime($dateRange[1], 0);

				DBG_INFO(DBGZ_KAMKONFIG, __METHOD__, "dateRangeStart=".$dateRange[0]." ($unixDate), dateRangeEnd=".$dateRange[1]." ($unixDateRangeEnd)");

				while ($unixDate <= $unixDateRangeEnd)
				{
					foreach ($timeRanges as &$timeRange)
					{
						$startTime = date("Y-m-d", $unixDate)." ".$timeRange[0];

						// Create a DateTime object using the device local time zone.
						$startTimeObj = date_create($startTime, new \DateTimeZone($timeZone));

						// Get the date/time components in local time (used for the capture filename)
						$localMinutes = $startTimeObj->format("i");
						$localHours = $startTimeObj->format("H");
						$localDay = $startTimeObj->format("d");
						$localMonth = $startTimeObj->format("m");
						$localDayOfWeek = "?";
						$localYear = $startTimeObj->format("Y");

						// Convert the DateTime object to UTC.
						$startTimeObj->setTimeZone(new \DateTimeZone("UTC"));

						// Create a CRON job for this item, but only if it occurs in the future.
						if ($startTimeObj->getTimestamp() > $systemTime)
						{
							//
							// If the end time is less than the start time (which happens - for example a video that
							// has a start time of 23:00 and has a duration of 2 hours ends at 01:00), then add 24 to
							// the end time (assumes cannot have a duration of more than 24 hours).
							//
							$startTimeInSeconds = strtotime($timeRange[0]);
							$endTimeInSeconds = strtotime($timeRange[1]);

							if ($endTimeInSeconds < $startTimeInSeconds)
							{
								$endTimeInSeconds += 24 * 60 * 60;
							}

							$durationInSeconds = $endTimeInSeconds - $startTimeInSeconds;

							// Get the date/time components in UTC time (used for the CRON job)
							$utcMinutes = $startTimeObj->format("i");
							$utcHours = $startTimeObj->format("H");
							$utcDay = $startTimeObj->format("d");
							$utcMonth = $startTimeObj->format("m");
							$utcDayOfWeek = "?";
							$utcYear = $startTimeObj->format("Y");

							$numCaptureJobsAdded += 1;

							// Indicate if this is the last job
							$lastjob = ($numCaptureJobsAdded == $numCaptureJobs) ? "yes" : "no";

							// commandline is "filename duration segmentduration"
							$commandParams = "/home/kam/data/Standard_SCU2X3_{$localYear}-{$localMonth}-{$localDay}_{$localHours}{$localMinutes} {$durationInSeconds} {$segmentDuration} $lastjob";

							$scheduledTime = "$utcMinutes $utcHours $utcDay $utcMonth $utcDayOfWeek";

							$cronJob = "$scheduledTime $command $commandParams";
							DBG_INFO(DBGZ_KAMKONFIG, __METHOD__, "Add new job: exec('crontab -l | { cat; echo $cronJob; } | crontab -')");

							exec("crontab -l | { cat; echo $cronJob; } | crontab -");
						}
						else
						{
							DBG_INFO(DBGZ_KAMKONFIG, __METHOD__, "Skipping job because it is for a time in the past");
							$numCaptureJobs--;
						}
					}

					$unixDate += 60 * 60 * 24; // advance unixDate to next day
				}
			}

			DBG_RETURN(DBGZ_KAMKONFIG, __METHOD__);
		}

		public function GetConfig(
			&$serverTimeStamp,
			&$secret,
			&$job,
			&$jobSite,
			&$device,
			&$deployedDevice,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAMKONFIG, __METHOD__);

			if (!$this->context->local_user && ($this->context->token === NULL))
			{
				$resultString = "token required";
				DBG_ERR(DBGZ_KAMKONFIG, __METHOD__, $resultString);

				return FALSE;
			}

			// File might not exist so use @ to suppress PHP error message.
			$result = @file_get_contents("/home/kam/data/config.json");

			if ($result !== FALSE)
			{
				$json = json_decode($result, TRUE);

				$serverTimeStamp = $json["servertimestamp"];
				$secret = $json["secret"];
				$job = $json["job"];
				$jobSite = $json["jobsite"];
				$device = $json["device"];
				$deployedDevice = $json["deployeddevice"];
			}
			else
			{
				$resultString = serialize(error_get_last());
			}

			DBG_RETURN_BOOL(DBGZ_KAMKONFIG, __METHOD__, $result);
			return $result;
		}

		public function ResetDevice(
			$kamLogAction, // (0=none, 1=truncate, 2=delete),
			$deleteWLANMac0Addr,
			$deleteDeviceStatus,
			$uploadFiles,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAMKONFIG, __METHOD__, "kamLogAction=$kamLogAction, deleteDeviceStatus=$deleteDeviceStatus, uploadFiles=$uploadFiles");

			$result = TRUE;

			if ($uploadFiles)
			{
				$awsFileManager = new AWSFileManager(IDAX_VIDEOFILES_BUCKET, AWSREGION, AWSKEY, AWSSECRET);

				// Prefix the following files with job site id + deviceid + date to get unique names.
				$result = $this->GetConfig($serverTimeStamp, $secret, $job, $jobSite, $device, $deployedDevice, $resultString);

				if ($result)
				{
					$this->jobSiteId = $jobSite["jobsiteid"];
					$this->deviceId = $device["deviceid"];

					$now = date("Y-m-d-H-i-s");

					if ($result && is_file("/home/kam/data/devicelog.json"))
					{
						$result = $awsFileManager->UploadFile("/home/kam/data/devicelog.json", "{$this->deviceId}-{$this->jobSiteId}-{$now}-devicelog.json", "public-read", true, $resultString);
					}

					if ($result && is_file("/home/kam/data/capture.log"))
					{
						$result = $awsFileManager->UploadFile("/home/kam/data/capture.log", "{$this->deviceId}-{$this->jobSiteId}-{$now}-capture.log", "public-read", true, $resultString);
					}

					if ($result && is_file("/home/kam/data/kam.log"))
					{
						$result = $awsFileManager->UploadFile("/home/kam/data/kam.log", "{$this->deviceId}-{$this->jobSiteId}-{$now}-kam.log", "public-read", true, $resultString);
					}
				}
			}

			// Update the wifi mode to indicate to the bootconnectionmgr that the wifi can be taken down.
			$kam = new Kam(new MethodCallContext(TRUE, NULL, NULL, NULL));
			$kam->SetDeviceStatus("wifimode", "disconnected:".time(), $resultString);

			// Now erase files, but only if the upload succeeded
			if ($result)
			{
				$this->EraseData($kamLogAction, $deleteWLANMac0Addr, $deleteDeviceStatus, $resultString);
			}

			DBG_RETURN(DBGZ_KAMKONFIG, __METHOD__);
		}

		public function EraseData(
			$kamLogAction, // (0=none, 1=truncate, 2=delete),
			$deleteWLANMac0Addr,
			$deleteDeviceStatus,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAMKONFIG, __METHOD__, "kamLogAction=$kamLogAction, deleteDeviceStatus=$deleteDeviceStatus");

			$result = TRUE;

			$kamVideos = new KamVideos();
			$kamVideos->DeleteVideos();

			// Delete the wlan mac addr file.
			if ($deleteWLANMac0Addr)
			{
				exec("sudo rm -f /home/kam/wlan0macaddr");
			}

			// Clean up device state.
			if ($deleteDeviceStatus)
			{
				exec("sudo rm -f /home/kam/data/devicestatus.json");
			}

			exec("sudo rm -f /home/kam/data/config.json");

			// Delete any CRON jobs created for www-data account (scheduled capture jobs, device checkins)
			exec("sudo crontab -u www-data -r");

			// Clean up log files (device log and capture log)
			exec("sudo rm -f /home/kam/data/devicelog.json");
			exec("sudo rm -f /home/kam/data/capture.log");

			if ($kamLogAction == 1)  // truncate
			{
				// Turn off all logging as we don't want any more trace statments to appear in the file we're deleting!
				DBG_SET_PARAMS(0, 0, FALSE, FALSE, NULL);

				// Just truncate the file - want to leave it present because the bootmanager script uses the presence of the file
				// in the boot sequence.
				exec("truncate -s 0 /home/kam/data/kam.log");
			}
			else if ($kamLogAction == 2)  // delete
			{
				// Turn off all logging as we don't want any more trace statments to appear in the file we're deleting!
				DBG_SET_PARAMS(0, 0, FALSE, FALSE, NULL);

				// Just truncate the file - want to leave it present because the bootmanager script uses the presence of the file
				// in the boot sequence.
				exec("sudo rm -f /home/kam/data/kam.log");
			}

			// Clean up captured images
			exec("sudo rm -f /home/kam/data/*.raw");
			exec("sudo rm -f /home/kam/data/*.jpeg");

			// Release any acquired token
			$tokenManager = new TokenManager(new MethodCallContext(TRUE, NULL, NULL, NULL));
			$tokenManager->ReleaseToken($resultString);

			DBG_RETURN(DBGZ_KAMKONFIG, __METHOD__, $result);
			return $result;
		}

		public function ScheduleCRONJobs(
			$deployedDevice
			)
		{
			// First delete all CRON jobs previously scheduled.  New CRON jobs will get created
			// according to the updated configuration.
			exec("crontab -r");

			if (isset($deployedDevice["timezone"]) && isset($deployedDevice["config"]))
			{
				// The client app sends an abbreviation for the time zone, but we use the full name.
				$timeZone = timezone_name_from_abbr($deployedDevice["timezone"]);

				if (isset($deployedDevice["config"]["dateranges"]) && isset($deployedDevice["config"]["timeranges"]))
				{
					$this->ScheduleCaptureJobs(
							$timeZone,
							$deployedDevice["config"]["dateranges"],
							$deployedDevice["config"]["timeranges"]
							);
				}
			}

			// Set the last known good state to indicate that the device is "deployed" in the sense that it is now ready to execute its video capture schedule.
			$kam = new Kam(new MethodCallContext(TRUE, NULL, NULL, NULL));
			$kam->SetDeviceStatus("lastknownstate", "deployed:".time(), $resultString);
		}

		public function SaveConfig(
			$serverTimeStamp,
			$secret,
			$job,
			$jobSite,
			$device,
			$deployedDevice,
			$resetDevice,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAMKONFIG, __METHOD__, "serverTimeStamp=$serverTimeStamp, secret=$secret");

			if (!$this->context->local_user && ($this->context->token === NULL))
			{
				$resultString = "token required";
				DBG_ERR(DBGZ_KAMKONFIG, __METHOD__, $resultString);

				return FALSE;
			}

			// Default resetDevice to TRUE
			if ($resetDevice === NULL)
			{
				$resetDevice = TRUE;
			}

			if ($resetDevice)
			{
				$this->ResetDevice(
						0,     //kamLogAction (0=none)
						FALSE, //deleteWLANMac0Addr
						FALSE, //deleteDeviceStatus
						FALSE, //uploadFiles
						$resultString
						);
			}

			$config = array(
					"servertimestamp" => $serverTimeStamp,
					"secret" => $secret,
					"job" => $job,
					"jobsite" => $jobSite,
					"device" => $device,
					"deployeddevice" => $deployedDevice
					);

			$result = file_put_contents("/home/kam/data/config.json", json_encode($config));

			if ($result !== FALSE)
			{
				// Make sure everyone can read and write the file.
				exec("sudo chmod 0777 /home/kam/data/config.json");

				// First delete all CRON jobs previously schedules.  New CRON jobs will get created
				// according to the updated configuration.
				exec("crontab -r");

				$this->ScheduleCRONJobs($deployedDevice);

				// Set the last known good state to indicate that the device is "deployed" in the sense that it is now ready to execute its video capture schedule.
				$kam = new Kam(new MethodCallContext(TRUE, NULL, NULL, NULL));
				$kam->SetDeviceStatus("lastknownstate", "deployed:".time(), $resultString);
			}
			else
			{
				$resultString = serialize(error_get_last());
			}

			DBG_RETURN_BOOL(DBGZ_KAMKONFIG, __METHOD__, $result);
			return $result;
		}

		public function ApplyKamParams(
			$name,
			$resultString
			)
		{
			DBG_ENTER(DBGZ_KAMKONFIG, __METHOD__, "name=$name");

			$result = $this->GetConfig($serverTimeStamp, $secret, $job, $jobSite, $device, $deployedDevice, $resultString);

			if ($result)
			{
				$kam = new Kam($this->context);
				$result = $kam->SetKamParams($deployedDevice["config"]["kamparams"], $resultString);
			}

			DBG_RETURN_BOOL(DBGZ_KAMKONFIG, __METHOD__, $result);
			return $result;
		}
	}
?>
