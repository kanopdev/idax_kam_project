<?php

	namespace Core\Common\Classes;

	use \Core\Common\Classes\KamKonfig;
	use \Core\Common\Classes\KamVideos;

	require_once '/home/kam/kapturrkam/core/core.php';

	function GetSystemInfo()
	{
		DBG_ENTER(DBGZ_KAM, __FUNCTION__);

		$systemInfo = array();

		// Get CPU Frequency
		$outputLine = exec('cpufreq-info -e | grep "current CPU frequency*"', $outputLines, $result);

		if ($outputLine)
		{
			// Sample output is "  current CPU frequency is 300 MHz.".  Explode the output into tokens and take the second to last one
			$tokens = explode(" ", $outputLine);
			$systemInfo["CPU Frequency"] = $tokens[count($tokens) - 2];
		}

		// Get Product Name
		exec('lshw -class system | grep "product:*"', $outputLines, $result);

		if (count($outputLines))
		{
			// Sample output is
			//     WARNING: you should run this program as super-user.
			//         product: TI AM335x BeagleBone Green
			//     WARNING: output may be incomplete or inaccurate, you should run this program as super-user.
			$systemInfo["Product Name"] = substr($outputLines[1], strpos($outputLines[1], "product: ") + strlen("product: "));
		}

		// Get CPU Model Name
		$outputLine = exec('grep "model name" /proc/cpuinfo', $outputLines, $result);

		if ($outputLine)
		{
			// Sample output is 'model name	: ARMv7 Processor rev 2 (v7l)'
			$systemInfo["CPU Model"] = substr($outputLine, strpos($outputLine, "model name:\t ") + strlen("model name:\t "));
		}

		DBG_RETURN(DBGZ_KAM, __FUNCTION__);
		return $systemInfo;
	}

	function GetPowerInfo()
	{
		DBG_ENTER(DBGZ_KAM, __FUNCTION__);

		$batteryRemaining = "unknown";
		$batteryCharging = "unknown";

		$powerInfo = array();

		// Sample output from "batterystats -r"
		//     CH1 (Battery): 12.34V   196mA
		//     CH2 (Source) : 0.10V     0mA
		//     CH3 (Device) : 5.24V   388mA
		//     Remaining: 77.57%, Charging: No

		exec("sudo /home/kam/kapturrkam/bin/batterystats -r", $batteryStatLines, $result);

		// if ($result != 0)
		// {
		// 	DBG_INFO(DBGZ_KAM, __METHOD__, "batterystats failed - using test data");

		// 	$batteryStatLines[] = "CH1 (Battery): 12.34V   196mA";
		// 	$batteryStatLines[] = "CH2 (Source) : 0.10V     0mA";
		// 	$batteryStatLines[] = "CH3 (Device) : 5.24V   388mA";
		// 	$batteryStatLines[] = "Remaining: 77.57%, Charging: No";
		// }

		foreach ($batteryStatLines as &$batteryStatLine)
		{
			$token = strtok($batteryStatLine, ":");

			if ($token == "Remaining")
			{
				// Get the remaining % and charging status.  Add it to the battery channel info.
				$batteryRemaining = strtok("%");
				$chargingWord = strtok(":");
				$batteryCharging = trim(strtok(""));

				if (($batteryChannelId !== NULL) && isset($powerInfo[$batteryChannelId]))
				{
					$powerInfo[$batteryChannelId]["remaining"] = $batteryRemaining;
					$powerInfo[$batteryChannelId]["charging"] = $batteryCharging;
				}
			}
			else
			{
				$channelInfo = explode(" ", $token);
				$channelId = $channelInfo[0];
				$channelName = trim($channelInfo[1], "()");

				$voltage = trim(strtok("V"));
				$current = trim(strtok("mA"));

				$powerInfo[$channelId] = array("name" => $channelName, "voltage" => $voltage, "current" => $current);

				if ($channelName == "Battery")
				{
					$batteryChannelId = $channelId;
					$powerInfo[$channelId]["remaining"] = $batteryRemaining;
					$powerInfo[$channelId]["charging"] = $batteryCharging;
				}
			}
		}

		DBG_RETURN(DBGZ_KAM, __FUNCTION__);
		return $powerInfo;
	}

	function GetGISLocation()
	{
		DBG_ENTER(DBGZ_KAM, __FUNCTION__);

		$gisLocation = array("latitude" => "56", "longitude" => "129");

		DBG_RETURN(DBGZ_KAM, __FUNCTION__);
		return $gisLocation;
	}

	function GetWWANStatus()
	{
		DBG_ENTER(DBGZ_KAM, __FUNCTION__);

		$batteryRemaining = "unknown";
		$batteryCharging = "unknown";

		$wwanInfo = array();

		// Sample output from "wanfun -s"
		//     Network type: GSM
		//     Status: Registered, home network.
		//     Cell number: 12062271454
		//     IMEI: 358942051096759
		//     Signal strength: Great
		//     Firmware: 17.00.503
		//     Carrier: T-Mobile
		//     result: success

		exec("/home/kam/kapturrkam/bin/wanfun -s", $wwanStatusLines, $result);

		// if ($result == 0)
		// {
		// 	DBG_INFO(DBGZ_KAM, __FUNCTION__, "wanfun -s failed - using test data");

		// 	$wwanStatusLines[] = "Network type: GSM";
		// 	$wwanStatusLines[] = "Status: Registered, home network.";
		// 	$wwanStatusLines[] = "Cell number: 12062271454";
		// 	$wwanStatusLines[] = "IMEI: 358942051096759";
		// 	$wwanStatusLines[] = "Signal strength: Great";
		// 	$wwanStatusLines[] = "Firmware: 17.00.503";
		// 	$wwanStatusLines[] = "Carrier: T-Mobile";
		// 	$wwanStatusLines[] = "result: success";
		// }

		foreach ($wwanStatusLines as &$wwanStatusLine)
		{
			$token = strtok($wwanStatusLine, ":");
			$value = trim(strtok(""));

			$wwanInfo[$token] = $value;
		}

		DBG_RETURN(DBGZ_KAM, __FUNCTION__);
		return $wwanInfo;
	}

	function GetSSID()
	{
		//
		// The ssid is stored in the file /home/kam/wlan0macaddr.  The format of the file is
		//
		//    wlan0 mac address#xx:xx:xx:xx:xx:xx#ssid
		//
		$ssid = NULL;

		$wlan0FileContents = @file_get_contents("/home/kam/wlan0macaddr");

		if ($wlan0FileContents !== FALSE)
		{
			$fileContentElements = explode("#", trim($wlan0FileContents));

			if (count($fileContentElements) == 3)
			{
				$ssid = $fileContentElements[2];
			}
			else
			{
				DBG_ERR(DBGZ_KAM, __FUNCTION__, "Failure obtaining SSID - improper file format");
			}
		}
		else
		{
			DBG_ERR(DBGZ_KAM, __FUNCTION__, "Error reading /home/kam/wlan0macaddr - error=".serialize(error_get_last));
		}

		return $ssid;
	}

	class Kam
	{
		private $context = NULL;

		private $fd = -1;

		private $defaultBrightness = 128;
		private $defaultContrast = 128;
		private $defaultSaturation = 128;
		private $defaultGain = 0;
		private $defaultWhiteBalanceAuto = 1;
		private $defaultWhiteBalanceTemperature = 4000;
		private $defaultSharpness = 128;
		private $defaultBacklight = 0;
		private $defaultExposureAbsolute = 250;
		private $defaultExposureAutoPriority = 1;
		private $defaultPan = 0;
		private $defaultTilt = 0;
		private $defaultFocus = 0;
		private $defaultFocusAuto = 1;
		private $defaultZoom = 100;

		private $parameterInfo = array(
				"brightness" => array(
						"values" => array("min" => 0, "max" => 255, "default" => 128),
						"handlers" => array("get" => "getBrightness", "set" => "setBrightness"),
						"autosetting" => NULL
						),
				"contrast" => array(
						"values" => array("min" => 0, "max" => 255, "default" => 128),
						"handlers" => array( "get" => "getContrast", "set" => "setContrast"),
						"autosetting" => NULL
						),
				"saturation" => array(
						"values" => array("min" => 0, "max" => 255, "default" => 128),
						"handlers" => array( "get" => "getSaturation", "set" => "setSaturation"),
						"autosetting" => NULL
						),
				"gain" => array(
						"values" => array("min" => 0, "max" => 255, "default" => 0),
						"handlers" => array( "get" => "getGain", "set" => "setGain"),
						"autosetting" => NULL
						),
				"autowhitebalance" => array(
						"values" => array("min" => 0, "max" => 1, "default" => 1),
						"handlers" => array("get" => "getAutoWhiteBalance", "set" => "setAutoWhiteBalance"),
						"autosetting" => NULL
						),
				"whitebalance" => array(
						"values" => array("min" => 2000, "max" => 6500, "default" => 4000),
						"handlers" => array("get" => "getWhiteBalanceTemperature", "set" => "setWhiteBalanceTemperature"),
						"autosetting" => "autowhitebalance"
						),
				"sharpness" => array(
						"values" => array("min" => 0, "max" => 255, "default" => 128),
						"handlers" => array( "get" => "getSharpness", "set" => "setSharpness"),
						"autosetting" => NULL
						),
				"backlight" => array(
						"values" => array("min" => 0, "max" => 1, "default" => 0),
						"handlers" => array( "get" => "getBacklightCompensation", "set" => "setBacklightCompensation"),
						"autosetting" => NULL
						),
				"absoluteexposure" => array(
						"values" => array("min" => 3, "max" => 2047, "default" => 250),
						"handlers" => array( "get" => "getExposureAbsolute", "set" => "setExposureAbsolute"),
						"autosetting" => "exposurepriority"
						),
				"exposurepriority" => array(
						"values" => array("min" => 0, "max" => 1, "default" => 1),
						"handlers" => array( "get" => "getExposureAutoPriority", "set" => "setExposureAutoPriority"),
						"autosetting" => NULL
						),
				"pan" => array(
						"values" => array("min" => -36000, "max" => 36000, "default" => 0),
						"handlers" => array( "get" => "getPan", "set" => "setPan"),
						"autosetting" => NULL
						),
				"tilt" => array(
						"values" => array("min" => -36000, "max" => 36000, "default" => 0),
						"handlers" => array( "get" => "getTilt", "set" => "setTilt"),
						"autosetting" => NULL
						),
				"focus" => array(
						"values" => array("min" => 0, "max" => 255, "default" => 0),
						"handlers" => array( "get" => "getFocus", "set" => "setFocus"),
						"autosetting" => "autofocus"
						),
				"autofocus" => array(
						"values" => array("min" => 0, "max" => 1, "default" => 1),
						"handlers" => array( "get" => "getFocusAuto", "set" => "setFocusAuto"),
						"autosetting" => NULL
						),
				"zoom" => array(
						"values" => array("min" => 100, "max" => 500, "default" => 100),
						"handlers" => array( "get" => "getZoom", "set" => "setZoom"),
						"autosetting" => NULL
						)
				);

		public static function MethodCallDispatcher(
			$context,
			$methodName,
			$parameters
			)
		{
 			DBG_ENTER(DBGZ_KAM, __METHOD__, "methodName=$methodName");

			$kam = new Kam($context);

			$response = "failed";
			$responder = "Kam::$methodName";
			$returnval = "failed";

			switch ($methodName)
			{
				case "DevicePing":
					$response = "success";
					$returnval = array();
					break;

				case 'Shutdown':
					$result = $kam->Shutdown($resultString);

					if ($result)
					{
						$response = "success";
					}
					else
					{
						$response = "failed";
					}

					$returnval = array("resultstring" => $resultString);

					break;

				case 'SetDateTime':
					$result = $kam->SetDateTime(
							isset($parameters['date']) ? $parameters['date'] : NULL,
							isset($parameters['time']) ? $parameters['time'] : NULL,
							$newTime,
							$resultString
							);

					if ($result)
					{
						$response = "success";
						$returnval = array("newtime" =>$newTime, "resultstring" => $resultString);
					}
					else
					{
						$response = "failed";
						$returnval = array("resultstring" => $resultString);
					}

					break;

				case "SetDeviceStatus":
					$result = $kam->SetDeviceStatus(
							isset($parameters['key']) ? $parameters['key'] : NULL,
							isset($parameters['value']) ? $parameters['value'] : NULL,
							$resultString
							);

					if ($result)
					{
						$response = "success";
					}
					else
					{
						$response = "failed";
					}

					$returnval = array("resultstring" => $resultString);

					break;

				case 'GetStatus':
					$result = $kam->GetStatus($status, $resultString);

					if ($result)
					{
						$response = "success";
						$returnval = array(
								"status" => $status,
								"resultstring" => $resultString
								);
					}
					else
					{
						$response = "failed";
						$returnval = array("resultstring" => $resultString);
					}
					break;

				case 'ResetDevice':
					$result = $kam->ResetDevice($resultString);

					if ($result)
					{
						$response = "success";
					}
					else
					{
						$response = "failed";
					}

					$returnval = array("resultstring" => $resultString);

					break;

				case 'GetKamParams':
					$result = $kam->GetKamParams(
							$kamParams,
							$resultString
							);

					if ($result)
					{
						$response = "success";
						$returnval = array(
								"kamparams" => $kamParams,
								"resultstring" => $resultString
								);
					}
					else
					{
						$response = "failed";
						$returnval = array("resultstring" => $resultString);
					}

					break;

				case 'SetKamParams':
					$result = $kam->SetKamParams(
							isset($parameters['kamparams']) ? json_decode($parameters['kamparams'], TRUE) : NULL,
							$resultString
							);

					if ($result)
					{
						$response = "success";
					}
					else
					{
						$response = "failed";
					}

					$returnval = array("resultstring" => $resultString);

					break;

				case 'ResetKamParams':
					$result = $kam->ResetKamParams($resultString);

					if ($result)
					{
						$response = "success";
					}
					else
					{
						$response = "failed";
					}

					$returnval = array("resultstring" => $resultString);

					break;

				case 'GetFileContents':
					$result = $kam->GetFileContents(
							$fileContents,
							$resultString
							);

					if ($result)
					{
						$response = "success";
						$returnval = array("filecontents" => $fileContents, "resultstring" => $resultString);
					}
					else
					{
						$response = "failed";
						$returnval = array("resultstring" => $resultString);
					}
					
					break;

				case 'GetImage':
					$result = $kam->GetImage($imageContents, $resultString);

					if ($result)
					{
						$response = "success";
						$returnval = array(
								"imagecontents" => base64_encode($imageContents),
								"resultstring" => $resultString
								);
					}
					else
					{
						$response = "failed";
						$returnval = array("resultstring" => $resultString);
					}

					break;

				case 'CaptureImage':
					$result = $kam->CaptureImage($imageFilename, $resultString);

					if ($result)
					{
						$response = "success";
						$returnval = array(
								"imagefilename" => $imageFilename,
								"resultstring" => $resultString
								);
					}
					else
					{
						$response = "failed";
						$returnval = array("resultstring" => $resultString);
					}

					break;

				case 'LiveStreamStart':
					$result = $kam->LiveStreamStart($playlistFilename, $resultString);
					if ($result)
					{
						$response = "success";
						$returnval = array(
								"playlistfilename" => $playlistFilename,
								"resultstring" => $resultString
								);
					}
					else
					{
						$response = "failed";
						$returnval = array("resultstring" => $resultString);
					}

					break;

				case 'LiveStreamStop':
					$result = $kam->LiveStreamStop($resultString);

					if ($result)
					{
						$response = "success";
					}
					else
					{
						$response = "failed";
					}

					$returnval = array("resultstring" => $resultString);

					break;

				default:
					$response = "failed";
					$responder = "Kam";
					$returnval = "method not found";
					break;
			}

			DBG_INFO(DBGZ_KAM, __METHOD__, "responder=$responder, response=$response");

			$response_str = array(
					"results" => array(
							'response' => $response,
			  				'responder' => $responder,
							'returnval' => $returnval
							)
					);
 
			DBG_RETURN(DBGZ_KAM, __METHOD__);
			return $response_str;
		}

		public function __construct(
			$context
			)
		{
			$this->context = $context;
		}

		public function __destruct()
		{
			if ($this->fd !== -1)
			{
				$this->fd = KamClose($this->fd);
				$this->fd = -1;
			}
		}

		public function getfd()
		{
			if ($this->fd === -1)
			{
				$this->fd = KamOpen();
			}

			return $this->fd;
		}

		// public function getBrightness(
		// 	&$disabled,
		// 	&$minValue,
		// 	&$maxValue,
		// 	&$defaultValue,
		// 	&$currentValue,
		// 	&$resultString
		// 	)
		// {
		// 	DBG_ENTER(DBGZ_KAM, __METHOD__);

		// 	//Brightness value 0-255, def: 128\n";
		// 	$result = KamGetBrightness($this->getfd(), $disabled, $minValue, $maxValue, $defaultValue, $currentValue);

		// 	DBG_INFO(DBGZ_KAM, __FUNCTION__, "disabled=$disabled");

		// 	if (!$disabled)
		// 	{
		// 		DBG_INFO(DBGZ_KAM, __FUNCTION__, "minValue=$minValue, maxValue=$maxValue, defaultValue=$defaultValue, currentValue=$currentValue");
		// 	}

		// 	DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
		// 	return $result;
		// }

		public function getBrightness()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetBrightness($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setBrightness(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetBrightness($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getContrast()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetContrast($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setContrast(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetContrast($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getSaturation()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetSaturation($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setSaturation(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			//Saturation value 0-255, def: 128\n"
			$result = KamSetSaturation($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getGain()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetGain($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setGain(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetGain($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getAutoWhiteBalance()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetAutoWhiteBalance($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setAutoWhiteBalance(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetAutoWhiteBalance($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getWhiteBalanceTemperature()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetWhiteBalance($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setWhiteBalanceTemperature(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetWhiteBalance($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getSharpness()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetSharpness($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setSharpness(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetSharpness($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getBacklightCompensation()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetBacklight($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setBacklightCompensation(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetBacklight($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getExposureAbsolute()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetAbsoluteExposure($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setExposureAbsolute(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetAbsoluteExposure($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getExposureAutoPriority()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetExposurePriority($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setExposureAutoPriority(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetExposurePriority($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getPan()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetPan($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setPan(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetPan($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getTilt()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetTilt($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setTilt(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetTilt($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getFocus()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetFocus($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setFocus(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetFocus($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getFocusAuto()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetAutoFocus($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setFocusAuto(
			$value
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetAutoFocus($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function getZoom()
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = KamGetZoom($this->getfd());

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function setZoom($value)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "value=$value");

			$result = KamSetZoom($this->getfd(), $value);

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return ($result == 0);
		}

		public function Shutdown(
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			if (!$this->context->local_user && ($this->context->token === NULL))
			{
				$resultString = "token required";
				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);

				return FALSE;
			}

			exec("sudo shutdown");

			DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, TRUE);
			return TRUE;
		}

		public function SetDateTime(
			$date,
			$time,
			&$newTime,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "date=$date, time=$time");

			$result = FALSE;

			if (!$this->context->local_user && ($this->context->token === NULL))
			{
				$resultString = "token required";
				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);

				return FALSE;
			}

			DBG_INFO(DBGZ_KAM, __METHOD__, "Exec: sudo timedatectl set-ntp false");
			exec("sudo timedatectl set-ntp FALSE", $output, $retval);

			if ($retval == 0)
			{
				DBG_INFO(DBGZ_KAM, __METHOD__, "Exec: sudo timedatectl set-time '$date $time'");
				exec("sudo timedatectl set-time '$date $time'", $output, $retval);

				if ($retval == 0)
				{
					$result = TRUE;
					$newTime = time();

					// Update the wifi mode so it has the new time.
					$this->SetDeviceStatus("wifimode", "connected:newTime", $resultString);
				}
				else
				{
					$resultString = "Error setting date/time";
					DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);
				}
			}
			else
			{
				$resultString = "Error turning off ntp";
				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);
			}

			DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function GetStatus(
			&$status,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			if (!$this->context->local_user && ($this->context->token === NULL))
			{
				$resultString = "token required";
				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);

				return FALSE;
			}

			// Get the device's mac address which we'll report as the device id.
			$id = "unknown";

			exec("/sbin/ifconfig usb0", $outputLines, $retval);

			if ($retval == 0)
			{
				// Format for the first line of output is "usb0    Link encap:Ethernet  HWaddr xx:xx:xx:xx:xx:xx
				$outputLineFields = explode(" ", $outputLines[0]);
				$macAddress = $outputLineFields[count($outputLineFields) - 1];

				$id = str_replace(":", "", $macAddress);
			}

			$deviceTime = time();
			$this->GetDeviceStatus(NULL, $deviceStatus, $resultString);
			$systemInfo = GetSystemInfo();
			$powerInfo = GetPowerInfo();
			$storageSize = disk_total_space("/");
			$storageAvailable = disk_free_space("/");
			$ssid = GetSSID();
			//$gisLocation = GetGISLocation();
			//$wwanStatus = GetWWANStatus();

			// Get captured videos.
			$kamVideos = new KamVideos($this->context);
			$videos = $kamVideos->GetVideos();

			$status = array(
					"id" => $id,
					"time" => $deviceTime,
					"status" => $deviceStatus,
					"lastknownstate" => $deviceStatus["lastknownstate"],
					"system" => $systemInfo,
					"power" => $powerInfo,
					"storage" => array("size" => $storageSize, "available" => $storageAvailable),
					"ssid" => $ssid,
					//"gislocation" => $gisLocation,
					//"wwan" => $wwanStatus,
					"videos" => $videos
					);

			DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, TRUE);
			return TRUE;
		}

		public function SetDeviceStatus(
			$key,
			$value,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "key=$key, value=$value");

			$result = FALSE;

			// File may not exist so suppress errors with @.
			$deviceStatusContents = @file_get_contents("/home/kam/data/devicestatus.json");

			if ($deviceStatusContents !== FALSE)
			{
				$deviceStatus = json_decode($deviceStatusContents, TRUE);
			}
			else
			{
				$deviceStatus = array();
			}

			$deviceStatus[$key] = $value;

			if (file_put_contents("/home/kam/data/devicestatus.json", json_encode($deviceStatus)) !== FALSE)
			{
				// If creating this file for the first time, enable all permissions.  If the file already exists,
				// the chmod may fail if the file was created under a different account.  So use @ to suppress any error messages.
				exec("sudo chmod 0777 /home/kam/data/devicestatus.json");
				$result = TRUE;
			}
			else
			{
				$resultString = serialize(error_get_last());
			}

			DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function GetDeviceStatus(
			$key,
			&$value,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__, "key=$key");

			$result = FALSE;

			$value = NULL;

			// File might not exist so use @ to suppress PHP error message.
			$deviceStatusContents = @file_get_contents("/home/kam/data/devicestatus.json");

			if ($deviceStatusContents !== FALSE)
			{
				$deviceStatus = json_decode($deviceStatusContents, TRUE);

				if ($deviceStatus != NULL)
				{
					// If key is non-NULL, return info just for that key.  Otherwise return everything.
					if ($key !== NULL)
					{
						$value = $deviceStatus[$key];
						$result = TRUE;
					}
					else
					{
						$value = $deviceStatus;
					}
				}
			}
			else
			{
				$resultString = serialize(error_get_last());
			}

			DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function ResetDevice(
			&$resultString
			)
		{
			// This API shuts down the device and so, if successful, will not return.

			if (!$this->context->local_user && ($this->context->token === NULL))
			{
				$resultString = "token required";
				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);

				return FALSE;
			}

			$kamKonfig = new KamKonfig($this->context);

			$kamKonfig->ResetDevice(
					2,     //$kamLogAction (2 = delete),
					TRUE,  //deleteWLANMac0Addr
					TRUE,  //deleteDeviceStatus
					FALSE, //$uploadFiles
					$resultString
					);

			$this->Shutdown();

			return TRUE;
		}

		public function GetKamParams(
			&$kamParams,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			if (!$this->context->local_user && ($this->context->token === NULL))
			{
				$resultString = "token required";
				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);

				return FALSE;
			}

			$result = TRUE;
			$kamParams = array();

			foreach ($this->parameterInfo as $key => &$info)
			{
				DBG_INFO(DBGZ_KAM, __METHOD__, "key=$key");

				$value = call_user_func(array("\Core\Common\Classes\Kam", $info["handlers"]["get"]));

				$kamParams[$key] = $value;
			}

			DBG_RETURN_RESULT(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function SetKamParams(
			$kamParams,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			if (!$this->context->local_user && ($this->context->token === NULL))
			{
				$resultString = "token required";
				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);

				return FALSE;
			}

			$result = FALSE;

			// Validate parameters
			$validParameters = TRUE;

			if ($kamParams == NULL)
			{
				$resultString = "Missing parameter 'kamparams'";

				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);
				$validParameters = FALSE;
			}

			if ($validParameters)
			{
				$result = TRUE;

				foreach ($kamParams as $key => &$value)
				{
					DBG_INFO(DBGZ_KAM, __METHOD__, "paramName=$key, paramValue=$value");

					if (isset($this->parameterInfo[$key]))
					{
						//
						// Note:
						//
						// Some settings have an automatic setting which, if turned, ignores the setting's value.  For example,
						// whitebalance can be explicitly set by providing a value for whitebalance.  However, if autowhitebalance
						// is on, the whitebalance setting is ignored.  And actually the camera driver will return an error if we
						// try to set whitebalance when autowhitebalance is already on.
						//
						// So we'll always apply the auto setting first (to turn it on or off).  Then, if we turned it on, we'll ignore
						// the actual setting.  And if we turned it off, we'll apply the actual setting.
						//

						$autoParameterName = $this->parameterInfo[$key]["autosetting"];

						// Check if the parameter has an auto-setting.
						if ($autoParameterName === NULL)
						{
							// No auto setting, so just apply the provided value.
							DBG_INFO(DBGZ_KAM, __METHOD__, "Applying value $value for setting $key");

							call_user_func(array("\Core\Common\Classes\Kam", $this->parameterInfo[$key]["handlers"]["set"]), intval($value));
						}
						else
						{
							//
							// Check for the value of the auto setting - if it's on (1) or 0 (off).  It's supposed to be present in the kamParams array,
							// but we'll confirm just to be sure.  And if it's not there, we'll just try to set the value.
							//
							if (isset($kamParams[$autoParameterName]))
							{
								// Apply the auto-setting - turn it on or off according to its value in kamParams.
								DBG_INFO(DBGZ_KAM, __METHOD__, "Applying auto setting $autoParameterName for setting $key");

								call_user_func(array("\Core\Common\Classes\Kam", $this->parameterInfo[$autoParameterName]["handlers"]["set"]), intval($kamParams[$autoParameterName]));

								// Now apply the actual setting value, but only if we just turned off the auto setting.
								if ($kamParams[$autoParameterName] == "0")
								{
									DBG_INFO(DBGZ_KAM, __METHOD__, "Applying value $value for setting $key after turning off auto setting $autoParameterName");
									call_user_func(array("\Core\Common\Classes\Kam", $this->parameterInfo[$key]["handlers"]["set"]), intval($value));
								}
								else
								{
									DBG_INFO(DBGZ_KAM, __METHOD__, "Ignoring value $value for setting $key because auto setting was turned on");
								}
							}
							else
							{
								DBG_WARN(DBGZ_KAM, __METHOD__, "No kamParam set for auto setting $autoParameterName");
							}
						}
					}
					else
					{
						DBG_WARN(DBGZ_KAM, __METHOD__, "Parameter $key not recognized");
					}
				}
			}

			DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function ResetKamParams(
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			if (!$this->context->local_user && ($this->context->token === NULL))
			{
				$resultString = "token required";
				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);

				return FALSE;
			}

			$result = TRUE;

			// Create an array of value name, value pairs and pass to SetKamParams.
			$kamParams = array();

			foreach ($this->parameterInfo as $key => &$info)
			{
				$kamParams[$key] = $info["values"]["default"];
			}

			$result = $this->SetKamParams($kamParams, $resultString);

			DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function GetFileContents(
			&$fileContents,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$fileContents = array(
					"kamlog" => @file_get_contents("/home/kam/data/kam.log"),
					"capturelog" => @file_get_contents("/home/kam/data/capture.log"),
					"config" => @file_get_contents("/home/kam/data/config.json"),
					"devicestatus" => @file_get_contents("/home/kam/data/devicestatus.json"),
					"videos" => @file_get_contents("/home/kam/data/videos.json")
					);

			DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, TRUE);
			return TRUE;
		}

		public function GetImage(
			&$imageContents,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			if (!$this->context->local_user && ($this->context->token === NULL))
			{
				$resultString = "token required";
				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);

				return FALSE;
			}

			$destinationDirectory = "/home/kam/data";
			$imageFilename = "image";

			$kam = new Kam(NULL);

			$result = $this->CameraCaptureImage($destinationDirectory, $imageFilename, $resultString);

			if ($result)
			{
				$imageContents = file_get_contents("{$destinationDirectory}/{$imageFilename}.jpg");

				if ($imageContents === FALSE)
				{
					$result = FALSE;
					$resultString = implode(",", error_get_last());

					DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);
				}

				if (is_file("$destinationDirectory/{$imageFilename}.jpg"))
				{
					unlink("$destinationDirectory/{$imageFilename}.jpg");
				}
			}

			DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function CameraCaptureImage(
			$destinationDirectory,
			$baseImageName,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			$result = FALSE;

			$binPath = "/home/kam/kapturrkam/bin";
			$capture = "$binPath/capture";
			$ffmpeg = "$binPath/ffmpeg";
			$imageCaptureParams = "-F -l -x VBR -b 64 -c 10 -w $destinationDirectory/$baseImageName";
			$imageFFMpegParams = "-hide_banner -v 0 -y -i {$destinationDirectory}/{$baseImageName}_0001.raw -vframes 1 -s 640x480 -f image2 {$destinationDirectory}/{$baseImageName}.jpg";

			DBG_INFO(DBGZ_KAM, __METHOD__, "$capture $imageCaptureParams");
			exec("$capture $imageCaptureParams", $execOutput, $execResult);

			if ($execResult === 0)
			{
				$result = TRUE;

				DBG_INFO(DBGZ_KAM, __METHOD__, "$ffmpeg $imageFFMpegParams");
				exec("$ffmpeg $imageFFMpegParams", $execOutput, $execResult);

				if ($execResult === 0)
				{
					if (is_file("$destinationDirectory/{$baseImageName}_0001.raw"))
					{
						unlink("$destinationDirectory/{$baseImageName}_0001.raw");
					}
				}
				else
				{
					$result = FALSE;
					$resultString = "ffmpeg failed with $execResult";

					DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);
				}
			}
			else
			{
				$result = FALSE;
				$resultString = "capture failed with $execResult";

				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);
			}

			DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}


		public function CaptureImage(
			&$imageFilename,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			if (!$this->context->local_user && ($this->context->token === NULL))
			{
				$resultString = "token required";
				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);

				return FALSE;
			}

			$result = FALSE;

			$publishPath = "/var/www/html";
			//@mkdir($publishPath, TRUE);

			$imageBaseFilename = "image";

			$result = $this->CameraCaptureImage($publishPath, $imageBaseFilename, $resultString);

			if ($result)
			{
				$imageFilename = "{$imageBaseFilename}.jpg";
			}

			DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function LiveStreamStart(
			&$playlistFilename,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			if (!$this->context->local_user && ($this->context->token === NULL))
			{
				$resultString = "token required";
				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);

				return FALSE;
			}

			//
			// Check if Live Streaming is already running and, if so, just return
			// immediately.
			//
			// if ($this->GetDeviceStatus("livestream", $value, $resultString))
			// {
			// 	// If on, the html page follows (separated by a ":")
			// 	$info = explode(":", $value);

			// 	if ($info[0] == "on")
			// 	{
			// 		$playlistFilename = $info[1];

			// 		DBG_INFO(DBGZ_KAM, __METHOD__, "Live streaming already in progress. playListFilename=$playlistFilename, value=$value");

			// 		DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, TRUE);
			// 		return TRUE;
			// 	}
			// }

			$result = FALSE;

			$pid = shell_exec("/home/kam/kapturrkam/bin/livestream.sh > /dev/null 2>&1 & echo $!");

			if ($pid)
			{
				//
				// Live Streaming should be running now.  Query the state just to confirm and grab the
				// playlist filename.
				//
				// Poll the status - up to 3 seconds before bailing out
				//
				$totalDelay = 0;

				do
				{
					if ($this->GetDeviceStatus("livestream", $value, $resultString))
					{
						// If on, the html page follows (separated by a ":")
						$info = explode(":", $value);

						if (($info[0] == "on" )|| ($info[0] == "starting"))
						{
							$playlistFilename = $info[1];

							DBG_INFO(DBGZ_KAM, __METHOD__, "Live streaming: state=".$info[0].", playlistFilename=$playlistFilename");

							$result = TRUE;
							break;
						}
						else
						{
							DBG_ERR(DBGZ_KAM, __METHOD__, "Live streaming not started (status info is $value) - delay 1/4 second");

							usleep(250000);
							$totalDelay += 250000;
						}
					}
				} while ($totalDelay < 3000000);

				if ($result)
				{
					DBG_ERR(DBGZ_KAM, __METHOD__, "Live streaming started after $totalDelay microseconds");
				}
				else
				{
					DBG_ERR(DBGZ_KAM, __METHOD__, "Live streaming failed to start after $totalDelay microseconds");
				}
			}
			else
			{
				$resultString = "Failed starting livestream.sh";
			}

			DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}

		public function LiveStreamStop(
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAM, __METHOD__);

			if (!$this->context->local_user && ($this->context->token === NULL))
			{
				$resultString = "token required";
				DBG_ERR(DBGZ_KAM, __METHOD__, $resultString);

				return FALSE;
			}

			$result = FALSE;

			if ($this->GetDeviceStatus("livestream", $value, $resultString))
			{
				//
				// Value is formatted as status:playlistname:shpid|ffmpegpid|cappid where
				//     - status:    on | off
				//     - html:      html page for viewing the live stream
				//     - shpid:     process id of livestream.sh
				//     - ffmpegpid: process id of ffmpeg process
				//     - cappid:    process id of capture process
				//
				$liveStreamData = explode(":", $value);

				if ($liveStreamData[0] !== "off")
				{
					for ($i=2; $i<count($liveStreamData); $i++)
					{
						DBG_INFO(DBGZ_KAM, __METHOD__, "Killing process ".$liveStreamData[$i]);
						exec("sudo kill -2 $liveStreamData[$i]");
					}
				}

				$this->SetDeviceStatus("livestream", "off", $resultString);
			}

			$result = TRUE;

			DBG_RETURN_BOOL(DBGZ_KAM, __METHOD__, $result);
			return $result;
		}
	}
?>
