<?php
	namespace Core\Common\Classes;

	class MethodCallContext
	{
		public $token;
		public $local_user;
		public $client_version;
		public $client_build;
		public $client_os;
		public $client_device;

		public function __construct(
			$local_user = NULL,
			$token = NULL,
			$parameters = NULL,
			$userAgentString = NULL
			)
		{
			$this->local_user = $local_user;
			$this->token = $token;

			$this->client_version = isset($parameters['_mhdr_version']) ? $parameters['_mhdr_version'] : "UNKNOWN";
			$this->client_build = isset($parameters['_mhdr_build']) ? $parameters['_mhdr_build'] : "UNKNOWN";

			$this->client_os = 'UNKNOWN';
			$this->client_device = 'UNKNOWN';

			if (isset($userAgentString) && $userAgentString)
			{
				// get client OS type
				if (strpos($userAgentString, "Win") !== false)
				{
					$this->client_os = 'win';
				}
				else if (strpos($userAgentString, "Windows NT") !== false)
				{
					$this->client_os = 'win';
				}
				else if (strpos($userAgentString, "Mac") !== false)
				{
					$this->client_os = 'mac';
				}
				else if (strpos($userAgentString, "Linux") !== false)
				{
					$this->client_os = 'lin';
				}

				// get client device type
				if (strpos($userAgentString, "WPDesktop") !== false)
				{
					$this->client_device = 'WindowsPhone';
					$this->client_os = 'win';
				}
				else if (strpos($userAgentString, "Windows Phone") !== false)
				{
					$this->client_device = 'WindowsPhone';
					$this->client_os = 'win';
				}
				else if (strpos($userAgentString, "Macintosh") !== false)
				{
					$this->client_device = 'Macintosh';
				}
				else if (strpos($userAgentString, "iPhone") !== false)
				{
					$this->client_device = 'iPhone';
					$this->client_os = 'iOS';
				}
				else if (strpos($userAgentString, "iPad") !== false)
				{
					$this->client_device = 'iPad';
					$this->client_os = 'iOS';
				}
				else if (strpos($userAgentString, "Android") !== false)
				{
					$this->client_device = 'Android';
					$this->client_os = 'lin';
				}
			}
		}
	}
?>
