<?php

	namespace Core\Common\Classes;

	require_once '/home/kam/kapturrkam/core/core.php';

	class TokenManager
	{
		private $context = NULL;
		public static function MethodCallDispatcher(
			$context,
			$methodName,
			$parameters
			)
		{
 			DBG_ENTER(DBGZ_TOKENMGR, __METHOD__, "methodName=$methodName");

			$tokenManager = new TokenManager($context);

			$response = "failed";
			$responder = "TokenManager::$methodName";
			$returnval = "failed";

			switch ($methodName)
			{
				case 'AcquireToken':
					$result = $tokenManager->AcquireToken(
							isset($parameters['pemfile']) ? rawurldecode($parameters['pemfile']) : NULL,
							$token,
							$tokenValidityPeriod,
							$resultString
							);

					if ($result)
					{
						$response = "success";
						$returnval = array(
								"token" => $token,
								"tokenvalidityperiod" => $tokenValidityPeriod
								);
					}
					else
					{
						$response = "failed";
						$returnval = array("resultstring" => $resultString);
					}

					break;

				case 'ReleaseToken':
					$result = $tokenManager->ReleaseToken(
							$resultString
							);

					if ($result)
					{
						$response = "success";
					}
					else
					{
						$response = "failed";
					}

					$returnval = array("resultstring" => $resultString);

					break;

				default:
					$response = "failed";
					$responder = "TokenManager";
					$returnval = "method not found";
					break;
			}

			DBG_INFO(DBGZ_TOKENMGR, __METHOD__, "responder=$responder, response=$response");

			$response_str = array(
					"results" => array(
							'response' => $response,
			  				'responder' => $responder,
							'returnval' => $returnval
							)
					);

			DBG_RETURN(DBGZ_TOKENMGR, __METHOD__);
			return $response_str;
		}

		public function __construct(
			$context
			)
		{
			$this->context = $context;
		}

		public static function ValidateToken(
			$token,
			&$expired
			)
		{
			DBG_ENTER(DBGZ_TOKENMGR, __METHOD__);

			$result = FALSE;

			// File might not exist so use @ to suppress PHP error message.
			$tokenInfo = json_decode(@file_get_contents("/home/kam/data/token.json"), TRUE);

			if ($tokenInfo !== NULL)
			{
				if (strcmp($token, $tokenInfo["token"]) === 0)
				{
					$result = TRUE;
				}
			}

			DBG_RETURN_BOOL(DBGZ_TOKENMGR, __METHOD__, $result);
			return $result;
		}

		public function AcquireToken(
			$pemFile,
			&$token,
			&$tokenValidityPeriod,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_TOKENMGR, __METHOD__);

			$result = FALSE;

			// Validate parameters
			$validParameters = TRUE;

			if ($pemFile === NULL)
			{
				$resultString = "Missing parameter 'pemfile'";

				DBG_ERR(DBGZ_TOKENMGR, __METHOD__, $resultString);
				$validParameters = FALSE;
			}

			if ($validParameters)
			{
				// The client key file is encrypted with a passphrase.
				$pemFileResource = openssl_pkey_get_private($pemFile, "Kanopian");

				if ($pemFileResource !== FALSE)
				{
					$details = openssl_pkey_get_details($pemFileResource);
					$clientPublicKey = $details["key"];

					$acceptedPublicKey = file_get_contents("/home/kam/.ssh/pubkey.out");

					if (strcmp($clientPublicKey, $acceptedPublicKey) === 0)
					{
						$token = md5(microtime().rand());
						$result = TRUE;

						// Store the token so it can be recalled for subsequent API calls.
						$acquiredTime = time();

						$tokenInfo = array(
								"token" => $token,
								"acquiredtime" => $acquiredTime,
								"expirationtime" => $acquiredTime + 3600
								);

						file_put_contents("/home/kam/data/token.json", json_encode($tokenInfo));
					}
					else
					{
						$resultString = "Incorrect private key";
						DBG_ERR(DBGZ_TOKENMGR, __METHOD__, $resultString);
					}
				}
				else
				{
					$resultString = "Invalid private key";
					DBG_ERR(DBGZ_TOKENMGR, __METHOD__, $resultString);
				}
			}

			DBG_RETURN_BOOL(DBGZ_TOKENMGR, __METHOD__, $result);
			return $result;
		}

		public function ReleaseToken(
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_TOKENMGR, __METHOD__);

			exec("rm -f /home/kam/data/token.json");

			DBG_RETURN_BOOL(DBGZ_TOKENMGR, __METHOD__, TRUE);
			return TRUE;
		}
	}
?>
