<?php

	namespace Core\Common\Classes;

	use \Core\Common\Classes\AWSFileManager;
	use \Core\Common\Classes\Kam;
	use \Core\Common\Classes\KamKonfig;

	require_once '/home/kam/kapturrkam/core/core.php';
	require_once '/home/kam/kapturrkam/core/DBParams.php';

	class KamVideos
	{
		private $awsFileManager = NULL;
		private $jobSiteId = NULL;
		private $deviceId = NULL;
		private $secret = NULL;

		public function UploadVideo(
			$videoName,
			$cameraLocation,
			$startDate,
			$startTime,
			$segments,
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAMVIDEOS, __METHOD__, "videoName=$videoName, cameraLocation=$cameraLocation, startDate=$startDate, startTime=$startTime, num segments=".count($segments));

			$result = FALSE;

			// Upload the files to AWS S3 bucket.  Then signal the server that they need to be ingested.

			// All files have to be copied.  If any are not copied successfully, we'll delete those that were and fail the call.
			$uploadedFiles = array();
			$uploadStartTime = time();

			foreach ($segments as &$segment)
			{
				DBG_INFO(DBGZ_KAMVIDEOS, __METHOD__, "segment filename=".$segment["filename"].", duration=".$segment["duration"].", sizeondisk=".$segment["sizeondisk"]);

				// Use the device id to ensure a unique name.
				$bucketFilename = $this->deviceId."_".basename($segment["filename"]);

				$result = $this->awsFileManager->UploadFile($segment["filename"], $bucketFilename, "public-read", true, $resultString);

				if ($result)
				{
					$uploadedFiles[] = $bucketFilename;
				}
				else
				{
					DBG_ERR(DBGZ_KAMVIDEOS, __METHOD__, "Failed uploading segment with resultString=$resultString");
					break;
				}
			}

			if (count($uploadedFiles) == count($segments))
			{
				// All files were uploaded successfully.  Mark them as such and call the server.
				$uploadTime = time() - $uploadStartTime;

				$urlString = "https://collect.idaxdata.com/MethodCall.php/VideoJobSite::VideoFilesUploadedToBucket";
				$urlString .= "?jobsiteid=$this->jobSiteId";
				$urlString .= "&deviceid=$this->deviceId";
				$urlString .= "&secret=$this->secret";
				$urlString .= "&name=".urlencode($videoName);
				$urlString .= "&cameralocation=".urlencode($cameraLocation);
				$urlString .= "&capturestarttime=".urlencode("$startDate $startTime");
				$urlString .= "&uploadtime=$uploadTime";

				$fileIndex = 0;

				foreach ($uploadedFiles as &$uploadedFile)
				{
					$urlString .= "&bucketfilename_{$fileIndex}=$uploadedFile";
					$fileIndex += 1;
				}

				DBG_INFO(DBGZ_KAMVIDEOS, __METHOD__, "urlString=$urlString");

				$contents = file_get_contents($urlString);

				if ($contents !== FALSE)
				{
					$response = json_decode($contents, TRUE);

					if ($response["results"]["response"] == "success")
					{
						$result = TRUE;
					}
					else
					{
						$result = FALSE;

						DBG_ERR(DBGZ_KAMVIDEOS, __METHOD__, "VideoJobSite::VideoFilesUploadedToBucket failed with resultString=".$response["results"]["returnval"]["resultstring"]);
					}
				}
			}
			else
			{
				// Last file upload failed.  Delete those that succeeded.
				foreach ($uploadedFiles as &$uploadedFile)
				{
					$awsFileManager->DeleteFile($uploadedFile);
				}
			}

			DBG_RETURN_BOOL(DBGZ_KAMVIDEOS, __METHOD__, $result);
			return $result;
		}

		public function UploadVideos(
			&$resultString
			)
		{
			DBG_ENTER(DBGZ_KAMVIDEOS, __METHOD__);

			$kamKonfig = new KamKonfig(new MethodCallContext(TRUE, NULL, NULL, NULL));

			$result = $kamKonfig->GetConfig($serverTimeStamp, $secret, $job, $jobSite, $device, $deployedDevice, $resultString);

			if ($result)
			{
				$this->jobSiteId = $jobSite["jobsiteid"];
				$this->deviceId = $device["deviceid"];
				$this->secret = $deployedDevice["secret"];
				$cameraLocation = $deployedDevice["cameralocation"];

				$videos = $this->GetVideos();
				$numVideos = count($videos);
				$numVideosUploaded = 0;

				$this->awsFileManager = new AWSFileManager(IDAX_VIDEOFILES_BUCKET, AWSREGION, AWSKEY, AWSSECRET);

				$videoNumber = 1;

				foreach ($videos as $basename => &$video)
				{
					// Don't upload videos whose video capture state is not complete.
					$videoCaptureState = $video["capturestate"];

					if ($videoCaptureState != "complete")
					{
						DBG_INFO(DBGZ_KAMVIDEOS, __METHOD__, "Skipping video $basename because videoCaptureState=$videoCaptureState");
						continue;
					}

					// Don't upload a video that's already been uploaded or is currently uploading.
					if (isset($video["uploadstate"]))
					{
						$uploadState = $video["uploadstate"];

						if ($uploadState == "uploaded")
						{
							$numVideosUploaded += 1;

							DBG_INFO(DBGZ_KAMVIDEOS, __METHOD__, "Skipping video $basename because uploadState=$uploadState)");
							continue;
						}
					}

					//
					// get startDate and starttTme which are part of the filename.
					//
					// Note that the basename may have _ so work from the back of the exploded array to get the startdate and starttime components.
					//
					$fileParts = explode("_", $basename);

					$startDate = $fileParts[count($fileParts) - 2];
					$startTime = substr($fileParts[count($fileParts) - 1], 0, 2).":".substr($fileParts[count($fileParts) - 1], 2, 2).":00";

					//
					// videoName is formatted as "basename_videoNumber".
					//
					$videoName = $job["office"].": ".$job["nickname"]." ".$jobSite["sitecode"]."_".str_pad($videoNumber, 2, "0", STR_PAD_LEFT);

					$video["uploadstate"] = "uploading";

					// Save the new state.
					$this->SaveVideos($videos);

					$result = $this->UploadVideo(
							$videoName,
							$cameraLocation,
							$startDate,
							$startTime,
							$video["segments"],
							$resultString
							);

					if ($result)
					{
						$video["uploadstate"] = "uploaded"; //date("Y-m-d H:i:s");

						$numVideosUploaded += 1;

						DBG_INFO(DBGZ_KAMVIDEOS, __METHOD__, "UploadVideo video $videoName succeeded");
					}
					else
					{
						$video["uploadstate"] = "failed"; //date("Y-m-d H:i:s");
						DBG_ERR(DBGZ_KAMVIDEOS, __METHOD__, "UploadVideo $videoName failed with resultString=$resultString");
					}

					// Save the new state.
					$this->SaveVideos($videos);

					$videoNumber += 1;
				}

				if ($numVideosUploaded == $numVideos)
				{
					// All videos uploaded.  Nothing else to do, so let's reset the system and then shut it down.
					$kamKonfig->ResetDevice(
							1,     //kamLogAction (1=truncate)
							FALSE, //deleteDeviceStatus
							TRUE,  //uploadVideos
							$resultString
							);

					$kam = new Kam(new MethodCallContext(TRUE, NULL, NULL, NULL));

					// Set the last known good state to indicate that all videos have been uploaded.
					$kam->SetDeviceStatus("lastknownstate", "videouploadcomplete:".time(), $resultString);

					$kam->Shutdown($resultString);
				}
			}

			DBG_RETURN_BOOL(DBGZ_KAMVIDEOS, __METHOD__, $result);
			return $result;
		}

		public function BeginCaptureVideo(
			$name,
			$duration
			)
		{
			DBG_ENTER(DBGZ_KAMVIDEOS, __METHOD__, "name=$name, duration=$duration");

			// Get the current set of videos.
			$videos = $this->GetVideos();

			// Add the new one
			$videos[$name] = array("starttime" => date("Y-m-d H:i:s"), "capturestate" => "in progress", "duration" => $duration);

			// Save the new set.
			$result = $this->SaveVideos($videos);

			DBG_RETURN_BOOL(DBGZ_KAMVIDEOS, __METHOD__, $result);
			return $result;
		}

		public function AddSegment(
			$name,
			$filename,
			$duration
			)
		{
			DBG_ENTER(DBGZ_KAMVIDEOS, __METHOD__, "name=$name, filename=$filename, duration=$duration");

			$result = FALSE;

			// Get the current set of videos.
			$videos = $this->GetVideos();

			// Update the named video
			if (isset($videos[$name]))
			{
				if (!isset($videos[$name]["segments"]))
				{
					$videos[$name]["segments"] = [];
				}

				$videos[$name]["segments"][] = array("filename" => $filename, "sizeondisk" => filesize($filename), "duration" => $duration);

				// Save the updated info.
				$result = $this->SaveVideos($videos);
			}
			else
			{
				DBG_ERR(DBGZ_KAMVIDEOS, __METHOD__, "Video $name not found");
			}

			DBG_RETURN_BOOL(DBGZ_KAMVIDEOS, __METHOD__, $result);
			return $result;
		}

		public function CompleteCaptureVideo(
			$name,
			$completionStatus
			)
		{
			DBG_ENTER(DBGZ_KAMVIDEOS, __METHOD__, "name=$name, completionStatus=$completionStatus");

			$result = FALSE;

			// Get the current set of videos.
			$videos = $this->GetVideos();

			// Update the named video
			if (isset($videos[$name]))
			{
				$videos[$name]["endtime"] = date("Y-m-d H:i:s");
				$videos[$name]["capturestate"] = "complete";
				$videos[$name]["capturecompletionstatus"] = $completionStatus;

				// Save the updated info.
				$result = $this->SaveVideos($videos);
			}
			else
			{
				DBG_ERR(DBGZ_KAMVIDEOS, __METHOD__, "Video $name not found");
			}

			DBG_RETURN_BOOL(DBGZ_KAMVIDEOS, __METHOD__, $result);
			return $result;
		}

		public function BeginTransferVideo(
			$name
			)
		{
			DBG_ENTER(DBGZ_KAMVIDEOS, __METHOD__, "name=$name");

			$result = FALSE;

			// Get the current set of videos.
			$videos = $this->GetVideos();

			// Update the named video
			if (isset($videos[$name]))
			{
				$videos[$name]["transferstarttime"] = date("Y-m-d H:m:s");
				$videos[$name]["transferstate"] = "in progress";

				// Save the updated info.
				$result = $this->SaveVideos($videos);
			}
			else
			{
				DBG_ERR(DBGZ_KAMVIDEOS, __METHOD__, "Video $name not found");
			}

			DBG_RETURN_BOOL(DBGZ_KAMVIDEOS, __METHOD__, $result);
			return $result;
		}

		public function CompleteTransferVideo(
			$name,
			$completionStatus
			)
		{
			DBG_ENTER(DBGZ_KAMVIDEOS, __METHOD__, "name=$name, completionStatus=$completionStatus");

			$result = FALSE;

			// Get the current set of videos.
			$videos = $this->GetVideos();

			// Update the named video
			if (isset($videos[$name]))
			{
				$videos[$name]["transferstate"] = "complete";
				$videos[$name]["transfercompletetime"] = date("Y-m-d H:m:s");
				$videos[$name]["transfercompletionstatus"] = $completionStatus;

				// Save the updated info.
				$result = $this->SaveVideos($videos);
			}
			else
			{
				DBG_ERR(DBGZ_KAMVIDEOS, __METHOD__, "Video $name not found");
			}

			DBG_RETURN_BOOL(DBGZ_KAMVIDEOS, __METHOD__, $result);
			return $result;
		}

		public function SaveVideos(
			$videos
			)
		{
			DBG_ENTER(DBGZ_KAMVIDEOS, __METHOD__);

			$result = FALSE;

			$fileContents = json_encode($videos);

			if ($fileContents !== FALSE)
			{
				$result = (FALSE !== file_put_contents("/home/kam/data/videos.json", $fileContents));

				if ($result)
				{
					// Make sure everyone can access the file.
					exec("sudo chmod 0777 /home/kam/data/videos.json");
				}
			}

			DBG_RETURN_BOOL(DBGZ_KAMVIDEOS, __METHOD__, $result);
			return $result;
		}

		public function GetVideos()
		{
			DBG_ENTER(DBGZ_KAMVIDEOS, __METHOD__);

			$videos = [];

			if (is_file("/home/kam/data/videos.json"))
			{
				$jsonDecodedVideos = json_decode(file_get_contents("/home/kam/data/videos.json"), TRUE);

				if ($jsonDecodedVideos !== NULL)
				{
					$videos = $jsonDecodedVideos;
				}
			}
			else
			{
				DBG_ERR(DBGZ_KAMVIDEOS, __METHOD__, "Missing file /home/kam/data/videos.json");
			}

			DBG_RETURN(DBGZ_KAMVIDEOS, __METHOD__);
			return $videos;
		}

		public function GetVideo(
			$name
			)
		{
			DBG_ENTER(DBGZ_KAMVIDEOS, __METHOD__, "name=$name");

			$videos = $this->GetVideos();

			if (isset($videos[$name]))
			{
				return $videos[$name];
			}

			DBG_WARN(DBGZ_KAMVIDEOS, __METHOD__, "Video $name not found");

			DBG_RETURN(DBGZ_KAMVIDEOS, __METHOD__);
			return NULL;
		}

		function DeleteVideos()
		{
			DBG_ENTER(DBGZ_KAMVIDEOS, __METHOD__);

			// Clean up videos
			$files = glob("/home/kam/data/*.mp4");

			foreach ($files as &$file)
			{
				exec("sudo rm -f $file");
			}

			exec("sudo rm -f /home/kam/data/videos.json");

			DBG_RETURN(DBGZ_KAMVIDEOS, __METHOD__);
			return NULL;
		}
	}
?>
