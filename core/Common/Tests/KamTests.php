<?php

	use \Core\Common\Classes\MethodCallContext;
	use \Core\Common\Classes\Kam;

	require_once '/home/kam/kapturrkam/core/core.php';
	require_once '/home/kam/kapturrkam/core/autoload.php';
	require_once '/home/kam/kapturrkam/core/LocalMethodCall.php';

	function DoGetImageTest()
	{
		DBG_ENTER(DBGZ_APP, __FUNCTION__);

		$kam = new Kam(new MethodCallContext(TRUE, NULL, NULL));
		$kam->GetImage($imageContents, $resultString);

		DBG_INFO(DBGZ_APP, __FUNCTION__, "imageContents=".base64_encode($imageContents));

		DBG_RETURN(DBGZ_APP, __FUNCTION__);
	}

	function DoCaptureImageTest()
	{
		DBG_ENTER(DBGZ_APP, __FUNCTION__);

		$kam = new Kam(new MethodCallContext(TRUE, NULL, NULL));
		$kam->CaptureImage($imageFilename, $resultString);

		DBG_INFO(DBGZ_APP, __FUNCTION__, "imageFilename=$imageFilename");

		DBG_RETURN(DBGZ_APP, __FUNCTION__);
	}

	function DoStartLiveStreamTest()
	{
		DBG_ENTER(DBGZ_APP, __FUNCTION__);

		$kam = new Kam(new MethodCallContext(TRUE, NULL, NULL));
		$kam->LiveStreamStart($playlistFilename, $resultString);

		DBG_INFO(DBGZ_APP, __FUNCTION__, "playlistFilename=$playlistFilename");

		DBG_RETURN(DBGZ_APP, __FUNCTION__);
	}

	function DoStopLiveStreamTest()
	{
		DBG_ENTER(DBGZ_APP, __FUNCTION__);

		$kam = new Kam(new MethodCallContext(TRUE, NULL, NULL));
		$kam->LiveStreamStop($resultString);

		DBG_RETURN(DBGZ_APP, __FUNCTION__);
	}

	function DoSetDateTimeTest(
		$date,
		$time
		)
	{
		DBG_ENTER(DBGZ_APP, __FUNCTION__, "date=$date, time=$time");

		$kam = new Kam(new MethodCallContext(TRUE, NULL, NULL));
		$kam->SetDateTime($date, $time, $resultString);

		DBG_RETURN(DBGZ_APP, __FUNCTION__);
	}

	function DoGetStatusTest()
	{
		DBG_ENTER(DBGZ_APP, __FUNCTION__);

		$kam = new Kam(new MethodCallContext(TRUE, NULL, NULL));
		$result = $kam->GetStatus($status, $resultString);

		var_dump($status);

		DBG_RETURN(DBGZ_APP, __FUNCTION__);
	}

	DBG_SET_PARAMS(DBGZ_APP | DBGZ_KAM, DBGL_TRACE | DBGL_INFO | DBGL_ERR | DBGL_WARN, FALSE, FALSE, dbg_dest_terminal);
	//DBG_SET_PARAMS(0, 0, FALSE, FALSE, dbg_dest_terminal);

	if (isset($argv[1]))
	{
		if ($argv[1] == '-ls')
		{
			DoStartLiveStreamTest();
		}
		else if ($argv[1] == '-kls')
		{
			DoStopLiveStreamTest();
		}
		else if ($argv[1] == '-gi')
		{
			DoGetImageTest();
		}
		else if ($argv[1] == '-ci')
		{
			DoCaptureImageTest();
		}
		else if ($argv[1] == '-sd')
		{
			DoSetDateTimeTest($argv[2], $argv[3]);
		}
		else if ($argv[1] == '-gs')
		{
			DoGetStatusTest();
		}
	}
	else
	{
		echo "Usage: ".$argv[0]." [-ls | | -kls | -ci]\n";
	}
?>
