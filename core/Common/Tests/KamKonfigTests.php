<?php

	use \Core\Common\Classes\MethodCallContext;
	use \Core\Common\Classes\KamKonfig;

	require_once '/home/kam/kapturrkam/core/core.php';
	require_once '/home/kam/kapturrkam/core/autoload.php';

	function DoBrightnessTest(
		$brightnessLevel
		)
	{
		DBG_ENTER(DBGZ_APP, __FUNCTION__);

		$kamKonfig = new \Core\Common\Classes\KamKonfig(NULL);

		$value = $kamKonfig->getBrightness();

		if ($value === FALSE)
		{
			DBG_INFO(DBGZ_APP, __FUNCTION__, "getBrightness failed");
		}
		else
		{
			DBG_INFO(DBGZ_APP, __FUNCTION__, "getBrightness returned $value");
		}

		$result = $kamKonfig->setBrightness(intval($brightnessLevel, 10));

		if ($result)
		{
			DBG_ERR(DBGZ_APP, __FUNCTION__, "setBrightness failed with $result");
		}

		$value = $kamKonfig->getBrightness();

		if ($value === FALSE)
		{
			DBG_INFO(DBGZ_APP, __FUNCTION__, "getBrightness failed");
		}
		else
		{
			DBG_INFO(DBGZ_APP, __FUNCTION__, "getBrightness returned $value");
		}

		DBG_RETURN(DBGZ_APP, __FUNCTION__);
	}

	function DoContrastTest(
		$contrastLevel
		)
	{
		DBG_ENTER(DBGZ_APP, __FUNCTION__);

		$kamKonfig = new \Core\Common\Classes\KamKonfig(NULL);

		$value = $kamKonfig->getContrast();

		if ($value === FALSE)
		{
			DBG_INFO(DBGZ_APP, __FUNCTION__, "getContrast failed");
		}
		else
		{
			DBG_INFO(DBGZ_APP, __FUNCTION__, "getContrast returned $value");
		}

		$result = $kamKonfig->setContrast(intval($contrastLevel, 10));

		if ($result)
		{
			DBG_ERR(DBGZ_APP, __FUNCTION__, "setContrast failed with $result");
		}

		$contrast = $kamKonfig->getContrast();

		if ($contrast === FALSE)
		{
			DBG_INFO(DBGZ_APP, __FUNCTION__, "getContrast failed");
		}
		else
		{
			DBG_INFO(DBGZ_APP, __FUNCTION__, "getContrast returned $value");
		}

		DBG_RETURN(DBGZ_APP, __FUNCTION__);
	}

	function DoSaturationTest(
		$saturationLevel
		)
	{
		DBG_ENTER(DBGZ_APP, __FUNCTION__);

		$kamKonfig = new \Core\Common\Classes\KamKonfig(NULL);
		$kamKonfig->setSaturation(intval($saturationLevel, 10));

		DBG_RETURN(DBGZ_APP, __FUNCTION__);
	}

	function DoGainTest(
		$gainLevel
		)
	{
		DBG_ENTER(DBGZ_APP, __FUNCTION__);

		$kamKonfig = new \Core\Common\Classes\KamKonfig(NULL);
		$kamKonfig->setGain(intval($gainLevel, 10));

		DBG_RETURN(DBGZ_APP, __FUNCTION__);
	}

	function DoWhiteBalanceTest(
		$whiteBalanceTemperature
		)
	{
		DBG_ENTER(DBGZ_APP, __FUNCTION__);

		$kamKonfig = new \Core\Common\Classes\KamKonfig(NULL);
		$kamKonfig->setWhiteBalanceTemperature(intval($whiteBalanceTemperature, 10));

		DBG_RETURN(DBGZ_APP, __FUNCTION__);
	}

	function DoSharpnessTest(
		$sharpnesssLevel
		)
	{
		DBG_ENTER(DBGZ_APP, __FUNCTION__);

		$kamKonfig = new \Core\Common\Classes\KamKonfig(NULL);
		$kamKonfig->setSharpness(intval($sharpnesssLevel, 10));

		DBG_RETURN(DBGZ_APP, __FUNCTION__);
	}

	function DoSetParamsTest()
	{
		$kamParams = array(
				"brightness" => 75,
				"contrast" => 165,
				"saturation" => 212,
				"gain" => 39,
				"autowhitebalance" => 0,
				"whitebalance" => 5550,
				"sharpness" => 189,
				"backlight" => 1,
				"absoluteexposure" => 1800,
				"exposurepriority" => 0,
				"pan" => -18000,
				"tilt" => 18000,
				"autofocus" => 0,
				"focus" => 45,
				"zoom" => 275
				);

		$kamKonfig = new \Core\Common\Classes\KamKonfig(NULL);
		$kamKonfig->SetKamParams($kamParams, $resultString);
	}

	function DoGetParamsTest()
	{
		$kamKonfig = new \Core\Common\Classes\KamKonfig(NULL);
		$kamKonfig->GetKamParams($kamParams, $resultString);

		var_dump($kamParams);
	}

	function DoResetParamsTest()
	{
		$kamKonfig = new \Core\Common\Classes\KamKonfig(NULL);
		$kamKonfig->ResetKamParams($resultString);
		$kamKonfig->GetKamParams($kamParams, $resultString);

		var_dump($kamParams);
	}

	function DoSaveConfigTest()
	{
		$kamKonfig = new \Core\Common\Classes\KamKonfig(new MethodCallContext(TRUE, NULL, NULL, NULL));

		$secret = "d0965acd348b274e60bf334ff3500a6e"; //md5(microtime().rand());

		// Get the Device ID (use the device MAC address)
		exec("/sbin/ifconfig eth0", $outputLines, $retval);

		if ($retval == 0)
		{
			// Format for the first line of output is "eth0    Link encap:Ethernet  HWaddr xx:xx:xx:xx:xx:xx
			$outputLineFields = explode(" ", $outputLines[0]);
			$macAddress = $outputLineFields[count($outputLineFields) - 1];

			$deviceId = str_replace(":", "", $macAddress);

			$kamKonfig->SaveConfig(
					time(),   // Server time stamp
					$secret,
					array("jobid" =>  69, "number" => "KKBETA", "name" => "KapturrKam Beta", "nickname" => "KAPT", "office" => "WA"),
					array("jobsiteid" => 177, "sitecode" => "KK"),
					array("deviceid" => $deviceId),
					array(
							"secret" => $secret,
							"jobsiteid" => 177,
							"deviceid" => $deviceId,
							"timezone" => "UTC",
							"cameralocation" => "North",
							"config" => array(
									"dateranges" => array(array("2016-09-12", "2016-09-13")),
									"timeranges" => array(array("07:00", "08:00"), array("17:00", "18:00")),
									"kamparams" => array("brightness" => 64),
									)
							),
					FALSE,
					$resultString
					);
		}
		else
		{
			echo "Failed to get device id - retval=$retval\n";
		}
	}

	function DoGetConfigTest()
	{
		$kamKonfig = new \Core\Common\Classes\KamKonfig(NULL);
		$kamKonfig->GetConfig($jobDetails, $schedule, $kamParams, $resultString);

		var_dump($jobDetails);
		var_dump($schedule);
		var_dump($kamParams);
	}

	function DoResetDeviceTest()
	{
		$kamKonfig = new \Core\Common\Classes\KamKonfig(new MethodCallContext(TRUE, NULL, NULL, NULL));
		$result = $kamKonfig->ResetDevice(TRUE, $resultString);
		var_dump($result);
		var_dump($resultString);
	}

	DBG_SET_PARAMS(DBGZ_APP | DBGZ_KAMKONFIG, DBGL_TRACE | DBGL_INFO | DBGL_ERR | DBGL_WARN, FALSE, FALSE, dbg_dest_terminal);
	//DBG_SET_PARAMS(0, 0, FALSE, FALSE, dbg_dest_terminal);

	if (isset($argv[1]))
	{
		if ($argv[1] == '-gp')
		{
			DoGetParamsTest();
		}
		else if ($argv[1] == '-sp')
		{
			DoSetParamsTest();
		}
		else if ($argv[1] == '-rp')
		{
			DoResetParamsTest();
		}
		else if ($argv[1] == '-b')
		{
			DoBrightnessTest($argv[2]);
		}
		else if ($argv[1] == '-c')
		{
			DoContrastTest($argv[2]);
		}
		else if ($argv[1] == '-sc')
		{
			DoSaveConfigTest();
		}
		else if ($argv[1] == '-gc')
		{
			DoGetConfigTest();
		}
		else if ($argv[1] == '-rd')
		{
			DoResetDeviceTest();
		}
	}
	else
	{
		echo "Usage: ".$argv[0]." [-gp | -sp | -rp | -b level | -c level | -sc | -gc]\n";
	}
?>
