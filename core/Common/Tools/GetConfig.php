<?php

	use \Core\Common\Classes\MethodCallContext;
	use \Core\Common\Classes\KamKonfig;

	require_once '/home/kam/kapturrkam/core/core.php';
	require_once '/home/kam/kapturrkam/core/autoload.php';

	//DBG_SET_PARAMS(DBGZ_APP | DBGZ_KAMKONFIG, DBGL_TRACE | DBGL_INFO | DBGL_ERR | DBGL_WARN, FALSE, FALSE, dbg_dest_terminal);
	DBG_SET_PARAMS(0, 0, FALSE, FALSE, NULL);

	$kamKonfig = new KamKonfig(new MethodCallContext(TRUE, NULL, NULL, NULL));

	if ($argc == 4)
	{
		$jobSiteId = $argv[1];
		$deviceId = $argv[2];
		$secret = $argv[3];
		$serverTimeStamp = "0000-00-00 00:00:00";
	}
	else
	{
		$result = $kamKonfig->GetConfig($serverTimeStamp, $secret, $job, $jobSite, $device, $deployedDevice, $resultString);

		$jobSiteId = $deployedDevice["jobsiteid"];
		$deviceId = $deployedDevice["deviceid"];
	}

	$contents = file_get_contents("https://collect.idaxdata.com/MethodCall.php/JobSite::GetDeployedDeviceInfo?jobsiteid=$jobSiteId&infolevel=3&deviceid=$deviceId&secret=$secret&lastupdatetime=$serverTimeStamp");

	if ($contents !== FALSE)
	{
		$response = json_decode($contents, true);

		if ($response["results"]["response"] == "success")
		{
			$returnval = $response["results"]["returnval"];

			$serverTimeStamp = $returnval["servertimestamp"];
			$job = $returnval["job"];
			$jobSite = $returnval["jobsite"];
			$device = $returnval["device"];
			$deployedDevice = $returnval["deployeddevice"];
		}

		$kamKonfig->SaveConfig($serverTimeStamp, $secret, $job, $jobSite, $device, $deployedDevice, $resultString);
	}
?>
