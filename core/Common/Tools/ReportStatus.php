<?php

	use \Core\Common\Classes\MethodCallContext;
	use \Core\Common\Classes\Kam;
	use \Core\Common\Classes\KamKonfig;

	require_once '/home/kam/kapturrkam/core/core.php';
	require_once '/home/kam/kapturrkam/core/autoload.php';

	$batteryShutdownThreshold = 10.8;               // Volts
	$batteryCriticalThreshold = 11;                 // Volts
	$batteryWarningThreshold = 11.8;                // Volts
	$storageCriticalThreshold = 100 * 1024 * 1024;  // 100 MB
	$storageWarningThreshold  = 500 * 1024 * 1024;  // 500 MB
	$gisLocationChangeThreshold = 10;               // meters

	function GetHaversineDistance(
		$lat1,
		$long1,
		$lat2,
		$long2
		)
	{
		// Ripped from http://www.movable-type.co.uk/scripts/latlong.html

		// formula:	a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
		// c = 2 ⋅ atan2( √a, √(1−a) )
		// d = R ⋅ c

		$R = 6371000; // metres
		$phi1 = deg2rad($lat1);
		$phi2 = deg2rad($lat2);
		$deltaPhi = deg2rad($lat2-$lat1);
		$deltaLamba = deg2rad($long2-$long1);

		$a = sin($deltaPhi / 2) * sin($deltaPhi / 2) + cos($phi1) * cos($phi2) * sin($deltaLamba/2) * sin($deltaLamba/2);
		$c = 2 * atan2(sqrt($a), sqrt(1-$a));

		$d = $R * $c;

		return $d;
	}

	function SendNotification(
		$notificationType,
		$notificationDescription,
		$job,
		$jobSite,
		$device,
		$deployedDevice
		)
	{
		DBG_ENTER(DBGZ_APP, __FUNCTION__, "type=$notificationType, notificationDescription=$notificationDescription");

		$notificationInfo = $deployedDevice["config"]["notificationinfo"];

		if (in_array($type, $notificationInfo["notifications"]))
		{
			$jobSiteId = $deployedDevice["jobsiteid"];
			$cameraLocation = $deployedDevice["cameralocation"];
			$siteCode = $jobSite["sitecode"];
			$description = $jobSite["description"];
			$deviceId = $deployedDevice["deviceid"];

			$email = $notificationInfo["contact"]["email"];
			$name = $notificationInfo["contact"]["name"];
			$phone = $notificationInfo["contact"]["phone"];

			DBG_INFO(DBGZ_APP, __FILE__, "email=$email, name=$name, phone=$phone");
		}

		DBG_RETURN(DBGZ_APP, __FUNCTION__);
	}

	DBG_SET_PARAMS(DBGZ_APP | DBGZ_KAM | DBGZ_KAMKONFIG, DBGL_TRACE | DBGL_INFO | DBGL_ERR | DBGL_WARN, FALSE, FALSE, dbg_dest_log, dbg_file);
	DBG_SET_PARAMS(0, 0, FALSE, FALSE, NULL);

	DBG_ENTER(DBGZ_APP, "ReportStatus", "user=".posix_getpwuid(posix_geteuid())["name"]);

	//
	// Get command line parameters
	//
	// Usage is: ReportStatus.php [-e event] [-s status] [-i] [-l logfile] [-s]
	//    -e event: the event (or reason) to be logged.
	//    -s status: the status to be recorded
	//    -l: if present, indicates the event and status should be logged in a local log file (filename is the value for -l)
	//    -s: if present, indicates the event and status should be reported to the server.
	//    -i: if present, indicates an image from the camera should be included with the status (only used if sending to server).
	//
	$commandLineOptions = getopt("e:s:l:ri");

	$logToFile = isset($commandLineOptions["l"]);
	$reportToServer = isset($commandLineOptions["r"]);
	$transmitImage = isset($commandLineOptions["i"]);

	$kam = new Kam(new MethodCallContext(TRUE, NULL, NULL, NULL));

	if (isset($commandLineOptions["s"]))
	{
		// Format of status info should be statusname:value
		$statusInfo = explode(":", $commandLineOptions["s"], 2);
		$kam->SetDeviceStatus($statusInfo[0], $statusInfo[1], $resultString);
	}

	if ($logToFile || $reportToServer)
	{
		$result = $kam->GetStatus($status, $resultString);

		if ($result)
		{
			$event = isset($commandLineOptions["e"]) ? $commandLineOptions["e"] : "unknown";

			if ($logToFile)
			{
				$logFile = fopen($commandLineOptions["l"], 'a');

				// If creating this file for the first time, enable all permissions.  If the file already exists,
				// the chmod may fail if the file was created under a different account.  So use @ to suppress any error messages.
				@chmod($commandLineOptions["l"], 0777);

				if ($logFile !== FALSE)
				{
					// The status includes captured video files.  We don't want to log those in the log file.
					unset($status["videos"]);

					fwrite($logFile, json_encode(array("event" => $event, "status" => $status)));
					fclose($logFile);
				}
			}

			if ($reportToServer)
			{
				// Get job site details from the config.
				$kamKonfig = new KamKonfig(NULL);

				$result = $kamKonfig->GetConfig($serverTimeStamp, $secret, $job, $jobSite, $device, $deployedDevice, $resultString);

				$jobSiteId = $deployedDevice["jobsiteid"];
				$deviceId = $deployedDevice["deviceid"];

				$params = array(
						'jobsiteid' => $jobSiteId,
						"deviceid" => $deviceId,
						"secret" => $secret,
						"event" => $event,
						"status" => $status
						);

				if ($transmitImage)
				{
					$destinationDirectory = "/home/kam/data";
					$imageFilename = "status_image";

					@mkdir($destinationDirectory, TRUE);

					$result = $kam->CameraCaptureImage($destinationDirectory, $imageFilename, $resultString);

					if ($result)
					{
						$image = base64_encode(file_get_contents("{$destinationDirectory}/{$imageFilename}.jpg"));

						unlink("$destinationDirectory/{$imageFilename}.jpg");

						$params["image"] = $image;
					}
				}

				// use key 'http' even if you send the request to https://...
				$options = array(
						'http' => array(
								'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
								'method'  => 'POST',
								'content' => http_build_query($params)
								)
						);

				DBG_INFO(DBGZ_APP, "ReportStatus", "Reporting status to server");

				$context  = stream_context_create($options);
				$contents = file_get_contents("https://collect.idaxdata.com/MethodCall.php/JobSite::ReportDeployedDeviceStatus", false, $context);

				if ($contents !== FALSE)
				{
					$response = json_decode($contents, true);

					$results = $response["results"];

					if ($results["response"] == "success")
					{
						DBG_INFO(DBGZ_APP, "ReportStatus", "JobSite::ReportDeployeDeviceStatus succeeded.  Saving and applying new settings.");

						$returnval = $response["results"]["returnval"];

						$serverTimeStamp = $returnval["servertimestamp"];
						$job = $returnval["job"];
						$jobSite = $returnval["jobsite"];
						$device = $returnval["device"];
						$deployedDevice = $returnval["deployeddevice"];

						$kamKonfig->SaveConfig($serverTimeStamp, $secret, $job, $jobSite, $device, $deployedDevice, $resultString);
					}
					else
					{
						DBG_ERR(DBGZ_APP, "ReportStatus", "JobSite::ReportDeployeDeviceStatus failed with resultstring=".$results["returnval"]["resultstring"]);
					}
				}
				else
				{
					DBG_ERR(DBGZ_APP, "ReportStatus", "Failed to report status");
				}
			}
		}
	}

	DBG_RETURN(DBGZ_APP, "ReportStatus");
?>
