<?php

	use \Core\Common\Classes\MethodCallContext;
	use \Core\Common\Classes\KamKonfig;

	require_once '/home/kam/kapturrkam/core/core.php';
	require_once '/home/kam/kapturrkam/core/autoload.php';

	DBG_SET_PARAMS(DBGZ_APP | DBGZ_KAMKONFIG, DBGL_TRACE | DBGL_INFO | DBGL_ERR | DBGL_WARN, FALSE, FALSE, dbg_dest_terminal);
	//DBG_SET_PARAMS(0, 0, FALSE, FALSE, NULL);

	$kamKonfig = new KamKonfig(new MethodCallContext(TRUE, NULL, NULL, NULL));

	$result = $kamKonfig->GetConfig($serverTimeStamp, $secret, $job, $jobSite, $device, $deployedDevice, $resultString);

	if ($result)
	{
		$kamKonfig->ScheduleCRONJobs($deployedDevice);
	}
?>
