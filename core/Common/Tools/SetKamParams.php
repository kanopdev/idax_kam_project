<?php

	use \Core\Common\Classes\MethodCallContext;
	use \Core\Common\Classes\Kam;
	use \Core\Common\Classes\KamKonfig;

	require_once '/home/kam/kapturrkam/core/core.php';
	require_once '/home/kam/kapturrkam/core/autoload.php';

	DBG_SET_PARAMS(DBGZ_APP | DBGZ_KAM | DBGZ_KAMKONFIG, DBGL_TRACE | DBGL_INFO | DBGL_ERR | DBGL_WARN, FALSE, FALSE, dbg_dest_log, dbg_file);
	//DBG_SET_PARAMS(0, 0, FALSE, FALSE, NULL);

	// First reset all parameters to defaults
	$kam = new Kam(new MethodCallContext(TRUE, NULL, NULL, NULL));

	$kam->ResetKamParams($resultString);

	// Now set configured parameters
	$kamKonfig = new KamKonfig(new MethodCallContext(TRUE, NULL, NULL, NULL));

	$kamKonfig->ApplyKamParams("default", $resultString);
?>
