<?php

	use \Core\Common\Classes\MethodCallContext;
	use \Core\Common\Classes\KamVideos;
	use \Core\Common\Classes\KamKonfig;

	require_once '/home/kam/kapturrkam/core/core.php';
	require_once '/home/kam/kapturrkam/core/autoload.php';

	DBG_SET_PARAMS(DBGZ_APP | DBGZ_KAMVIDEOS | DBGZ_KAMKONFIG | DBGZ_AWSFILEMGR | DBGZ_KAM, DBGL_TRACE | DBGL_INFO | DBGL_ERR | DBGL_WARN, FALSE, FALSE, dbg_dest_log, dbg_file);
	//DBG_SET_PARAMS(0, 0, FALSE, FALSE, NULL);

	$usage = FALSE;

	$kamVideos = new KamVideos(NULL);

	if (isset($argv[1]))
	{
		switch ($argv[1])
		{
			case "-sc":
				// start capture - name = argv[2], duration=argv[3]
				if ($argc == 4)
				{
					$result = $kamVideos->BeginCaptureVideo(basename($argv[2]), $argv[3]);
				}
				else
				{
					$usage = TRUE;
				}
				break;

			case "-ec":
				// end capture - name = argv[2], completionstatus = argv[3]
				if ($argc == 4)
				{
					$result = $kamVideos->CompleteCaptureVideo(basename($argv[2]), $argv[3]);
				}
				else
				{
					$usage = TRUE;
				}
				break;

			case "-as":
				// add segment - name = argv[2], segment filename = argv[3], segment duration = argv[4]
				if ($argc == 5)
				{
					$result = $kamVideos->AddSegment(basename($argv[2]), $argv[3], $argv[4]);
				}
				else
				{
					$usage = TRUE;
				}
				break;

			case "-uv":
				// add segment - name = argv[2], segment filename = argv[3], segment duration = argv[4]
				$kamVideos->UploadVideos($resultString);
				break;

			default:
				echo "invalid command ".$argv[1]."\n";
				$usage = TRUE;
				break;
		}
	}

	if ($usage)
	{
		echo "Usage: ".$argv[0]." [-sc name capturestarttime duration | -ec name completionstatus  | -as name segfilename segfilesize]\n";
	}
?>
