<?php

	use \Core\Common\Classes\MethodCallContext;
	use \Core\Common\Classes\Kam;

	require_once '/home/kam/kapturrkam/core/core.php';
	require_once '/home/kam/kapturrkam/core/autoload.php';

	DBG_SET_PARAMS(DBGZ_APP | DBGZ_KAMKONFIG, DBGL_TRACE | DBGL_INFO | DBGL_ERR | DBGL_WARN, FALSE, FALSE, dbg_dest_log, dbg_file);
	//DBG_SET_PARAMS(0, 0, FALSE, FALSE, NULL);

	if ($argc == 2)
	{
		$kam = new Kam(new MethodCallContext(TRUE, NULL, NULL, NULL));

		$result = $kam->GetDeviceStatus($argv[1], $value, $resultString);

		if ($result)
		{
			echo $value . PHP_EOL;
		}
		else
		{
			echo "Error: $resultString" . PHP_EOL;
		}
	}
?>
