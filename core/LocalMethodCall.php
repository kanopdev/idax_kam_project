<?php

	require_once '/home/kam/kapturrkam/core/core.php';
	require_once CORE_ROOT.'/autoload.php';

	use \Core\Common\Classes\TokenManager;
	use \Core\Common\Classes\MethodCallContext;

	function MapPublicClassName(
		$publicClassName
		)
	{
		DBG_ENTER(DBGZ_METHODCALL, __FUNCTION__, "publicClassName=$publicClassName");

		$classMap = array(
				"TokenManager" => "\Core\Common\Classes\TokenManager",
				"Kam" => "\Core\Common\Classes\Kam",
				"KamKonfig" => "\Core\Common\Classes\KamKonfig"
				);

		if (isset($classMap[$publicClassName]))
		{
			$mappedClassName = $classMap[$publicClassName];
		}
		else
		{
			$mappedClassName = $publicClassName;
		}

		DBG_RETURN(DBGZ_METHODCALL, __FUNCTION__, "Mapped $publicClassName to $mappedClassName");
		return $mappedClassName;
	}

	function LocalMethodCall(
		$className,
		$methodName,
		$parameters,
		$userAgentString
		)
	{
		DBG_ENTER(DBGZ_METHODCALL, __FUNCTION__, "className=$className, methodName=$methodName");

		$token = isset($parameters['_mhdr_token']) ? $parameters['_mhdr_token'] : NULL;
		$validToken = TRUE;  // A "NULL" token is considered valid for anonymous clients
		$tokenexpired = FALSE;

		if ($token != NULL)
		{
			$validToken = TokenManager::ValidateToken($token, $tokenexpired);
		}

		if ($validToken === TRUE)
		{
			if ($tokenexpired === TRUE)
			{
				DBG_ERR(DBGZ_METHODCALL, __FUNCTION__, "token expired");

				$validToken = FALSE;

				$response_str = array(
						"results" => array(
								'response' => 'failed',
								'responder' => "TokenManager",
								'returnval' =>  array("resultstring" => "login required")
								)
						);
			}
			else
			{
				$className = MapPublicClassName($className);

				if (class_exists($className))
				{
					$context = new MethodCallContext(
							FALSE,
							$token,
							$parameters,
							$userAgentString
							);

					try
					{
						$response_str = call_user_func(
								array($className, "MethodCallDispatcher"),
								$context,
								$methodName,
								$parameters
								);
					}
					catch (Exception $exception)
					{
						DBG_EXCEPTION(DBGZ_METHODCALL, __FUNCTION__, $exception->__toString());

						$response_str = array(
								"results" => array(
										'response' => 'exception',
										'responder' => "$className::$methodName",
										'returnval' =>  array("resultstring" => $exception->__toString())
										)
								);
					}
				}
				else
				{
					DBG_ERR(DBGZ_METHODCALL, __FUNCTION__, "Class $className not found");

					$response_str = array(
							"results" => array(
									'response' => 'failed',
									'responder' => $className,
									'returnval' =>  array("resultstring" => "failed")
									)
							);
				}
			}
		} 
		else
		{
			DBG_ERR(DBGZ_METHODCALL, __FUNCTION__, "invalid token");

			$response_str = array(
					"results" => array(
							'response' => 'failed',
							'responder' => "TokenManager",
							'returnval' =>  array("resultstring" => "login required")
							)
					);
		}

		DBG_RETURN(DBGZ_METHODCALL, __FUNCTION__);
		return $response_str;
	}
?>
