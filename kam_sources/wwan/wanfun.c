/*
 * hacked up version of picocom.c --> wanfun
 *
 * Customized to get modem/network status, 
 * retrieve GPS location or send text messages
 *
 * Customized by Randy Kath (randy@kanopian.com)
 *
 * Originally by Nick Patavalis (npat@efault.net)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>
#include <stdarg.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <limits.h>
#include <stdbool.h>
#include <time.h>
#ifdef USE_FLOCK
#include <sys/file.h>
#endif
#define _GNU_SOURCE
#include <getopt.h>
#include "term.h"

/**********************************************************************/

/* parity modes names */
const char *parity_str[] = {
	[P_NONE] = "none",
	[P_EVEN] = "even",
	[P_ODD] = "odd",
	[P_MARK] = "mark",
	[P_SPACE] = "space",
};

/* flow control modes names */
const char *flow_str[] = {
	[FC_NONE] = "none",
	[FC_RTSCTS] = "RTS/CTS",
	[FC_XONXOFF] = "xon/xoff",
	[FC_OTHER] = "other",
};

/**********************************************************************/

/* implemented caracter mappings */
#define M_CRLF   (1 << 0) /* map CR  --> LF */
#define M_CRCRLF (1 << 1) /* map CR  --> CR + LF */
#define M_IGNCR  (1 << 2) /* map CR  --> <nothing> */
#define M_LFCR   (1 << 3) /* map LF  --> CR */
#define M_LFCRLF (1 << 4) /* map LF  --> CR + LF */
#define M_IGNLF  (1 << 5) /* map LF  --> <nothing> */
#define M_DELBS  (1 << 6) /* map DEL --> BS */
#define M_BSDEL  (1 << 7) /* map BS  --> DEL */
#define M_NFLAGS 8

/* default character mapping */
#define M_I_DFL 0
#define M_O_DFL 0
#define M_E_DFL (M_DELBS | M_CRCRLF)

/* character mapping names */
struct map_names_s {
	char *name;
	int flag;
} map_names[] = {
	{ "crlf", M_CRLF },
	{ "crcrlf", M_CRCRLF },
	{ "igncr", M_IGNCR },
    { "lfcr", M_LFCR },
	{ "lfcrlf", M_LFCRLF },
	{ "ignlf", M_IGNLF },
	{ "delbs", M_DELBS },
	{ "bsdel", M_BSDEL },
	/* Sentinel */
	{ NULL, 0 } 
};

/**********************************************************************/

struct {
	char port[128];
	int baud;
	enum flowcntrl_e flow;
	enum parity_e parity;
	int databits;
	int stopbits;
	int noreset;
	int nolock;
	int imap;
	int omap;
} opts = {
	.port = "/dev/ttyUSB3",
	.baud = 115200,
	.flow = FC_NONE,
	.parity = P_NONE,
	.databits = 8,
	.stopbits = 1,
	.noreset = 0,
	.nolock = 0,
	.imap = M_I_DFL,
	.omap = M_O_DFL
};

enum WWAN_ACTION {
    WWAN_NOACTION,
    WWAN_STATUS,
    WWAN_LOCATION,
    WWAN_SENDTEXT
};

static enum WWAN_ACTION action = WWAN_NOACTION;
static bool verbose = false;
static char cellnumber[13];
static char txtmsg[161];

static float            last_latitude = 0.0, last_longitude = 0.0;

int tty_fd;

#ifndef TTY_Q_SZ
#define TTY_Q_SZ 256
#endif

struct tty_q {
	int len;
	unsigned char buff[TTY_Q_SZ];
} tty_q;

#define TTY_RD_SZ 128

int tty_write_sz;

#define TTY_WRITE_SZ_DIV 10
#define TTY_WRITE_SZ_MIN 8

#define set_tty_write_sz(baud)							\
    do {												\
        tty_write_sz = (baud) / TTY_WRITE_SZ_DIV;		\
	    if ( tty_write_sz < TTY_WRITE_SZ_MIN )			\
            tty_write_sz = TTY_WRITE_SZ_MIN;			\
    } while (0)

/**********************************************************************/

/* maximum number of chars that can replace a single characted
   due to mapping */
#define M_MAXMAP 4

int do_map (char *b, int map, char c)
{
	int n;

	switch (c) {
	case '\x7f':
		/* DEL mapings */
		if ( map & M_DELBS ) {
			b[0] = '\x08'; n = 1;
		} else {
			b[0] = c; n = 1;
		}
		break;
	case '\x08':
		/* BS mapings */
		if ( map & M_BSDEL ) {
			b[0] = '\x7f'; n = 1;
		} else {
			b[0] = c; n = 1;
		}
		break;
	case '\x0d':
		/* CR mappings */
		if ( map & M_CRLF ) {
			b[0] = '\x0a'; n = 1;
		} else if ( map & M_CRCRLF ) {
			b[0] = '\x0d'; b[1] = '\x0a'; n = 2;
		} else if ( map & M_IGNCR ) {
			n = 0;
		} else {
			b[0] = c; n = 1;
		}
		break;
	case '\x0a':
		/* LF mappings */
		if ( map & M_LFCR ) {
			b[0] = '\x0d'; n = 1;
		} else if ( map & M_LFCRLF ) {
			b[0] = '\x0d'; b[1] = '\x0a'; n = 2;
		} else if ( map & M_IGNLF ) {
			n = 0;
		} else {
			b[0] = c; n = 1;
		}
		break;
	default:
		b[0] = c; n = 1;
		break;
	}

	return n;
}


/**********************************************************************/

bool OK_response(char *buf, int len, char *result)
{
    if (len >= 6 &&
        buf[len-6] == '\r' && 
        buf[len-5] == '\n' && 
        buf[len-4] == 'O' && 
        buf[len-3] == 'K' && 
        buf[len-2] == '\r' && 
        buf[len-1] == '\n')
    {
        strcpy(result, "OK");
        return true;
    }
    return false;
}

bool ERROR_response(char *buf, int len, char *result)
{
    if (len >= 9 &&
        buf[len-9] == '\r' && 
        buf[len-8] == '\n' && 
        buf[len-7] == 'E' && 
        buf[len-6] == 'R' && 
        buf[len-5] == 'R' && 
        buf[len-4] == 'O' && 
        buf[len-3] == 'R' && 
        buf[len-2] == '\r' && 
        buf[len-1] == '\n')
    {
        strcpy(result, "ERROR");
        return true;
    }
    return false;
}

bool interactive_response(char *buf, int len)
{
    return (len >= 4 &&
        buf[len-4] == '\r' && 
        buf[len-3] == '\n' && 
        buf[len-2] == '>' && 
        buf[len-1] == ' ');
}

bool process_command(char *cmd, char *msg, char *response)
{
    char result[7];
	char *pch = cmd;
    bool done = false;
	fd_set rdset, wrset;
	int r, n;
	
	/* convert into character mapped input command */
	tty_q.len = 0;
	while (pch[0]) 
	{
        if (tty_q.len + M_MAXMAP <= TTY_Q_SZ) {
            n = do_map((char *)tty_q.buff + tty_q.len, opts.omap, pch[0]);
            tty_q.len += n;
        } else 
        {
            fprintf(stderr, "Command: %s ignored, sequence too long: %d\n", cmd, n);
            return false;
        }
        pch++;
    }

	while ( !done ) {
		FD_ZERO(&rdset);
		FD_ZERO(&wrset);
        FD_SET(tty_fd, &rdset);
        if ( tty_q.len ) 
            FD_SET(tty_fd, &wrset);

		r = select(tty_fd + 1, &rdset, &wrset, NULL, NULL);
		if ( r < 0 )  {
			if ( errno == EINTR )
				continue;
			else
			{
				fprintf(stderr, "select failed: %d : %s", errno, strerror(errno));
				return false;
            }
		}

        /* read from port */
		if ( FD_ISSET(tty_fd, &rdset) ) 
		{

			char buff_rd[TTY_RD_SZ];
			char buff_map[TTY_RD_SZ * M_MAXMAP];
            int i;

			do 
			{
				n = read(tty_fd, &buff_rd, sizeof(buff_rd));
			} while (n < 0 && errno == EINTR);
			
			/* nothing to read */
			if (n == 0) 
			{
				fprintf(stderr, "term closed");
			}
			/* error reading */
			else if ( n < 0 ) 
			{
				if ( errno != EAGAIN && errno != EWOULDBLOCK )
				{
					fprintf(stderr, "read from term failed: %s", strerror(errno));
					return false;
                }
			} 
			/* a response was read */
			else 
			{	
                /* map response chars */
				char *bmp = &buff_map[0];
				for (i = 0; i < n; i++) 
				{
					bmp += do_map(bmp, opts.imap, buff_rd[i]);
				}
				n = bmp - buff_map;

                /* if \r\nOK\r\n response */
                if (OK_response(buff_rd, i, result)) 
                {
                    /* if a response other than OK */
                    if (i > 10) 
                    {
                        /* skip crlf sequences around response and truncate OK sequence from response */
                        pch = buff_rd+2;                
                        strncpy(response, pch, i-10);
                        response[i-10] = 0;

                        if (verbose)
                            fprintf(stderr, "%s\n", response);
                    }
                    done = true;
                }
                    
                /* if \r\nERROR\r\n response */
                else if (ERROR_response(buff_rd, i, result))
                {
                    done = true;           
                }
                                         
                /* else if an interactive SMS message command response, 
                 * recognized by the "\r\n> " sequence
                 * queue the ^Z terminated message part to complete the send */
                else if (interactive_response(buff_rd, i) && strlen(msg))
                {
                    /* convert into character mapped input command */
                    pch = msg;
                    tty_q.len = 0;
                    while (pch[0]) 
                    {
                        if (tty_q.len + M_MAXMAP <= TTY_Q_SZ) {
                            n = do_map((char *)tty_q.buff + tty_q.len, opts.omap, pch[0]);
                            tty_q.len += n;
                        } else 
                        {
                            fprintf(stderr, "message: %s ignored, sequence too long: %d\n", msg, n);
                            return false;
                        }
                        pch++;
                    }
                }
                /* command ECHO and any response other than above */
                else 
                {
                    if (i > 1 && verbose) 
                    {
                        /* remove trailing /r from echo response */
                        strncpy(response, buff_rd, i-1);
                        response[i-1] = 0;
                        fprintf(stderr, "Command %s, ", response);
                    }
                }                
			}			
		}

        /* write to port */
		if ( FD_ISSET(tty_fd, &wrset) ) {
			int sz;
			sz = (tty_q.len < tty_write_sz) ? tty_q.len : tty_write_sz;
			do {
				n = write(tty_fd, tty_q.buff, sz);
			} while ( n < 0 && errno == EINTR );
			if ( n <= 0 )
			{
				fprintf(stderr, "write to term failed: %s", strerror(errno));
				return false;
            }
			memmove(tty_q.buff, tty_q.buff + n, tty_q.len - n);
			tty_q.len -= n;
		}
	}
	return (!strcmp(result, "OK"));
}


/**********************************************************************/

bool send_text() {
	char    response_str[1024];
    char    msgcmd[30];    
    bool    success = false;
    
    if (!process_command("ATZ\r\n", NULL, response_str))
    {
        fprintf(stderr, "Unable to initialize modem.\n");
        return success;
    }

    /* assumes a valid phone number - the ipad app will do the vetting prior to configuring the device */
    if (process_command("AT+CMGF=1\r\n", NULL, response_str)) 
    {
        // set number to text
        if (strlen(cellnumber) != 10) 
        {
            // default test number
            strcpy(cellnumber, "4254295282");
        }
        sprintf(msgcmd, "AT+CMGS=\"+1%s\",145\r\n", cellnumber);
        if (!strlen(txtmsg)) 
        {
            // default test message
            strcpy(txtmsg, "This is a test message from Beaglebone");
        }
        else if (strlen(txtmsg) > 160)
        {
            // truncate if too long
            txtmsg[160] = 0;
        }
        // add ^Z send command terminator to message
        strcat(txtmsg, "\x1a");
        if (process_command(msgcmd, txtmsg, response_str))
        {
            if (verbose)
            {
                fprintf(stdout, "Successfully sent to: %s, message: %s\n", cellnumber, txtmsg);
                fprintf(stdout, "Response received: %s\n", response_str);
            }
            success = true;
        }
        else
        {
            if (verbose)
            {
                fprintf(stderr, "Failed to text: %s, message: %s\n", cellnumber, txtmsg);
            }
        }
    } 
    else
    {
        fprintf(stderr, "Failed to initialize AT+CMGF=1\n");
    }
    return success;
}

/**********************************************************************/

bool gps_location(const char *response_str) {
    char    latdegrees[3], latminutes[8];
    char    longdegrees[4], longminutes[8];
    char    altstr[6];
    char    fix_status[12];
    char    ptr[256], timestr[256], datestr[256];
    char    *pch;
    int     i;
    float   latitude, longitude;
    
    // make sure we have valid entries for all fields
    strcpy(ptr, response_str);
    pch = strchr(ptr, ',');
    for (i=0; i<4; i++) {
        pch = strchr(pch+1, ',');
        if (i<3 && pch[1] == ',') {
            return false;
        }
    }
        
    // retrieve UTC time
    strcpy(timestr, response_str+9);
    pch = strtok(timestr, ".");
    char hour[3], min[3], sec[3];
    strncpy(hour, pch, 2); hour[2] = 0;
    strncpy(min, pch+2, 2); min[2] = 0;
    strncpy(sec, pch+4, 2); sec[2] = 0;

    // retrieve latitude detail from 1st delimited position (0-based)
    strcpy(ptr, response_str);
    pch = strtok(ptr, ",");
    pch = strtok(NULL, ",");
    strncpy(latdegrees, pch, 2); latdegrees[2] = 0;
    strncpy(latminutes, pch+2, strlen(pch)-3); latminutes[strlen(pch)-3] = 0;
    latitude = (float)atoi(latdegrees) + atof(latminutes)/60;
    if (pch[strlen(pch)-1] == 'S')
        latitude *= -1;

    // retrieve longitude detail from 2nd delimited position
    pch = strtok(NULL, ",");
    strncpy(longdegrees, pch, 3); longdegrees[3] = 0;
    strncpy(longminutes, pch+3, strlen(pch)-4); longminutes[strlen(pch)-4] = 0;
    longitude = (float)atoi(longdegrees) + atof(longminutes)/60;
    if (pch[strlen(pch)-1] == 'W')
        longitude *= -1;

    // retrieve altitude from 4th delimited position
    pch = strtok(NULL, ",");
    pch = strtok(NULL, ",");
    strncpy(altstr, pch, 3); altstr[3] = 0;
    
    // retrieve fix status from 5th delimited position
    pch = strtok(NULL, ",");
    if (pch[0] == '0') 
        strcpy(fix_status, "Invalid fix");
    else if (pch[0] == '2') 
        strcpy(fix_status, "2D fix");
    else if (pch[0] == '3') 
        strcpy(fix_status, "3D fix");

    // retrieve acquisition date
    strcpy(datestr, response_str);
    pch = strtok(datestr, ",");
    for (i=0; i<9; i++)
        pch = strtok(NULL, ",");
    char day[2], mon[2], year[2];
    strncpy(day, pch, 2); day[2] = 0;
    strncpy(mon, pch+2, 2); mon[2] = 0;
    strncpy(year, pch+4, 2); year[2] = 0;

    // if current fix has changed from last_ read values, update last_ read values and return
    if (last_latitude != latitude && last_longitude != longitude) {
        last_latitude = latitude;
        last_longitude = longitude;
        return false;
    }
    // we made it here, we're done print result
    fprintf(stderr, "Acquired: 20%s-%s-%s %s:%s:%s\n", year, mon, day, hour, min, sec);
    fprintf(stderr, "Lat: %.6f\nLong: %.6f\nAlt: %s meters\n", latitude, longitude, altstr);
    fprintf(stderr, "Status: %s\n", fix_status);
    return true;
}

bool print_location() {
    int i;
    bool success = false;
	char response_str[1024];

    if (!process_command("ATZ\r\n", NULL, response_str))
    {
        if (verbose)
            fprintf(stderr, "Unable to initialize modem.\n");
        return success;
    }

    if (!process_command("AT$GPSP=1\r\n", NULL, response_str))
        if (verbose)
            fprintf(stderr, "Failed response to set GPSP=1.\n");
    for (i=0; i<15; i++) 
    {
        if (process_command("AT$GPSACP\r\n", NULL, response_str)) 
        {
            if (gps_location(response_str)) 
            {
                success = true;
                break;
            }
        }
        sleep(1);
    }
    if (!success && verbose)
        fprintf(stderr, "Unable to acquire GPS location.\n");
    return success;
}

/**********************************************************************/

void cell_number(const char *response_str) {
    char val[100];
    char *token = "\",\"";
    char *location = strstr(response_str, token);
    if (location && strlen(location) >= 14) 
    {
        if (location[0] == '+') // VZW includes '+' prefix
            strncpy(val, location+4, 11);
        else                    // TMo does not include the prefix
            strncpy(val, location+3, 11);
        val[11] = 0;
        fprintf(stdout, "Cell number: %s\n", val);
    }
}
void cell_IMEI(const char *response_str) {
    char val[100];
    strcpy(val, response_str);
    fprintf(stdout, "IMEI: %s\n", val);
}
void cell_firmware(const char *response_str) {
    char val[100];
    strcpy(val, response_str);
    fprintf(stdout, "Firmware: %s\n", val);
}
void network_status(const char *response_str) {
    char network[100], status[100];
    char *token = "+CGREG: ";
    char num1, num2;
    char *location = strstr(response_str, token);
    if (location && strlen(location) > 10) 
    {
        num1 = location[8]; 
        num2 = location[10];
        if (num1 == '0')
            strcpy(network, "GSM");
        else if (num1 == '2')
            strcpy(network, "UTRAN");
        else if (num1 == '3')
            strcpy(network, "GSM w/EGPRS");
        else if (num1 == '4')
            strcpy(network, "UTRAN w/HSDPA");
        else if (num1 == '5')
            strcpy(network, "UTRAN w/HSUPA");
        else if (num1 == '6')
            strcpy(network, "UTRAN w/HSDPA & HSUPA");
        else if (num1 == '7')
            strcpy(network, "E-UTRAN");
        else 
            strcpy(network, "Unknown");

        if (num2 == '0')
            strcpy(status, "Not registered, not searching.");
        else if (num2 == '1')
            strcpy(status, "Registered, home network.");
        else if (num2 == '2')
            strcpy(status, "Not registered, searching...");
        else if (num2 == '3')
            strcpy(status, "Registration denied.");
        else if (num2 == '4')
            strcpy(status, "Unknown status.");
        else if (num2 == '5')
            strcpy(status, "Registered, roaming.");

        fprintf(stdout, "Network type: %s\nStatus: %s\n", network, status);
    }
}
void cell_signal(const char *response_str) {
    char val[100];
    char *token = "+CSQ: ";
    char *numstr;
    char *location = strstr(response_str, token);
    
    if (location && strlen(location) >= 8) 
    {
        numstr = location+6; numstr[2] = 0;
        int strength = atoi(numstr);
        if (strength < 10) 
            strcpy(val, "Weak");
        else if (strength < 15)
            strcpy(val, "OK");
        else if (strength < 20)
            strcpy(val, "Good");
        else if (strength < 31)
            strcpy(val, "Great");
        else if (strength < 99)
            strcpy(val, "Excellent");
        else if (strength == 99)
            strcpy(val, "Unknown");

        fprintf(stdout, "Signal strength: %s\n", val);
    }
}
void cell_carrier(const char *response_str) {
    char val[100];
    char *token1 = ",\"";
    char *token2 = "\",";
    char *location1 = strstr(response_str, token1);
    char *location2 = strstr(response_str, token2);
    
    // validate the response_str
    if (!strstr(response_str, "+COPS: "))
        return;
    
    if (location1 && location2) 
    {
        strncpy(val, location1+2, location2-location1);
        val[location2-(location1+2)] = 0;

        fprintf(stdout, "Carrier: %s\n", val);
    }
}

bool print_status() {
	char response_str[1024];

    if (process_command("ATZ\r\n", NULL, response_str))
    {
        if (process_command("AT+CGREG?\r\n", NULL, response_str))
            network_status(response_str);
        if (process_command("AT+CNUM\r\n", NULL, response_str))
            cell_number(response_str);
        if (process_command("AT+GSN\r\n", NULL, response_str))
            cell_IMEI(response_str);
        if (process_command("AT+CSQ\r\n", NULL, response_str))
            cell_signal(response_str);
        if (process_command("AT+CGMR\r\n", NULL, response_str))
            cell_firmware(response_str);
        if (process_command("AT+COPS?\r\n", NULL, response_str))
            cell_carrier(response_str);
        return true;
    }
    else
    {
        fprintf(stdout, "Failed to initialize modem.\n");
        return false;
    }
}


/**********************************************************************/

void show_usage(char *name)
{
	char *s;

	s = strrchr(name, '/');
	s = s ? s+1 : name;

	printf("wwanfuncs\n");
	printf("\nUsage is: %s [options]\n"
          " -b | --baud          Set baudrate for the port (115200 default)\n"
          " -s | --status        Status summary of the wwan module\n"
          " -l | --location      Get current location from the GPS module\n"
          " -v | --verbose       Print AT commands along with result strings\n"
          " -t | --text          Send default test message to test number\n"
          " -n | --number        [number], 10 digit number to text, digits only\n"
          " -m | --message       [MSG] text message to send, 160 chars max\n"
          " -p | --port          [tty device port] (/dev/ttyUSB3 default)\n"          
          "\n"
          "Examples:\n"
          "\twanfun -t -p /dev/ttyUSB2                      :: sends canned message to test phone number via specified port\n"
          "\twanfun -t -n 4254295282 -m 'This is a message' :: sends specified message to specified number\n"
          "\twanfun -s -v                                   :: prints status of the modem, prints verbose AT command messages\n"
          "\twanfun -l -v                                   :: tries to get GPS location, prints verobse AT command messages\n"
          "\n"
          "All of the above output a final status of either 'success' or 'failure'\n"
          "\te.g. result: success\n"
          "\n", s);
}

/**********************************************************************/

void parse_args(int argc, char *argv[])
{
	int r;

	static struct option longOptions[] =
	{
		{"verbose", no_argument, 0, 'v'},
		{"status", no_argument, 0, 's'},
		{"location", no_argument, 0, 'l'},
		{"text", no_argument, 0, 't'},
		{"number", required_argument, 0, 'n'},
		{"message", required_argument, 0, 'm'},
		{"baud", required_argument, 0, 'b'},
		{"port", required_argument, 0, 'p'},
		{"help", no_argument, 0, 'h'},
		{0, 0, 0, 0}
	};

	r = 0;
	while (1) {
		int optionIndex = 0;
		int c;

		/* no default error messages printed. */
		opterr = 0;

		c = getopt_long(argc, argv, "vsltn:m:b:p:h",
						longOptions, &optionIndex);

		if (c < 0)
			break;

		switch (c) {
		case 'b':
			opts.baud = atoi(optarg);
			if ( opts.baud == 0 || ! term_baud_ok(opts.baud) ) {
				fprintf(stderr, "Invalid --baud: %d\n", opts.baud);
				r = -1;
			}
			break;

        case 's':
            action = WWAN_STATUS;
            break;

        case 'v':
            verbose = true;
            break;

        case 'l':
            action = WWAN_LOCATION;
            break;
      
        case 't':
            action = WWAN_SENDTEXT;
            break;
        
        case 'n':
            if (strlen(optarg)) { 
                strcpy(cellnumber, optarg);
            }
            break;
      
        case 'm':
            if (strlen(optarg)) {
                if (strlen(optarg)<161) {                 
                    strcpy(txtmsg, optarg);
                } 
                else
                {
                    strncpy(txtmsg, optarg, 160);
                    txtmsg[160] = 0;
                }
            }
            break;

        case 'p':
            if (strlen(optarg)) {
                strncpy(opts.port, optarg, sizeof(opts.port) - 1);
                opts.port[sizeof(opts.port) - 1] = '\0';
            }
            break;

		case 'h':
			show_usage(argv[0]);
			exit(EXIT_SUCCESS);
		case '?':
		default:
			fprintf(stderr, "Unrecognized option(s)\n");
			r = -1;
			break;
		}
		if ( r < 0 ) {
			fprintf(stderr, "Run with '--help'.\n");
			exit(EXIT_FAILURE);
		}
	} /* while */	

    if (verbose)
    {
        printf("wwanfuncs");
        printf("\n");
        printf("port is        : %s\n", opts.port);
        printf("flowcontrol    : %s\n", flow_str[opts.flow]);
        printf("baudrate is    : %d\n", opts.baud);
        printf("parity is      : %s\n", parity_str[opts.parity]);
        printf("databits are   : %d\n", opts.databits);
        printf("stopbits are   : %d\n", opts.stopbits);
        printf("\n");
    }
}


/**********************************************************************/

int main(int argc, char *argv[])
{
	int r;

	parse_args(argc, argv);

	r = term_lib_init();
	if ( r < 0 )
	{
		fprintf(stderr, "term_init failed: %s", term_strerror(term_errno, errno));
		exit(0);
    }
	tty_fd = open(opts.port, O_RDWR | O_NONBLOCK | O_NOCTTY);
	if (tty_fd < 0)
	{
		fprintf(stderr, "cannot open %s: %s", opts.port, strerror(errno));
		exit(0);
    }
	if ( ! opts.nolock ) {
		r = flock(tty_fd, LOCK_EX | LOCK_NB);
		if ( r < 0 )
		{
			fprintf(stderr, "cannot lock %s: %s", opts.port, strerror(errno));
            exit(0);
        }
	}
    r = term_set(tty_fd,
                 1,              /* raw mode. */
                 opts.baud,      /* baud rate. */
                 opts.parity,    /* parity. */
                 opts.databits,  /* data bits. */
                 opts.stopbits,  /* stop bits. */
                 opts.flow,      /* flow control. */
                 1,              /* local or modem */
                 !opts.noreset); /* hup-on-close. */
	if ( r < 0 )
	{
		fprintf(stderr, "failed to add device %s: %s", opts.port, term_strerror(term_errno, errno));
		exit(0);
    }
	r = term_apply(tty_fd, 0);
	if ( r < 0 )
	{
		fprintf(stderr, "failed to config device %s: %s", opts.port, term_strerror(term_errno, errno));
		exit(0);
    }
	set_tty_write_sz(term_get_baudrate(tty_fd, NULL));


    switch (action) {
        case WWAN_STATUS:
            if (print_status())
                fprintf(stdout, "result: success\n");
            else
                fprintf(stdout, "result: failed\n");
            break;                
        case WWAN_LOCATION:
            if (print_location())
                fprintf(stdout, "result: success\n");
            else
                fprintf(stdout, "result: failed\n");
            break;
        case WWAN_SENDTEXT:
            if (send_text())
                fprintf(stdout, "result: success\n");
            else
                fprintf(stdout, "result: failed\n");
            break;
        default:
            show_usage(argv[0]);
            break;
    }

	if ( opts.noreset ) {
        if (verbose)
            fprintf(stderr, "Skipping tty reset...\r\n");
		term_erase(tty_fd);
	}

	/* wait a bit for output to drain */
	sleep(1);

	return EXIT_SUCCESS;
}
