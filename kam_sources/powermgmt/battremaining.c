#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>


#include <endian.h>
#include <linux/i2c-dev.h>

#define REG_SHUNT           1
#define REG_BUS             2
#define INA_ADDRESS         0x40
#define FullCharge          13000
#define EmptyCharge         11000

int i2c_bus = 2;
int i2c_address = INA_ADDRESS;
int handle;

int i2c_read( void *buf, int len )
{
    int rc = 0;

    if ( read( handle, buf, len ) != len )
    {
        printf( "I2C read failed: %s\n", strerror( errno ) );
        rc = -1;
    }
    return rc;
}
int i2c_write( void *buf, int len )
{
    int rc = 0;
    
    if ( write( handle, buf, len ) != len ) 
    {
        printf( "I2C write failed: %s\n", strerror( errno ) );
        rc = -1;
    }
    return rc;
}

int register_read( unsigned char reg, unsigned short *data )
{
    int rc = -1;
    unsigned char bite[ 4 ];
    
    bite[ 0 ] = reg;
    if ( i2c_write( bite, 1 ) == 0 )
    {
        if ( i2c_read( bite, 2 ) == 0 )
        {
            *data = ( bite[ 0 ] << 8 ) | bite[ 1 ];
            rc = 0;
        }
    }
    return rc;
}

int get_voltage( int chan, float *mv )
{
    short bus;

    if ( register_read( REG_BUS + ( chan << 1 ), (unsigned short*)&bus ) != 0 )
    {
        return -1;
    }
    *mv = ( float )( bus & 0xFFF8 );
    return 0;
}


int get_current( int chan, float *ma )
{
    short shunt;

    if ( register_read( REG_SHUNT + ( chan << 1 ), &shunt ) != 0 )
    {
        return -1;
    }
    *ma = (float)shunt / 4;
    return 0;
}


void show_remaining( void )
{
    float ma0, mv0;

    if ( get_current( 0, &ma0 ) || get_voltage( 0, &mv0 ) )
    {
        fprintf( stderr, "Error reading voltage/current\n" );
        return;
    }
    printf( "CH1 (Battery): %4.2fV  %4.0fmA,\tRemaining: %.2f\%,\tCharging: %s\n", 
            mv0/1000, ma0, 
            (mv0 - EmptyCharge) / (FullCharge - EmptyCharge) * 100, 
            ma0 > 0 ? "No" : "Yes");
}


int main( )
{
    char filename[ 20 ];
    snprintf( filename, 19, "/dev/i2c-%d", i2c_bus );
    handle = open( filename, O_RDWR );    
    if ( handle < 0 ) 
    {
        fprintf( stderr, "Error opening bus %d: %s\n", i2c_bus, strerror( errno ) );
        exit( 1 );
    }
    if ( ioctl( handle, I2C_SLAVE, i2c_address ) < 0 ) 
    {
        fprintf( stderr, "Error setting address %02X: %s\n", i2c_address, strerror( errno ) );
        exit( 1 );
    }
    show_remaining();
    close( handle );
    return 0;
}

