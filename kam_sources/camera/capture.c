﻿/*  V4L2 video capture example
 *
 *  This program can be used and distributed without restrictions.
 *
 *  This program is utilizes the V4L2 API
 *  see http://linuxtv.org/docs.php for more information
 */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <getopt.h>             /* getopt_long() */
#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>
#include <linux/uvcvideo.h>
#include "uvch264.h"
#include <endian.h>
#include <linux/i2c-dev.h>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

#define REG_SHUNT           1
#define REG_BUS             2
#define INA_ADDRESS         0x40
#define FullCharge          13300
#define EmptyCharge         8000

int i2c_bus = 2;
int i2c_address = INA_ADDRESS;
int handle;

enum io_method {
    IO_METHOD_READ,
    IO_METHOD_MMAP,
    IO_METHOD_USERPTR,
};

struct buffer {
    void   *start;
    size_t  length;
};

static char             *dev_name;
static char             timestr[20];
static enum io_method   io = IO_METHOD_MMAP;
static int              fd = -1;
static long long        toEpochOffset_ms;
struct buffer           *buffers;
static unsigned int    n_buffers;
static int              out_buf = 0;
static int              out_file = 0;
static int              out_filedate = 0;
static int              file_count = 0;
static int              force_format = 0;
static int              frame_count = 0; 
static FILE*            fileout = NULL;
static char             *filepath;
static int              bitrate = 0;
static int              fps = 30;
static int              qp = 0;
static int              iframe = 0;
static int              rcmode = -1;
static int              logging = 0;
static int              logbattery = 0;
static FILE*            logfile = NULL;
static unsigned long   duration = 0; // default 0, infinite
static unsigned long   segment_start = 0;
static unsigned long   segment_length = 3600; // default segment length 1 hr

char *updatetime () {
    struct tm *tmptr;
    time_t seconds;
    seconds = time( NULL );
    tmptr = localtime( &seconds );
    sprintf(timestr, "%4d-%02d-%02d %2d:%02d:%02d", 1900+tmptr->tm_year, 1+tmptr->tm_mon, tmptr->tm_mday, tmptr->tm_hour, tmptr->tm_min, tmptr->tm_sec );
    return timestr;
}  

static void errno_exit(const char *s)
{
    fprintf(logfile, "%s error %d, %s\n", s, errno, strerror(errno));
    exit(EXIT_FAILURE);
}

static int xioctl(int fh, int request, void *arg)
{
    int r;
    do {
        r = ioctl(fh, request, arg);
    } while (-1 == r && EINTR == errno);

    return r;
}

static void setFramerate(int fps) 
{
    int res;
    struct v4l2_streamparm vfstream;
    struct v4l2_captureparm vfcap;
    
    vfcap.capability = V4L2_CAP_TIMEPERFRAME;
    vfcap.capturemode = V4L2_MODE_HIGHQUALITY;
    vfcap.timeperframe.numerator = 1;
    vfcap.timeperframe.denominator = fps;

    vfstream.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    vfstream.parm.capture = vfcap;
    
    res = xioctl(fd, VIDIOC_S_PARM, &vfstream);
    if (res) 
    {
        perror("framerate");
        fprintf(logfile, "Set Framerate error %d, %s\n", errno, strerror(errno));
        return;
    }
    if (logging && logfile) 
    {
        fprintf(logfile, "%s Setting framerate %u / %u\n", updatetime(), vfcap.timeperframe.numerator, vfcap.timeperframe.denominator); 
        fflush(logfile);
    } 
    else 
    {
        fprintf(stderr, "Setting framerate %u / %u\n", vfcap.timeperframe.numerator, vfcap.timeperframe.denominator);     
    }
}

static void setBitrate(int bmin, int bmax)
{
    int res;
    struct uvc_xu_control_query ctrl;
    uvcx_bitrate_layers_t  conf;
    ctrl.unit = 12;
    ctrl.size = 10;
    ctrl.selector = UVCX_BITRATE_LAYERS;
    ctrl.data = (__u8*)&conf;
    ctrl.query = UVC_GET_CUR;
    conf.wLayerID = 0;
    conf.dwPeakBitrate = bmax;
    conf.dwAverageBitrate = bmin;
    ctrl.query = UVC_SET_CUR;
    res = xioctl(fd, UVCIOC_CTRL_QUERY, &ctrl);
    if (res) 
    {
        perror("ctrl_query");
        return;
    }
    if (logging && logfile) 
    {
        fprintf(logfile, "%s Setting bitrate peak: %d, average: %d\n", updatetime(), conf.dwPeakBitrate * 8, conf.dwAverageBitrate * 8); 
        fflush(logfile);
    } 
    else 
    {
        fprintf(stderr, "Setting bitrate peak: %d, average: %d\n", conf.dwPeakBitrate * 8, conf.dwAverageBitrate * 8);     
    }
}

static void setRCMode(int mode)
{
    int res;
    struct uvc_xu_control_query ctrl;
    uvcx_rate_control_mode_t conf;
    ctrl.selector = UVCX_RATE_CONTROL_MODE;
    ctrl.size = 3;
    ctrl.unit = 12;
    ctrl.data = (__u8*)&conf;
    conf.wLayerID = 0;
    conf.bRateControlMode = mode;
    ctrl.query = UVC_SET_CUR;
    res = xioctl(fd, UVCIOC_CTRL_QUERY, &ctrl);
    if (res) 
    { 
        perror("UVCIOC_CTRL_QUERY"); 
        return;
    }
    if (logging && logfile) 
    {
        fprintf(logfile, "%s Setting RC mode: %d\n", updatetime(), (int)conf.bRateControlMode); 
        fflush(logfile);
    } 
    else 
    {
        fprintf(stderr, "Setting RC mode: %d\n", (int)conf.bRateControlMode);
    }
}


void setNextFrame(int frame)
{
    int res;
    struct uvc_xu_control_query ctrl;
    uvcx_picture_type_control_t conf;
    ctrl.selector = UVCX_PICTURE_TYPE_CONTROL;
    ctrl.size = 4;
    ctrl.unit = 12;
    ctrl.data = (__u8*)&conf;
    conf.wLayerID = 0;
    conf.wPicType = frame;
    ctrl.query = UVC_SET_CUR;
    res = xioctl(fd, UVCIOC_CTRL_QUERY, &ctrl);
    if (res) { 
        perror("UVCIOC_CTRL_QUERY"); 
        return;
    }
    if (logging && logfile) 
    {
        fprintf(logfile, "%s Request IDR frame now.\n", updatetime()); 
        fflush(logfile);
    } 
    else 
    {
        fprintf(stderr, "Request IDR frame now.\n");
    }
}

void setQP(int tgt, int qpmin, int qpmax)
{
    int res;
    struct uvc_xu_control_query ctrl;
    uvcx_qp_steps_layers_t conf;
    ctrl.selector = UVCX_QP_STEPS_LAYERS;
    ctrl.size = 5;
    ctrl.unit = 12;
    ctrl.data = (__u8*)&conf;
    conf.wLayerID = 0;
    conf.bFrameType = 7;
    conf.bMinQp = qpmin;
    conf.bMaxQp = qpmax;
    conf.bFrameType = tgt;
    ctrl.query = UVC_SET_CUR;
    res = xioctl(fd, UVCIOC_CTRL_QUERY, &ctrl);
    if (res) { 
        perror("UVCIOC_CTRL_QUERY"); 
        return;
    }
    if (logging && logfile) 
    {
        fprintf(logfile, "%s Setting QP min: %d, max: %d\n", updatetime(), (int)conf.bMinQp, (int)conf.bMaxQp); 
        fflush(logfile);
    } 
    else 
    {
        fprintf(stderr, "Setting QP min: %d, max: %d\n", (int)conf.bMinQp, (int)conf.bMaxQp);
    }
}

static void process_image(const void *p, int size)
{
    if (out_buf)
        fwrite(p, size, 1, stdout);

    if (out_file || out_filedate)
        fwrite(p, size, 1, fileout);

    fflush(stderr);
    fflush(stdout);
}

static int is_new_segment() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    int ret = 0;

    ret = (tv.tv_sec - segment_start) > segment_length;

    if (ret)
        segment_start = tv.tv_sec;

    return ret;
}

static unsigned long now_secs() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec;
}

static void get_filename(char* buf) {
    struct timeval tv;
    float ts;
    
    file_count++;

    gettimeofday(&tv, NULL);
    ts = tv.tv_sec * 1000.0;
    ts += tv.tv_usec / 1000.0;

    if (!filepath)
        filepath = "output";

    /* create filename and path with timedate seconds suffix */
    if (out_filedate) {
        sprintf(buf, "%s_%.0f_%04d.raw", filepath, ts, file_count);
    }
    /* else create filename and path as provided with */
    else {
        sprintf(buf, "%s_%04d.raw", filepath, file_count);
    }
}

long long getEpochTimeShift() {
    struct timeval epochtime;
    struct timespec  vsTime;

    gettimeofday(&epochtime, NULL);
    long long epoch_ms = (long long) epochtime.tv_sec * 1000  + (long long) round(epochtime.tv_usec/1000.0);

    clock_gettime(CLOCK_MONOTONIC, &vsTime);
    long long uptime_ms = vsTime.tv_sec * 1000 + (long) round(vsTime.tv_nsec/1000000.0);
    
    fprintf(stdout, "epoch_ms: %lld, uptime_ms: %lld, Timeshift: %lld\n", epoch_ms, uptime_ms, epoch_ms - uptime_ms);
    return epoch_ms - uptime_ms;
}

static int read_frame(void)
{
    struct v4l2_buffer buf;
    unsigned int i;

    switch (io) {
        case IO_METHOD_READ:
            if (-1 == read(fd, buffers[0].start, buffers[0].length)) {
                switch (errno) {
                    case EAGAIN:
                        return 0;

                    case EIO:
                    /* Could ignore EIO, see spec. */

                    /* fall through */
                    default:
                        errno_exit("read");
                }
            }

            process_image(buffers[0].start, buffers[0].length);
            break;

        case IO_METHOD_MMAP:
            CLEAR(buf);

            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;

            if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
                switch (errno) {
                    case EAGAIN:
                    return 0;

                    case EIO:
                    /* Could ignore EIO, see spec. */

                    /* fall through */

                    default:
                    errno_exit("MMAP VIDIOC_DQBUF");
                }
            }

            assert(buf.index < n_buffers);
            process_image(buffers[buf.index].start, buf.bytesused);
            
/*            if (logging && logfile) 
            {
                long temp_ms = 1000 * buf.timestamp.tv_sec + (long) round(  buf.timestamp.tv_usec / 1000.0);
                fprintf(logfile, "%s Frame written with index: %ld, sequence #: %ld, size: %ld, timestamp: %ld\n", updatetime(), (unsigned long)buf.index, (unsigned long)buf.sequence, (unsigned long)buf.bytesused, temp_ms);
                fflush(logfile);
            }
*/
            if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
                errno_exit("MMAP VIDIOC_QBUF");
            break;

        case IO_METHOD_USERPTR:
            CLEAR(buf);

            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_USERPTR;

            if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
                switch (errno) {
                    case EAGAIN:
                        return 0;

                    case EIO:
                        /* Could ignore EIO, see spec. */

                        /* fall through */

                    default:
                        errno_exit("PTR VIDIOC_DQBUF");
                }
            }

            for (i = 0; i < n_buffers; ++i)
                if (buf.m.userptr == (unsigned long)buffers[i].start && buf.length == buffers[i].length)
                    break;

            assert(i < n_buffers);

            process_image((void *)buf.m.userptr, buf.bytesused);

            if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
                errno_exit("PTR VIDIOC_QBUF");
            break;
    }

    return 1;
}

static void uninit_device(void)
{
    unsigned int i;

    if (logging && logfile) 
    {
        fprintf(logfile, "%s unint_device called\n", updatetime()); 
        fflush(logfile);
    }
    switch (io) {
        case IO_METHOD_READ:
            free(buffers[0].start);
            break;

        case IO_METHOD_MMAP:
            for (i = 0; i < n_buffers; ++i)
                if (-1 == munmap(buffers[i].start, buffers[i].length))
                    errno_exit("munmap");
            break;

        case IO_METHOD_USERPTR:
            for (i = 0; i < n_buffers; ++i)
                free(buffers[i].start);
            break;
    }

    free(buffers);
}

static void init_read(unsigned int buffer_size)
{
    if (logging && logfile) 
    {
        fprintf(logfile, "%s init_read called\n", updatetime()); 
        fflush(logfile);
    }

    buffers = calloc(1, sizeof(*buffers));

    if (!buffers) {
        if (logging && logfile) 
        {
            fprintf(logfile, "%s Out of memory\n", updatetime()); 
            fflush(logfile);
        }
        else 
        {
            fprintf(stderr, "Out of memory\n");
        }
        exit(EXIT_FAILURE);
    }

    buffers[0].length = buffer_size;
    buffers[0].start = malloc(buffer_size);

    if (!buffers[0].start) {
        if (logging && logfile) 
        {
            fprintf(logfile, "%s Out of memory\n", updatetime()); 
            fflush(logfile);
        }
        else 
        {
            fprintf(stderr, "Out of memory\n");
        }
        exit(EXIT_FAILURE);
    }
}

static void init_mmap(void)
{
    if (logging && logfile) 
    {
        fprintf(logfile, "%s init_mmap called\n", updatetime()); 
        fflush(logfile);
    }

    struct v4l2_requestbuffers req;

    CLEAR(req);

    req.count = 20;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
        if (EINVAL == errno) {
            if (logging && logfile) 
            {
                fprintf(logfile, "%s %s does not support memory mapping\n", updatetime(), dev_name);
                fflush(logfile);
            }
            else 
            {
                fprintf(stderr, "%s does not support memory mapping\n", dev_name);
            }
            exit(EXIT_FAILURE);
        } else {
            errno_exit("VIDIOC_REQBUFS");
        }
    }

    if (req.count < 5) {
        if (logging && logfile) 
        {
            fprintf(logfile, "%s Insufficient buffer memory on %s\n", updatetime(), dev_name);
            fflush(logfile);
        }
        else 
        {
            fprintf(stderr, "Insufficient buffer memory on %s\n", dev_name);
        }
        exit(EXIT_FAILURE);
    }

    buffers = calloc(req.count, sizeof(*buffers));

    if (!buffers) {
        if (logging && logfile) 
        {
            fprintf(logfile, "%s Unable to allocate memory mapped file buffers\n", updatetime()); 
            fflush(logfile);
        }
        else 
        {
            fprintf(stderr, "Unable to allocate memory mapped file buffers\n");
        }
        exit(EXIT_FAILURE);
    }

    struct v4l2_buffer buf;
    for (n_buffers = 0; n_buffers < req.count; ++n_buffers) {

        CLEAR(buf);

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = n_buffers;

        if (-1 == xioctl(fd, VIDIOC_QUERYBUF, &buf))
          errno_exit("VIDIOC_QUERYBUF");

        buffers[n_buffers].length = buf.length;
        buffers[n_buffers].start = mmap(NULL /* start anywhere */,
                                       buf.length,
                                       PROT_READ | PROT_WRITE /* required */,
                                       MAP_SHARED /* recommended */,
                                       fd, buf.m.offset);

        if (MAP_FAILED == buffers[n_buffers].start)
            errno_exit("mmap");
    }
    if (logging && logfile) 
    {
        fprintf(logfile, "%s Initialized %d mmap file buffers of length: %ld\n", updatetime(), n_buffers, (unsigned long)buf.length); 
        fflush(logfile);
    }

}

static void init_userp(unsigned int buffer_size)
{
    if (logging && logfile) 
    {
        fprintf(logfile, "%s init_userp called\n", updatetime()); 
        fflush(logfile);
    }
    struct v4l2_requestbuffers req;

    CLEAR(req);

    req.count  = 20;
    req.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_USERPTR;

    if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
        if (EINVAL == errno) {
            if (logging && logfile) 
            {
                fprintf(logfile, "%s %s does not support user pointer i/o\n", updatetime(), dev_name);
                fflush(logfile);
            }
            else 
            {
                fprintf(stderr, "%s does not support user pointer i/o\n", dev_name);
            }
            exit(EXIT_FAILURE);
        } else {
            errno_exit("VIDIOC_REQBUFS");
        }
    }

    buffers = calloc(4, sizeof(*buffers));

    if (!buffers) {
        if (logging && logfile) 
        {
            fprintf(logfile, "%s Out of memory\n", updatetime());
            fflush(logfile);
        }
        else 
        {
            fprintf(stderr, "Out of memory\n");
        }
        exit(EXIT_FAILURE);
    }

    for (n_buffers = 0; n_buffers < req.count; ++n_buffers) {
        buffers[n_buffers].length = buffer_size;
        buffers[n_buffers].start = malloc(buffer_size);

        if (!buffers[n_buffers].start) {
            if (logging && logfile) 
            {
            fprintf(logfile, "%s Out of memory\n", updatetime());
                fflush(logfile);
            }
            else 
            {
                fprintf(stderr, "Out of memory\n");
            }
            exit(EXIT_FAILURE);
        }
    }
}

static void init_device(void)
{
    struct v4l2_capability cap;
    struct v4l2_cropcap cropcap;
    struct v4l2_crop crop;
    struct v4l2_format fmt;
    unsigned int min;

    if (logging && logfile) 
    {
        fprintf(logfile, "%s init_device called\n", updatetime()); 
        fflush(logfile);
    }

    if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &cap)) {
        if (EINVAL == errno) {
            if (logging && logfile) 
            {
                fprintf(logfile, "%s %s is no V4L2 device\n", updatetime(), dev_name);
                fflush(logfile);
            }
            else 
            {
                fprintf(stderr, "%s is no V4L2 device\n", dev_name);
            }
            exit(EXIT_FAILURE);
        } else {
            errno_exit("VIDIOC_QUERYCAP");
        }
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        if (logging && logfile) 
        {
            fprintf(logfile, "%s %s is no video capture device\n", updatetime(), dev_name);
            fflush(logfile);
        }
        else 
        {
            fprintf(stderr, "%s is no video capture device\n", dev_name);
        }
        exit(EXIT_FAILURE);
    }

    switch (io) {
        case IO_METHOD_READ:
            if (!(cap.capabilities & V4L2_CAP_READWRITE)) {
                if (logging && logfile) 
                {
                    fprintf(logfile, "%s %s does not support read i/o\n", updatetime(), dev_name);
                    fflush(logfile);
                }
                else 
                {
                    fprintf(stderr, "%s does not support read i/o\n", dev_name);
                }
                exit(EXIT_FAILURE);
            }
            break;

        case IO_METHOD_MMAP:
        case IO_METHOD_USERPTR:
            if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
                if (logging && logfile) 
                {
                    fprintf(logfile, "%s %s does not support streaming i/o\n", updatetime(), dev_name);
                    fflush(logfile);
                }
                else 
                {
                    fprintf(stderr, "%s does not support streaming i/o\n", dev_name);
                }
                exit(EXIT_FAILURE);
            }
            break;
    }


    /* Select video input, video standard and tune here. */
    CLEAR(cropcap);
    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (0 == xioctl(fd, VIDIOC_CROPCAP, &cropcap)) {
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect; /* reset to default */

        if (-1 == xioctl(fd, VIDIOC_S_CROP, &crop)) {
            switch (errno) {
                case EINVAL:
                    /* Cropping not supported. */
                    break;
                default:
                    /* Errors ignored. */
                    break;
            }
        }
    } else {
        /* Errors ignored. */
    }

    CLEAR(fmt);

    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (force_format) {
        if (force_format==3){
            fmt.fmt.pix.width       = 640;
            fmt.fmt.pix.height      = 480;
            fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_MPEG4;
            fmt.fmt.pix.field       = V4L2_FIELD_ANY;
            if (logging && logfile) 
            {
                fprintf(logfile, "%s Setting video format: 640x480 MPEG4\n", updatetime()); 
                fflush(logfile);
            }
        }
        if (force_format==2){
            fmt.fmt.pix.width       = 640;
            fmt.fmt.pix.height      = 480;
            fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_H264;
            fmt.fmt.pix.field       = V4L2_FIELD_ANY;
            if (logging && logfile) 
            {
                fprintf(logfile, "%s Setting video format: 640x480 h264\n", updatetime()); 
                fflush(logfile);
            }
        }
        else if(force_format==1){
            fmt.fmt.pix.width	    = 640;
            fmt.fmt.pix.height	    = 480;
            fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
            fmt.fmt.pix.field	    = V4L2_FIELD_INTERLACED;
            if (logging && logfile) 
            {
                fprintf(logfile, "%s Setting video format: 640x480 YUYV\n", updatetime()); 
                fflush(logfile);
            }
        }

        if (-1 == xioctl(fd, VIDIOC_S_FMT, &fmt))
            errno_exit("VIDIOC_S_FMT");

    /* Note VIDIOC_S_FMT may change width and height. */
    } else {
        /* Preserve original settings as set by v4l2-ctl for example */
        if (-1 == xioctl(fd, VIDIOC_G_FMT, &fmt))
            errno_exit("VIDIOC_G_FMT");
    }

    /* Buggy driver paranoia. */
    min = fmt.fmt.pix.width * 2;
    if (fmt.fmt.pix.bytesperline < min)
        fmt.fmt.pix.bytesperline = min;
        min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
    if (fmt.fmt.pix.sizeimage < min)
        fmt.fmt.pix.sizeimage = min;

    switch (io) {
        case IO_METHOD_READ:
            init_read(fmt.fmt.pix.sizeimage);
            break;

        case IO_METHOD_MMAP:
            init_mmap();
            break;

        case IO_METHOD_USERPTR:
            init_userp(fmt.fmt.pix.sizeimage);
            break;
    }
}

static void close_device(void)
{
    if (logging && logfile) 
    {
        fprintf(logfile, "%s close_device called\n", updatetime()); 
        fflush(logfile);
    }
    if (-1 == close(fd))
        errno_exit("close");

    fd = -1;
}

static void open_device(void)
{
    struct stat st;

    if (logging && logfile) 
    {
        fprintf(logfile, "%s open_device called\n", updatetime()); 
        fflush(logfile);
    }
    if (-1 == stat(dev_name, &st)) {
        if (logging && logfile) 
        {
            fprintf(logfile, "%s Cannot identify '%s': %d, %s\n", updatetime(), dev_name, errno, strerror(errno));
            fflush(logfile);
        }
        else 
        {
            fprintf(stderr, "Cannot identify '%s': %d, %s\n", dev_name, errno, strerror(errno));
        }
        exit(EXIT_FAILURE);
    }

    if (!S_ISCHR(st.st_mode)) {
        if (logging && logfile) 
        {
            fprintf(logfile, "%s %s is no device\n", updatetime(), dev_name);
            fflush(logfile);
        }
        else 
        {
            fprintf(stderr, "%s is no device\n", dev_name);
        }
        exit(EXIT_FAILURE);
    }

    fd = open(dev_name, O_RDWR /* required */ | O_NONBLOCK, 0);

    if (-1 == fd) {
        if (logging && logfile) 
        {
            fprintf(logfile, "%s Cannot open '%s': %d, %s\n", updatetime(), dev_name, errno, strerror(errno));
            fflush(logfile);
        }
        else 
        {
            fprintf(stderr, "Cannot open '%s': %d, %s\n", dev_name, errno, strerror(errno));
        }
        exit(EXIT_FAILURE);
    }
}

static void stop_capturing(void)
{
    enum v4l2_buf_type type;

    if (logging && logfile) 
    {
        fprintf(logfile, "%s stop_capturing called\n", updatetime()); 
        fflush(logfile);
    }

    switch (io) {
        case IO_METHOD_READ:
            /* Nothing to do. */
            break;

        case IO_METHOD_MMAP:
        case IO_METHOD_USERPTR:
            type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            if (-1 == xioctl(fd, VIDIOC_STREAMOFF, &type))
                errno_exit("VIDIOC_STREAMOFF");
            break;
    }
    
    uninit_device();
    close_device();
}

static void start_capturing(void)
{
    unsigned int i;
    enum v4l2_buf_type type;

    open_device();
    init_device();
    if (fps != 30)
        setFramerate(fps);

    //stick this somewhere so that it runs once, on the startup of your capture process
    //  noting, if you hibernate a laptop, you might need to recalc this if you don't restart 
    //  the process after dehibernation
    toEpochOffset_ms = getEpochTimeShift();

    if (logging && logfile) 
    {
        fprintf(logfile, "%s start_capturing called with initial time offset: %lld\n", updatetime(), toEpochOffset_ms); 
        fflush(logfile);
    }

    switch (io) {
        case IO_METHOD_READ:
            /* Nothing to do. */
            break;

        case IO_METHOD_MMAP:
            for (i = 0; i < n_buffers; ++i) {
                struct v4l2_buffer buf;

                CLEAR(buf);
                buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buf.memory = V4L2_MEMORY_MMAP;
                buf.index = i;

                if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
                    errno_exit("VIDIOC_QBUF");
            }
            type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            if (-1 == xioctl(fd, VIDIOC_STREAMON, &type))
                errno_exit("VIDIOC_STREAMON");
            break;

        case IO_METHOD_USERPTR:
            for (i = 0; i < n_buffers; ++i) {
                struct v4l2_buffer buf;

                CLEAR(buf);
                buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buf.memory = V4L2_MEMORY_USERPTR;
                buf.index = i;
                buf.m.userptr = (unsigned long)buffers[i].start;
                buf.length = buffers[i].length;

                if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
                    errno_exit("VIDIOC_QBUF");
            }
            type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            if (-1 == xioctl(fd, VIDIOC_STREAMON, &type))
                errno_exit("VIDIOC_STREAMON");
            break;
    }    
        /* init camera settings for capture once stream has started - they get reset by the driver on stop_capturing */
    if (bitrate > 0)
        setBitrate(bitrate / 8, bitrate / 8);
    if (iframe)
        setNextFrame(0x01);
    if (rcmode >= 0)
        setRCMode(rcmode);
    if (qp > 0)
        setQP(0, qp, qp);
}

int i2c_read( void *buf, int len )
{
    int rc = 0;

    if ( read( handle, buf, len ) != len )
    {
        printf( "I2C read failed: %s\n", strerror( errno ) );
        rc = -1;
    }
    return rc;
}
int i2c_write( void *buf, int len )
{
    int rc = 0;
    
    if ( write( handle, buf, len ) != len ) 
    {
        printf( "I2C write failed: %s\n", strerror( errno ) );
        rc = -1;
    }
    return rc;
}

int register_read( unsigned char reg, unsigned short *data )
{
    int rc = -1;
    unsigned char bite[ 4 ];
    
    bite[ 0 ] = reg;
    if ( i2c_write( bite, 1 ) == 0 )
    {
        if ( i2c_read( bite, 2 ) == 0 )
        {
            *data = ( bite[ 0 ] << 8 ) | bite[ 1 ];
            rc = 0;
        }
    }
    return rc;
}

int get_voltage( int chan, float *mv )
{
    short bus;

    if ( register_read( REG_BUS + ( chan << 1 ), (unsigned short*)&bus ) != 0 )
    {
        return -1;
    }
    *mv = ( float )( bus & 0xFFF8 );
    return 0;
}


int get_current( int chan, float *ma )
{
    unsigned short shunt;

    if ( register_read( REG_SHUNT + ( chan << 1 ), &shunt ) != 0 )
    {
        return -1;
    }
    *ma = (float)shunt / 4;
    return 0;
}


void show_remaining( void )
{
    if (logging && logfile) {
    
        char filename[ 20 ];
        snprintf( filename, 19, "/dev/i2c-%d", i2c_bus );
        handle = open( filename, O_RDWR );    
        float ma0, mv0;

        if ( handle < 0 ) 
        {
            fprintf( logfile, "%s Error opening bus %d: %s\n", updatetime(), i2c_bus, strerror( errno ) );
            exit( 1 );
        }
        if ( ioctl( handle, I2C_SLAVE, i2c_address ) < 0 ) 
        {
            fprintf( logfile, "%s Error setting address %02X: %s\n", updatetime(), i2c_address, strerror( errno ) );
            exit( 1 );
        }
        if ( get_current( 0, &ma0 ) || get_voltage( 0, &mv0 ) )
        {
            fprintf( logfile, "%s Error reading voltage/current\n", updatetime() );
            return;
        }
        fprintf( logfile, "%s CH1 (Battery): %4.2fmV  %4.0fmA,\tRemaining: %.2f,\tCharging: %s\n", updatetime(), mv0/1000, ma0, (mv0 - EmptyCharge) / (FullCharge - EmptyCharge) * 100, ma0 > 0 ? "No" : "Yes");
        fflush( logfile );
        close( handle );
    }
}

static void mainloop(void)
{
    unsigned int count;
    unsigned int loopIsInfinite = 0, durationIsInfinite = 0;
    char fname[128];
    unsigned long elapsed = 0;
    unsigned long start = now_secs();
    unsigned long last = 0;

    /* start video capture stream */
    start_capturing();

    if (frame_count == 0) 
        loopIsInfinite = 1; //infinite loop
    if (duration == 0)
        durationIsInfinite = 1; // forever duration
    count = frame_count; 

    while (((count-- > 0) || loopIsInfinite) && ((elapsed < duration) || durationIsInfinite)) {
    
        /* Capture in files of up to 1 hour in duration each  */ 
        if (is_new_segment() && (out_file || out_filedate)) {
            if (fileout) {
                fclose(fileout);
                if (logging && logfile) 
                {
                    fprintf(logfile, "Captured video to file: %s\n", fname);
                    fflush(logfile);
                }
                else 
                {
                    fprintf(stderr, "Captured video to file: %s\n", fname);
                }

                /* restart video capture stream to flush and reset buffers for subsequent segments */
                stop_capturing();
                start_capturing();
            }

            get_filename(fname);
            fileout = fopen(fname, "w+");
        } 

        for (;;) {
            fd_set fds;
            struct timeval tv;
            int r;

            FD_ZERO(&fds);
            FD_SET(fd, &fds);

            /* Timeout. */
            tv.tv_sec = 2;
            tv.tv_usec = 0;

            r = select(fd + 1, &fds, NULL, NULL, &tv);

            if (-1 == r) {
                if (EINTR == errno)
                    continue;
                 errno_exit("select");
            }

            if (0 == r) {
                if (logging && logfile) 
                {
                    fprintf(logfile, "%s 'Select' socket timeout, aborting\n", updatetime());
                    fflush(logfile);
                }
                else 
                {
                    fprintf(stderr, "select socket timeout, aborting\n");
                }
                exit(EXIT_FAILURE);
            }

            if (read_frame()) {
                break;
            }
        }
        
        elapsed = now_secs() - start;
        
        /* output battery remaining every 60s if option selected */
        if (logbattery && !(elapsed % 60) && last != elapsed) {
            /* stop capturing during battery read from ic2_bus then restart when done */
            stop_capturing();
            last = elapsed;
            show_remaining();
            start_capturing();
        }
        
    } // while
    
    /* stop video stream capture */
    stop_capturing();

    if (fileout) {
        fclose(fileout);
        if (logging && logfile) 
        {
            fprintf(logfile, "%s Captured video to file: %s\n", updatetime(), fname);
            fflush(logfile);
        }
        else 
        {
            fprintf(stderr, "Captured video to file: %s\n", fname);
        }
    }
}


static void usage(FILE *fp, int argc, char **argv)
{
    fprintf(fp, 
            "\nLogitech C390 Video Capture tool\n"
            "Usage: [options]\n\n"
            "   -d | --device name   Video device name [DEVICENAME], default: /dev/video0\n"
            "   -b | --bitrate brate Set bitrate in ko/s (only with RC mode = VBR or CBR)\n"
            "   -q | --qp qp         Set QP (only with RC mode = QP)\n"
            "   -i | --iframe        Send an I-Frame now\n"
            "   -x | --rcmode [mode] Set RC mode [VBR, CBR, QP]\n"
            "   -h | --help          Print this message\n"
            "   -m | --mmap          Use memory mapped buffers [default]\n"
            "   -r | --framerate     Set minimum fps [INT]\n"
            "   -u | --userp         Use application allocated buffers\n"
            "   -o | --output        Outputs stream to stdout\n"
            "   -l | --logcalls      Log ioctl and other calls to capture.log file\n"
            "   -L | --logbattery    Log ioctl and other calls with periodic battery remaining to capture.log file\n"
            "   -f | --format        Force format to 640x480 YUYV\n"
            "   -F | --formatH264    Force format to 640x480 H264\n"
            "   -M | --formatMPEG4   Force format to 640x480 MP4\n"
            "   -w | --writefile     Write output to [PATH/FILENAME].raw\n"
            "   -W | --writefiledate Write output to [PATH/FILENAME]_date.raw\n"
            "   -c | --count         Number of frames to grab [INT] - use 0 for infinite (default)\n"
            "   -t | --time          Duration (capture ends at duration=time or frames captured=count)\n"            
            "   -s | --segment       Segment length in seconds (default 3600)\n"            
            "\n"
            "Example usage: capture -F -o -c 300 > output.raw\n"
            "   Captures 300 frames of H264 at 640x480 and pipes to output.raw\n\n"
            "Example usage: capture -F -c 10 -w /home/data/image\n"
            "   Captures 10 frames of H264 at 640x480 and writes to /home/data/image.raw\n\n"
            "Example usage: capture -F -t 3600 -W /home/data/videograb\n"
            "   Captures 1 hour of H264 at 640x480 and writes to /home/data/videograb_xxxxxxxxxxxx.raw\n\n"
            );
}

static const char short_options[] = "d:b:q:ix:hmr:uolLfFMw:W:c:t:s:";

static const struct option
long_options[] = {
    { "device",       required_argument,  NULL, 'd' },
    { "bitrate",      required_argument,  NULL, 'b' },
    { "qp",           required_argument,  NULL, 'q' },
    { "iframe",       no_argument,        NULL, 'i' },
    { "rcmode",       required_argument,  NULL, 'x' },
    { "help",         no_argument,        NULL, 'h' },
    { "mmap",         no_argument,        NULL, 'm' },
    { "framerate",    required_argument,  NULL, 'r' },
    { "userp",        no_argument,        NULL, 'u' },
    { "output",       no_argument,        NULL, 'o' },
    { "logcalls",     no_argument,        NULL, 'l' },
    { "logbattery",   no_argument,        NULL, 'L' },
    { "format",       no_argument,        NULL, 'f' },
    { "formatH264",   no_argument,        NULL, 'F' },
    { "formatMPEG4",  no_argument,        NULL, 'M' },
    { "writefile",    required_argument,  NULL, 'w' },
    { "writefiledate",required_argument,  NULL, 'W' },
    { "count",        required_argument,  NULL, 'c' },
    { "time",         required_argument,  NULL, 't' },
    { "segment",      required_argument,  NULL, 's' },
    { 0, 0, 0, 0 }
};

void sig_handler(int signum)
{
    switch (signum)
    {
        case SIGINT:
            exit(EXIT_SUCCESS);
            break;

        default:
            break;
    }
}

int main(int argc, char **argv)
{
    signal(SIGINT, sig_handler);

    dev_name = "/dev/video0";

    int cnt = 0;
    for (;;) {
        int idx;
        int c;

        c = getopt_long(argc, argv, short_options, long_options, &idx);

        if (-1 == c) {
            if (cnt == 0) {
                usage(stdout, argc, argv);
                exit(EXIT_SUCCESS);
            }
            else {
                break;
            }
        }
        
        cnt++;

        switch (c) {
            case 0: /* getopt_long() flag */
                break;

            case 'd':
                dev_name = optarg;
                break;

            case 'b':
                errno = 0;
                bitrate = strtol(optarg, NULL, 0);
                if (errno)
                    errno_exit(optarg);
                break;

            case 'q':
                errno = 0;
                qp = strtol(optarg, NULL, 0);
                if (errno)
                    errno_exit(optarg);
                break;

            case 'i':
                iframe = 1;
                break;

            case 'x':
                if (!strcmp(optarg, "VBR"))
                    rcmode = RATECONTROL_VBR;

                if (!strcmp(optarg, "CBR"))
                    rcmode = RATECONTROL_CBR;

                if (!strcmp(optarg, "QP"))
                    rcmode = RATECONTROL_CONST_QP;

                if (rcmode < 0)
                {
                    fprintf(stderr, "Unknown RC mode.\n");
                    usage(stdout, argc, argv);
                    exit(EXIT_SUCCESS);
                }

                break;

            case 'h':
                usage(stdout, argc, argv);
                exit(EXIT_SUCCESS);

            case 'm':
                io = IO_METHOD_MMAP;
                break;

            case 'r':
                fps = strtol(optarg, NULL, 0);
                if (errno)
                    errno_exit(optarg);
                break;

            case 'u':
                io = IO_METHOD_USERPTR;
                break;

            case 'o':
                out_buf++;
                break;

            case 'l':
                logging=1;
                break;

            case 'L':
                logging=1;
                logbattery=1;
                break;

            case 'f':
                force_format=1;
                break;

            case 'F':
                force_format=2;
                break;

            case 'M':
                force_format=3;
                break;

            case 'w':
                out_file++;
                filepath = optarg;
                break;

            case 'W':
                out_filedate++;
                filepath = optarg;
                break;

            case 'c':
                errno = 0;
                frame_count = strtol(optarg, NULL, 0);
                if (errno)
                    errno_exit(optarg);
                break;

            case 't':
                errno = 0;
                duration = strtol(optarg, NULL, 0);
                if (errno)
                    errno_exit(optarg);
                break;

            case 's':
                errno = 0;
                segment_length = strtol(optarg, NULL, 0);
                if (errno)
                    errno_exit(optarg);
                break;

            default:
                usage(stderr, argc, argv);
                exit(EXIT_FAILURE);
        }
    }

    if (logging) {
        logfile = fopen( "/home/kam/data/capture.log", "a+" );
        if (!logfile) {
            fprintf( stderr, "Error opening logfile: /home/kam/data/capture.log");
        } 
        else {
            fprintf ( logfile, "\n%s Starting capture session.\n", updatetime() );

            // Change the file perms to 0777 to allow everyone to read and write.
            chmod("/home/kam/data/capture.log", 0777);
        }
    }

    mainloop();

    if (logging && logfile) {
        fprintf ( logfile, "%s Ending capture session.\n", updatetime() );
        fclose(logfile);
    }

    return 0;
}