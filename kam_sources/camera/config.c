/*
 *  V4L2 video capture example
 *
 *  This program can be used and distributed without restrictions.
 *
 *  This program is provided with the V4L2 API
 *  see http://linuxtv.org/docs.php for more information
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <getopt.h>             /* getopt_long() */

#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <linux/videodev2.h>
#include <linux/uvcvideo.h>

#include "uvch264.h"

struct v4l2_queryctrl queryctrl;
struct v4l2_querymenu querymenu;
struct v4l2_control control;
struct v4l2_cropcap cropcap;
struct v4l2_crop crop;
struct v4l2_format format;
struct v4l2_streamparm stream;
struct v4l2_captureparm *capture;
struct v4l2_outputparm *output;

static char *dev_name;
static int fd = -1;
static int bitrate = 0;
static int qp = 0;
static int rcmode = -1;

static void errno_exit(const char *s)
{
    fprintf(stderr, "%s error %d, %s\n", s, errno, strerror(errno));
    exit(EXIT_FAILURE);
}

static int xioctl(int fh, int request, void *arg)
{
    int r;

    do {
        r = ioctl(fh, request, arg);
    } while (-1 == r && EINTR == errno);

    return r;
}

static void setBitrate(int bmin, int bmax)
{  
    int res;
    struct uvc_xu_control_query ctrl;
    uvcx_bitrate_layers_t  conf;
    ctrl.unit = 12;
    ctrl.size = 10;
    ctrl.selector = UVCX_BITRATE_LAYERS;
    ctrl.data = (__u8*)&conf;
    ctrl.query = UVC_GET_CUR;
    conf.wLayerID = 0;
    conf.dwPeakBitrate = conf.dwAverageBitrate = 0;
    res = xioctl(fd, UVCIOC_CTRL_QUERY, &ctrl);
    if (res) {
        perror("ctrl_query");
        return;
    }
    fprintf(stderr, "Bitrate was %d [%d]\n", conf.dwPeakBitrate * 8, conf.dwAverageBitrate * 8);
    conf.dwPeakBitrate = bmax;
    conf.dwAverageBitrate = bmin;
    ctrl.query = UVC_SET_CUR;
    res = xioctl(fd, UVCIOC_CTRL_QUERY, &ctrl);
    if (res) {
        perror("ctrl_query");
        return;
    }
    fprintf(stderr, "Bitrate request %d [%d]\n", conf.dwPeakBitrate * 8, conf.dwAverageBitrate * 8);
    ctrl.query = UVC_GET_CUR;
    res = xioctl(fd, UVCIOC_CTRL_QUERY, &ctrl);
    if (res) {
        perror("ctrl_query");
        return;
    }
    fprintf(stderr, "Bitrate now %d [%d]\n", conf.dwPeakBitrate * 8, conf.dwAverageBitrate * 8);
}

static void setRCMode(int mode)
{
    int res;
    struct uvc_xu_control_query ctrl;
    uvcx_rate_control_mode_t conf;
    ctrl.selector = UVCX_RATE_CONTROL_MODE;
    ctrl.size = 3;
    ctrl.unit = 12;
    ctrl.data = (__u8*)&conf;
    ctrl.query = UVC_GET_CUR;
    conf.wLayerID = 0;
    res = xioctl(fd, UVCIOC_CTRL_QUERY, &ctrl);
    if (res) { 
        perror("query"); 
        return;
    }
    fprintf(stderr, "RC mode was %d\n", (int)conf.bRateControlMode);
    conf.bRateControlMode = mode;
    ctrl.query = UVC_SET_CUR;
    res = xioctl(fd, UVCIOC_CTRL_QUERY, &ctrl);
    if (res) { 
        perror("query"); 
        return;
    }
    fprintf(stderr, "RC mode request %d\n", (int)conf.bRateControlMode);
    ctrl.query = UVC_GET_CUR;
    res = xioctl(fd, UVCIOC_CTRL_QUERY, &ctrl);
    if (res) { 
        perror("query"); 
        return;
    }
    fprintf(stderr, "RC mode now %d\n", (int)conf.bRateControlMode);
}

void setQP(int tgt, int qpmin, int qpmax)
{
    int res;
    struct uvc_xu_control_query ctrl;
    uvcx_qp_steps_layers_t conf;
    ctrl.selector = UVCX_QP_STEPS_LAYERS;
    ctrl.size = 5;
    ctrl.unit = 12;
    ctrl.data = (__u8*)&conf;
    ctrl.query = UVC_GET_CUR;
    conf.wLayerID = 0;
    conf.bFrameType = 7;
    conf.bMinQp = conf.bMaxQp = 0;
    res = xioctl(fd, UVCIOC_CTRL_QUERY, &ctrl);
    if (res) { 
        perror("query"); 
        return;
    }
    fprintf(stderr, "QP was %d / %d\n", (int)conf.bMinQp, (int)conf.bMaxQp);
    conf.bMinQp = qpmin;
    conf.bMaxQp = qpmax;
    conf.bFrameType = tgt;
    ctrl.query = UVC_SET_CUR;
    res = xioctl(fd, UVCIOC_CTRL_QUERY, &ctrl);
    if (res) { 
        perror("query");
        return;
    }
    fprintf(stderr, "QP request %d / %d\n", (int)conf.bMinQp, (int)conf.bMaxQp);
    ctrl.query = UVC_GET_CUR;
    res = xioctl(fd, UVCIOC_CTRL_QUERY, &ctrl);
    if (res) { 
        perror("query"); 
        return;
    }
    fprintf(stderr, "QP now %d / %d\n", (int)conf.bMinQp, (int)conf.bMaxQp);
}

static void close_device(void)
{
    if (-1 == close(fd))
        errno_exit("close");
  
    fd = -1;
}

static void open_device(void)
{
    struct stat st;

    if (-1 == stat(dev_name, &st)) {
        fprintf(stderr, "Cannot identify '%s': %d, %s\n", dev_name, errno, strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (!S_ISCHR(st.st_mode)) {
        fprintf(stderr, "%s is no device\n", dev_name);
        exit(EXIT_FAILURE);
    }

    fd = open(dev_name, O_RDWR /* required */ | O_NONBLOCK, 0);

    if (-1 == fd) {
        fprintf(stderr, "Cannot open '%s': %d, %s\n", dev_name, errno, strerror(errno));
        exit(EXIT_FAILURE);
    }
}

static void print_video_input(void)
{
    printf(" -- print_video_input --\n");

    struct v4l2_input input;
    int index;

    if (-1 == ioctl(fd, VIDIOC_G_INPUT, &index)) {
        perror("VIDIOC_G_INPUT");
        exit(EXIT_FAILURE);
    }

    memset(&input, 0, sizeof(input));
    input.index = index;

    if (-1 == ioctl(fd, VIDIOC_ENUMINPUT, &input)) {
        perror("VIDIOC_ENUMINPUT");
        exit(EXIT_FAILURE);
    }

    printf("Current input: %s\n", input.name);
    printf("\n");
}

static void print_scale_metrics(void)
{
    double hscale, vscale;
    double aspect;
    int dwidth, dheight;

    printf(" -- print_scale_metrics --\n");

    memset (&cropcap, 0, sizeof (cropcap));
    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (-1 == ioctl (fd, VIDIOC_CROPCAP, &cropcap)) {
        perror ("VIDIOC_CROPCAP");
        exit (EXIT_FAILURE);
    }

    memset (&crop, 0, sizeof (crop));
    crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (-1 == ioctl (fd, VIDIOC_G_CROP, &crop)) {
        if (errno != EINVAL) {
            perror ("VIDIOC_G_CROP");
            exit (EXIT_FAILURE);
        }

        /* Cropping not supported. */
        crop.c = cropcap.defrect;
    }

    memset (&format, 0, sizeof (format));
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (-1 == ioctl (fd, VIDIOC_G_FMT, &format)) {
        perror ("VIDIOC_G_FMT");
        exit (EXIT_FAILURE);
    }

    /* The scaling applied by the driver. */

    hscale = format.fmt.pix.width / (double) crop.c.width;
    vscale = format.fmt.pix.height / (double) crop.c.height;

    aspect = cropcap.pixelaspect.numerator / (double) cropcap.pixelaspect.denominator;
    aspect = aspect * hscale / vscale;
    
    printf("hscale: %f, vscale: %f, aspect: %f\n", hscale, vscale, aspect);

    /* Devices following ITU-R BT.601 do not capture
       square pixels. For playback on a computer monitor
       we should scale the images to this size. */

    dwidth = format.fmt.pix.width / aspect;
    dheight = format.fmt.pix.height;
    printf("pixel width: %d, height: %d\n", dwidth, dheight);
    printf("\n");
}

static void print_formats(void) {
    struct v4l2_fmtdesc vfd = { .type = V4L2_BUF_TYPE_VIDEO_CAPTURE };
 
    while(!ioctl(fd, VIDIOC_ENUM_FMT, &vfd)) {
        vfd.index++;

        struct v4l2_frmivalenum vfi;
        vfi.pixel_format = vfd.pixelformat;
        vfi.index = 0;
        vfi.width = 640;
        vfi.height = 480;
        while(!ioctl(fd, VIDIOC_ENUM_FRAMEINTERVALS, &vfi)) {
            if (vfi.type == V4L2_FRMIVAL_TYPE_DISCRETE) {
                vfi.index++;
                printf("Pixel format %u, frame interval: %u / %u\n", vfi.pixel_format, vfi.discrete.numerator, vfi.discrete.denominator);
            } else {
                printf("No discrete frame interval types available\n");
                break;
            }
        }
    }
}

static void enumerate_menu(void)
{
	printf("     Menu items:\n");

	memset(&querymenu, 0, sizeof(querymenu));
	querymenu.id = queryctrl.id;

	for (querymenu.index = queryctrl.minimum;
	     querymenu.index <= queryctrl.maximum;
	     querymenu.index++) {
		if (0 == ioctl(fd, VIDIOC_QUERYMENU, &querymenu)) {
			printf("        %s\n", querymenu.name);
		}
	}
}

static void print_user_controls(void)
{
    printf(" -- print_user_controls --\n");
    int value;
    
    memset(&queryctrl, 0, sizeof(queryctrl));

    /* user class */
    printf("User class controls\n");
    queryctrl.id = V4L2_CTRL_CLASS_USER | V4L2_CTRL_FLAG_NEXT_CTRL;
    while (0 == ioctl(fd, VIDIOC_QUERYCTRL, &queryctrl)) {
        if (V4L2_CTRL_ID2CLASS(queryctrl.id) != V4L2_CTRL_CLASS_USER)
            break;
        if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
            continue;

        memset(&control, 0, sizeof(control));
        control.id = queryctrl.id;
        if (0 == ioctl(fd, VIDIOC_G_CTRL, &control)) {
            value = control.value;
        } else {
            value = -1;
        }

        printf("   queryctrl.id: 0x%08X, name: %s, value: %d, min: %d, max: %d, def: %d\n", 
               queryctrl.id, queryctrl.name, value, queryctrl.minimum, queryctrl.maximum, queryctrl.default_value);

        if (queryctrl.type == V4L2_CTRL_TYPE_MENU)
            enumerate_menu();

        queryctrl.id |= V4L2_CTRL_FLAG_NEXT_CTRL;
    }
    /* camera class */
    printf("Camera class controls\n");
    queryctrl.id = V4L2_CTRL_CLASS_CAMERA | V4L2_CTRL_FLAG_NEXT_CTRL;
    while (0 == ioctl(fd, VIDIOC_QUERYCTRL, &queryctrl)) {
        if (V4L2_CTRL_ID2CLASS(queryctrl.id) != V4L2_CTRL_CLASS_CAMERA)
            break;
        if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
            continue;

        memset(&control, 0, sizeof(control));
        control.id = queryctrl.id;
        if (0 == ioctl(fd, VIDIOC_G_CTRL, &control)) {
            value = control.value;
        } else {
            value = -1;
        }

        printf("   queryctrl.id: 0x%08X, name: %s, value: %d, min: %d, max: %d, def: %d\n", 
               queryctrl.id, queryctrl.name, value, queryctrl.minimum, queryctrl.maximum, queryctrl.default_value);

        if (queryctrl.type == V4L2_CTRL_TYPE_MENU)
            enumerate_menu();

        queryctrl.id |= V4L2_CTRL_FLAG_NEXT_CTRL;
    }
    printf("\n");
    
    /* streaming settings */
    memset(&stream, 0, sizeof(stream));
    stream.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    printf("Camera streaming controls\n");
    if (0 == ioctl(fd, VIDIOC_G_PARM, &stream)) {
        capture = (struct v4l2_captureparm *) &stream.parm;        
        printf("Streaming capture capability: %ld, capture mode: %ld, timeperframe: %ld/%ld\n", (unsigned long) capture->capability, (unsigned long) capture->capturemode, (unsigned long) capture->timeperframe.numerator, (unsigned long) capture->timeperframe.denominator);
    } else {
        fprintf(stderr, "VIDIOC_G_PARM error %d, %s\n", errno, strerror(errno));
    }

    /* supported video formats and framerates */
    print_formats();
    
    printf("\n");
    
}

static void set_control_value(int ctrlid, int newval)
{
    memset(&queryctrl, 0, sizeof(queryctrl));
    queryctrl.id = ctrlid;
    if (0 == ioctl(fd, VIDIOC_QUERYCTRL, &queryctrl)) {
        /* make sure its a valid value */
        if (queryctrl.minimum <= newval && newval <= queryctrl.maximum) {
            memset(&control, 0, sizeof(control));
            control.id = ctrlid;
            control.value = newval;
            if (0 == ioctl(fd, VIDIOC_S_CTRL, &control)) {
                printf("Successfully set %s: new value: %d\n", queryctrl.name, newval);
            } else {
                printf("FAILED to set %s: with new value: %d\n", queryctrl.name, newval);
            }
        }
    }
}

static void usage(FILE *fp, int argc, char **argv)
{
    fprintf(fp,
          "\nUsage: %s [option value] [option value] ...\n"
          "Options:\n"
          " -p | --print         Print all available settings and values\n"
          " -q | --qp            Set QP (only with RC mode = QP)\n"
          " -r | --rcmode        Set RC mode [VBR, CBR, QP]\n"
          " -b | --brightness    Brightness value 0-255, def: 128\n"
          " -c | --contrast      Contrast value 0-255, def: 128\n"
          " -s | --saturation    Saturation value 0-255, def: 128\n"
          " -g | --gain          Gain value 0-255, def: 0\n"
          " -0 | --bitrate       Set bitrate in ko/s (only with RC mode = VBR or CBR)\n"
          " -1 | --wbt_auto      White Balance Temerature Auto value 0-1, def: 1\n"
          " -2 | --wbt           White Balance Temperature value 2000-6500, def: 4000\n"
          " -3 | --sharpness     Sharpness value 0-255, def: 128\n"
          " -4 | --backlight     Backlight Compensation value 0-1, def: 0\n"
          " -5 | --exp_abs       Exposure Absolute value 3-2047, def: 250\n"
          " -6 | --exp_pri       Exposure Auto Priority value 0-1, def: 1\n"
          " -7 | --pan           Pan value -36000-36000, def 0\n"
          " -t | --tilt          Tile value -36000-36000, def 0\n"
          " -f | --focus         Focus value 0-250, def: 0\n"
          " -a | --focus_auto    Focus Auto 0-1, def: 1\n"
          " -z | --zoom          Zoom value 100-500, def: 100\n"
          "\n",
          argv[0]);
}

static const char short_options[] = "pq:r:b:c:s:g:0:1:2:3:4:5:6:7:t:f:a:z:";

static const struct option
    long_options[] = {
        { "print",          required_argument, NULL, 'p' },
        { "qp",             required_argument, NULL, 'q' },
        { "rcmode",         required_argument, NULL, 'r' },
        { "brightness",     required_argument, NULL, 'b' },
        { "contrast",       required_argument, NULL, 'c' },
        { "saturation",     required_argument, NULL, 's' },
        { "gain",           required_argument, NULL, 'g' },
        { "bitrate",        required_argument, NULL, '0' },
        { "wbt_auto",       required_argument, NULL, '1' },
        { "wbt",            required_argument, NULL, '2' },
        { "sharpness",      required_argument, NULL, '3' },
        { "backlight",      required_argument, NULL, '4' },
        { "exp_abs",        required_argument, NULL, '5' },
        { "exp_pri",        required_argument, NULL, '6' },
        { "pan",            required_argument, NULL, '7' },
        { "tilt",           required_argument, NULL, 't' },
        { "focus",          required_argument, NULL, 'f' },
        { "focus_auto",     required_argument, NULL, 'a' },
        { "zoom",           required_argument, NULL, 'z' },
        { 0, 0, 0, 0 }
    };

int main(int argc, char **argv)
{
    dev_name = "/dev/video0";

    if (argc == 1) {
        usage(stdout, argc, argv);
        exit(EXIT_SUCCESS);
    }

    open_device();

    for (;;) {
        int idx;
        int c;

        c = getopt_long(argc, argv, short_options, long_options, &idx);

        if (-1 == c)
            break;

        switch (c) {
            case 0: /* getopt_long() flag */
                break;
          
            case 'p':
                printf("\n");
                print_video_input();
                print_user_controls();
                print_scale_metrics();    
                break;

            case 'q':
                errno = 0;
                qp = strtol(optarg, NULL, 0);
                if (errno)
                    errno_exit(optarg);
                if (qp > 0)
                    setQP(0, qp, qp);
                break;
          
            case 'r':
                if (!strcmp(optarg, "VBR"))
                    rcmode = RATECONTROL_VBR;
              
                if (!strcmp(optarg, "CBR"))
                    rcmode = RATECONTROL_CBR;
              
                if (!strcmp(optarg, "QP"))
                    rcmode = RATECONTROL_CONST_QP;
              
                if (rcmode < 0) {
                    fprintf(stderr, "Unknown RC mode.\n");
                    usage(stdout, argc, argv);
                    exit(EXIT_SUCCESS);
                }
                setRCMode(rcmode);

                break;
          
            case '0':
                errno = 0;
                bitrate = strtol(optarg, NULL, 0);
                if (errno)
                    errno_exit(optarg);
                if (bitrate > 0)
                    setBitrate(bitrate / 8, bitrate / 8);
                break;

            case 'b':
                set_control_value(0x00980900, strtol(optarg, NULL, 0));
                break;                
            case 'c':
                set_control_value(0x00980901, strtol(optarg, NULL, 0));
                break;                
            case 's':
                set_control_value(0x00980902, strtol(optarg, NULL, 0));
                break;                
            case 'g':
                set_control_value(0x00980913, strtol(optarg, NULL, 0));
                break;                
            case '1':
                set_control_value(0x0098090C, strtol(optarg, NULL, 0));
                break;                
            case '2':
                set_control_value(0x0098091A, strtol(optarg, NULL, 0));
                break;                
            case '3':
                set_control_value(0x0098091B, strtol(optarg, NULL, 0));
                break;                
            case '4':
                set_control_value(0x0098091C, strtol(optarg, NULL, 0));
                break;                
            case '5':
                set_control_value(0x009A0902, strtol(optarg, NULL, 0));
                break;                
            case '6':
                set_control_value(0x009A0903, strtol(optarg, NULL, 0));
                break;                
            case '7':
                set_control_value(0x009A0908, strtol(optarg, NULL, 0));
                break;                
            case 't':
                set_control_value(0x009A0909, strtol(optarg, NULL, 0));
                break;                
            case 'f':
                set_control_value(0x009A090A, strtol(optarg, NULL, 0));
                break;                
            case 'a':
                set_control_value(0x009A090C, strtol(optarg, NULL, 0));
                break;                
            case 'z':
                set_control_value(0x009A090D, strtol(optarg, NULL, 0));
                break;                

            default:
                usage(stderr, argc, argv);
                exit(EXIT_FAILURE);
        }
    }

    close_device();
    printf("\n");
    return 0;
    
}
