PHP_ARG_ENABLE(idax, whether to enable IDAX 
API support,
[ --enable-idax   Enable IDAX API support])

if test "$PHP_IDAX" = "yes"; then
  AC_DEFINE(HAVE_IDAX, 1, [Whether you have IDAX API])
  PHP_NEW_EXTENSION(idax, idax.c vl42device.c, $ext_shared)
fi
