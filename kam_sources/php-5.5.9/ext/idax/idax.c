#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_idax.h"

#include "vl42device.h"

static zend_function_entry idax_functions[] =
{
    PHP_FE(KamOpen, NULL)
    PHP_FE(KamClose, NULL)
    PHP_FE(KamGetBrightness, NULL)
    PHP_FE(KamSetBrightness, NULL)
    PHP_FE(KamGetContrast, NULL)
    PHP_FE(KamSetContrast, NULL)
    PHP_FE(KamGetSaturation, NULL)
    PHP_FE(KamSetSaturation, NULL)
    PHP_FE(KamGetGain, NULL)
    PHP_FE(KamSetGain, NULL)
    PHP_FE(KamGetAutoWhiteBalance, NULL)
    PHP_FE(KamSetAutoWhiteBalance, NULL)
    PHP_FE(KamGetWhiteBalance, NULL)
    PHP_FE(KamSetWhiteBalance, NULL)
    PHP_FE(KamGetSharpness, NULL)
    PHP_FE(KamSetSharpness, NULL)
    PHP_FE(KamGetBacklight, NULL)
    PHP_FE(KamSetBacklight, NULL)
    PHP_FE(KamGetAbsoluteExposure, NULL)
    PHP_FE(KamSetAbsoluteExposure, NULL)
    PHP_FE(KamGetExposurePriority, NULL)
    PHP_FE(KamSetExposurePriority, NULL)
    PHP_FE(KamGetPan, NULL)
    PHP_FE(KamSetPan, NULL)
    PHP_FE(KamGetTilt, NULL)
    PHP_FE(KamSetTilt, NULL)
    PHP_FE(KamGetFocus, NULL)
    PHP_FE(KamSetFocus, NULL)
    PHP_FE(KamGetAutoFocus, NULL)
    PHP_FE(KamSetAutoFocus, NULL)
    PHP_FE(KamGetZoom, NULL)
    PHP_FE(KamSetZoom, NULL)
    PHP_FE(KamKaptureImage, NULL)
    PHP_FE(KamStreamStart, NULL)
    PHP_FE(KamStreamStop, NULL)
    {NULL, NULL, NULL}
};

zend_module_entry idax_module_entry =
{
#if ZEND_MODULE_API_NO >= 20010901
    STANDARD_MODULE_HEADER,
#endif
    PHP_IDAX_EXTNAME,
    idax_functions,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
#if ZEND_MODULE_API_NO >= 20010901
    PHP_IDAX_VERSION,
#endif
    STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_IDAX
ZEND_GET_MODULE(idax)
#endif

typedef enum _CAMERA_OPTIONS
{
    KamOptionBrightness = 0x00980900,
    KamOptionContrast = 0x00980901,
    KamOptionSaturation = 0x00980902,
    KamOptionGain = 0x00980913,
    KamOptionAutoWhiteBalance = 0x0098090C,
    KamOptionWhiteBalance = 0x0098091A,
    KamOptionSharpness = 0x0098091B,
    KamOptionBacklight = 0x0098091C,
    KamOptionAbsoluteExposure = 0x009A0902,
    KamOptionExposurePriority = 0x009A0903,
    KamOptionPan = 0x009A0908,
    KamOptionTilt = 0x009A0909,
    KamOptionFocus = 0x009A090A,
    KamOptionAutoFocus = 0x009A090C,
    KamOptionZoom = 0x009A090D
} CAMERA_OPTIONS;

PHP_FUNCTION(KamOpen)
{
    int rc;
    int fd = -1;

    rc = vl42_open(&fd);

    RETURN_LONG(fd);
}

PHP_FUNCTION(KamClose)
{
    /* input parameters */
    int rc;
    long fd;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_close(fd);

    RETURN_LONG(rc);
}

// PHP_FUNCTION(KamGetBrightness)
// {
//     int rc;

//     /* input parameters */
//     long fd;
//     zval *out_disabled;
//     int disabled;
//     zval *out_minValue;
//     int minValue;
//     zval *out_maxValue;
//     int maxValue;
//     zval *out_defaultValue;
//     int defaultValue;
//     zval *out_currentValue;
//     int currentValue;

//     /* read the input parameters */
//     if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "lzzzzz", &fd, &out_disabled, &out_minValue, &out_maxValue, &out_defaultValue, &out_currentValue) != SUCCESS)
//     {
//         return;
//     }

//     rc = vl42_get_control_value(fd, KamOptionBrightness, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

//     if (rc)
//     {
//         RETURN_FALSE;
//     }
//     else
//     {
//         zval_dtor(out_disabled);
//         ZVAL_LONG(out_disabled, disabled);

//         if (!disabled)
//         {
//             zval_dtor(out_minValue);
//             ZVAL_LONG(out_minValue, minValue);

//             zval_dtor(out_maxValue);
//             ZVAL_LONG(out_maxValue, maxValue);

//             zval_dtor(out_defaultValue);
//             ZVAL_LONG(out_defaultValue, defaultValue);

//             zval_dtor(out_currentValue);
//             ZVAL_LONG(out_currentValue, currentValue);
//         }

//         RETURN_LONG(currentValue);
//     }
// }

PHP_FUNCTION(KamGetBrightness)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionBrightness, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetBrightness)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionBrightness, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetContrast)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionContrast, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetContrast)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionContrast, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetSaturation)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionSaturation, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetSaturation)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionSaturation, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetGain)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionGain, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetGain)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionGain, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetAutoWhiteBalance)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionAutoWhiteBalance, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetAutoWhiteBalance)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionAutoWhiteBalance, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetWhiteBalance)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionWhiteBalance, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetWhiteBalance)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionWhiteBalance, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetSharpness)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionSharpness, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetSharpness)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionSharpness, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetBacklight)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionBacklight, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetBacklight)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionBacklight, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetAbsoluteExposure)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionAbsoluteExposure, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetAbsoluteExposure)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionAbsoluteExposure, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetExposurePriority)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionExposurePriority, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetExposurePriority)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionExposurePriority, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetPan)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionPan, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetPan)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionPan, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetTilt)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionTilt, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetTilt)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionTilt, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetFocus)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionFocus, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetFocus)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionFocus, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetAutoFocus)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionAutoFocus, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetAutoFocus)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionAutoFocus, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamGetZoom)
{
    int rc;

    long fd;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    int currentValue;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &fd) != SUCCESS)
    {
        return;
    }

    rc = vl42_get_control_value(fd, KamOptionZoom, &disabled, &minValue, &maxValue, &defaultValue, &currentValue);

    if (rc)
    {
        RETURN_FALSE;
    }
    else
    {
        RETURN_LONG(currentValue);
    }
}

PHP_FUNCTION(KamSetZoom)
{
    int rc;

    /* input parameters */
    long fd;
    long value;

    /* read the input parameters */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &fd, &value) != SUCCESS)
    {
        return;
    }

    rc = vl42_set_control_value(fd, KamOptionZoom, value);

    RETURN_LONG(rc);
}

PHP_FUNCTION(KamKaptureImage)
{
    RETURN_STRING("KamKaptureImage", 1);
}

PHP_FUNCTION(KamStreamStart)
{
    RETURN_STRING("KamStreamStart", 1);
}

PHP_FUNCTION(KamStreamStop)
{
    RETURN_STRING("KamStreamStop", 1);
}
