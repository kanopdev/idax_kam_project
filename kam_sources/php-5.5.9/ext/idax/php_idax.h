#ifndef _PHP_IDAX_H_

#define _PHP_IDAX_H_ 1

#define PHP_IDAX_VERSION "1.0"
#define PHP_IDAX_EXTNAME "idax"

PHP_FUNCTION(KamOpen);
PHP_FUNCTION(KamClose);
PHP_FUNCTION(KamGetBrightness);
PHP_FUNCTION(KamSetBrightness);
PHP_FUNCTION(KamGetContrast);
PHP_FUNCTION(KamSetContrast);
PHP_FUNCTION(KamGetSaturation);
PHP_FUNCTION(KamSetSaturation);
PHP_FUNCTION(KamGetGain);
PHP_FUNCTION(KamSetGain);
PHP_FUNCTION(KamGetAutoWhiteBalance);
PHP_FUNCTION(KamSetAutoWhiteBalance);
PHP_FUNCTION(KamGetWhiteBalance);
PHP_FUNCTION(KamSetWhiteBalance);
PHP_FUNCTION(KamGetSharpness);
PHP_FUNCTION(KamSetSharpness);
PHP_FUNCTION(KamGetBacklight);
PHP_FUNCTION(KamSetBacklight);
PHP_FUNCTION(KamGetAbsoluteExposure);
PHP_FUNCTION(KamSetAbsoluteExposure);
PHP_FUNCTION(KamGetExposurePriority);
PHP_FUNCTION(KamSetExposurePriority);
PHP_FUNCTION(KamGetPan);
PHP_FUNCTION(KamSetPan);
PHP_FUNCTION(KamGetTilt);
PHP_FUNCTION(KamSetTilt);
PHP_FUNCTION(KamGetFocus);
PHP_FUNCTION(KamSetFocus);
PHP_FUNCTION(KamGetAutoFocus);
PHP_FUNCTION(KamSetAutoFocus);
PHP_FUNCTION(KamGetZoom);
PHP_FUNCTION(KamSetZoom);
PHP_FUNCTION(KamKaptureImage);
PHP_FUNCTION(KamStreamStart);
PHP_FUNCTION(KamStreamStop);

extern zend_module_entry idax_module_entry;

#define phpext_KamOpen_ptr             &ida_module_entry
#define phpext_KamClose_ptr            &ida_module_entry
#define phpext_KamGetBrightness        &ida_module_entry
#define phpext_KamSetBrightness        &ida_module_entry
#define phpext_KamGetContrast          &ida_module_entry
#define phpext_KamSetContrast          &ida_module_entry
#define phpext_KamGetSaturation        &ida_module_entry
#define phpext_KamSetSaturation        &ida_module_entry
#define phpext_KamGetGain              &ida_module_entry
#define phpext_KamSetGain              &ida_module_entry
#define phpext_KamGetAutoWhiteBalance  &ida_module_entry
#define phpext_KamSetAutoWhiteBalance  &ida_module_entry
#define phpext_KamGetWhiteBalance      &ida_module_entry
#define phpext_KamSetWhiteBalance      &ida_module_entry
#define phpext_KamGetSharpness         &ida_module_entry
#define phpext_KamSetSharpness         &ida_module_entry
#define phpext_KamGetBacklight         &ida_module_entry
#define phpext_KamSetBacklight         &ida_module_entry
#define phpext_KamGetAbsoluteExposure  &ida_module_entry
#define phpext_KamSetAbsoluteExposure  &ida_module_entry
#define phpext_KamGetExposurePriority  &ida_module_entry
#define phpext_KamSetExposurePriority  &ida_module_entry
#define phpext_KamGetPan               &ida_module_entry
#define phpext_KamSetPan               &ida_module_entry
#define phpext_KamGetTilt              &ida_module_entry
#define phpext_KamSetTilt              &ida_module_entry
#define phpext_KamGetFocus             &ida_module_entry
#define phpext_KamSetFocus             &ida_module_entry
#define phpext_KamGetAutoFocus         &ida_module_entry
#define phpext_KamSetAutoFocus         &ida_module_entry
#define phpext_KamGetZoom              &ida_module_entry
#define phpext_KamSetZoom              &ida_module_entry
#define phpext_KamKaptureImage_ptr     &ida_module_entry
#define phpext_KamStreamStart_ptr      &ida_module_entry
#define phpext_KamStreamStop_ptr       &ida_module_entry

#endif  // _PHP_IDAX_H_
