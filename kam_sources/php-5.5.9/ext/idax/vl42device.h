#ifndef _VL42DEVICE_H_
#define _VL42DEVICE_H_

/*
 *  Device APIs
 */

int vl42_open(int *fd);
int vl42_close(int fd);
int vl42_set_control_value(int fd, int ctrlid, int value);
int vl42_get_control_info(int fd, int ctrlid, int *disabled, int *minValue, int *maxValus, int *defaultValue);
int vl42_get_control_value(int fd, int ctrlid, int *disabled, int *minValue, int *maxValus, int *defaultValue, int *currentValue);

#endif  // _VL42DEVICE_H_
