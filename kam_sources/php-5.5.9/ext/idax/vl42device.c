/*
 *  Device APIs
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <getopt.h>             /* getopt_long() */

#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <linux/videodev2.h>
#include <linux/uvcvideo.h>

#include "uvch264.h"
#include "vl42device.h"

static char *dev_name = "/dev/video0";

int vl42_open(
    int *fd
    )
{
    struct stat st;

    if (-1 == stat(dev_name, &st))
    {
        return errno;
    }

    if (!S_ISCHR(st.st_mode))
    {
        return EACCES;
    }

    *fd = open(dev_name, O_RDWR /* required */ | O_NONBLOCK, 0);

    if (-1 == *fd)
    {
        return errno;
    }

    return 0;
}

int vl42_close(
    int fd
    )
{
    if (-1 != close(fd))
    {
        return errno;
    }

    return 0;
}

int vl42_get_control_info(
    int fd,
    int ctrlid,
    int *disabled,
    int *minValue,
    int *maxValue,
    int *defaultValue
    )
{
    struct v4l2_queryctrl queryctrl;
    int rc = -1;

    memset(&queryctrl, 0, sizeof(queryctrl));

    queryctrl.id = ctrlid;

    rc = ioctl(fd, VIDIOC_QUERYCTRL, &queryctrl);

    if (rc != -1)
    {
        *disabled = queryctrl.flags & V4L2_CTRL_FLAG_DISABLED;

        if (!(*disabled))
        {
            *minValue = queryctrl.minimum;
            *maxValue = queryctrl.maximum;
            *defaultValue = queryctrl.default_value;
        }
    }
    else
    {
        rc = errno;
    }

    return rc;
}

int vl42_set_control_value(
    int fd,
    int ctrlid,
    int value
    )
{
    int rc;
    int disabled;
    int minValue;
    int maxValue;
    int defaultValue;
    struct v4l2_control control;

    rc = vl42_get_control_info(fd, ctrlid, &disabled, &minValue, &maxValue, &defaultValue);

    if (rc == 0)
    {
        if (!disabled && (minValue <= value) && (value <= maxValue))
        {
            memset(&control, 0, sizeof(control));

            control.id = ctrlid;
            control.value = value;

            if (-1 != ioctl(fd, VIDIOC_S_CTRL, &control))
            {
                rc = 0;
            }
            else
            {
                rc = errno;
            }
        }
        else
        {
            rc = EINVAL;
        }
    }

    return rc;
}

int vl42_get_control_value(
    int fd,
    int ctrlid,
    int *disabled,
    int *minValue,
    int *maxValue,
    int *defaultValue,
    int *currenValue
    )
{
    struct v4l2_control control;
    int rc;

    rc = vl42_get_control_info(fd, ctrlid, disabled, minValue, maxValue, defaultValue);

    if (rc == 0)
    {
        if (!(*disabled))
        {
            memset(&control, 0, sizeof(control));

            control.id = ctrlid;

            if (-1 != ioctl(fd, VIDIOC_G_CTRL, &control))
            {
                *currenValue = control.value;
            }
            else
            {
                rc = errno;
            }
        }
    }

    return rc;
}
